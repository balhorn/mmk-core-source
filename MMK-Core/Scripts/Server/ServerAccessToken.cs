﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace MMK.Core.Server
{
    public static class ServerAccessToken
    {
        public static void SendSuccess (GameObject target)
        {
            ExecuteEvents.Execute<IAccessTokenHandler>(target, null, (handler, data) => handler.OnAccessTokenSuccess());
        }

        public static void SendFail (GameObject target)
        {
            ExecuteEvents.Execute<IAccessTokenHandler>(target, null, (handler, data) => handler.OnAccessTokenFail());
        }
    }
}