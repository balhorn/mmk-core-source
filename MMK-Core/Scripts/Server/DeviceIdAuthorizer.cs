﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace MMK.Core.Server
{
    public class DeviceIdAuthorizer : MonoBehaviour, IBlockingOperation
    {
        private void Start ()
        {
            GameController.RegisterBlockingOperation(this, 1);
            API.Server.Auth(SystemInfo.deviceUniqueIdentifier, AuthCallback);;
        }

        private void AuthCallback(IServerResponse response)
        {
            GameController.CallbackExecuted(this);

            if (response.Error != null) {
                Debug.Log(response.Error);
                ServerAccessToken.SendFail(gameObject);
                return;
            }

            API.Server.AccessToken = response.Result.GetString("access_token");
            ServerAccessToken.SendSuccess(gameObject);
        }
    }
}