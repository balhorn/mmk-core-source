﻿using UnityEngine.Networking;

namespace MMK.Core.Server
{
    internal class BasicServerResponse : IServerResponse
    {
        public string Error { get; protected set; }
        public string RawResult { get; protected set; }

        public ServerResponse Result { get; protected set; }

        public BasicServerResponse(UnityWebRequest request)
        {
            if (request.isError || request.responseCode != 200) {
                Error = request.isError ? request.error : request.downloadHandler.text;
                return;
            }

            RawResult = request.downloadHandler.text;
            Result = new ServerResponse(RawResult);
        }
    }
}