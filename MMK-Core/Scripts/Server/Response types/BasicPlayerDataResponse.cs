﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace MMK.Core.Server
{
    internal class BasicPlayerDataResponse : BasicServerResponse, IPlayerDataResponse
    {
        public int Money { get; }
        public int Lifes { get; }
        public List<string> Boosters { get; }

        public BasicPlayerDataResponse(UnityWebRequest request) : base(request)
        {
            Debug.Log(RawResult);
            Money = Result.GetInteger("money");
            Lifes = Result.GetInteger("lifes");

            var boosters = Result.GetArray("boosters");
            Boosters = new List<string>();
            for (int i = 0; i < boosters.ArrayCount; i++) {
                Boosters.Add(boosters.GetArrayElementAt(i).ToString());
            }
        }
    }
}