﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using MMK.Core.InApp;
using MMK.Core;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace MMK.Core.Server
{
    internal class BasicProductsResponse : BasicServerResponse, IProductsResponse
    {
        private MonoBehaviour coroutineHandler;
        private readonly Coroutine downloadingCoroutine;

        public BasicProductsResponse(UnityWebRequest request, MonoBehaviour coroutineHandler) : base(request)
        {
            this.coroutineHandler = coroutineHandler;
            Products = new List<IProduct>();

            downloadingCoroutine = coroutineHandler.StartCoroutine(DownloadProducts());
        }

        public Coroutine WaitForDownload()
        {
            return downloadingCoroutine;
        }

        private IEnumerator DownloadProducts()
        {
            for (int i = 0; i < Result.ArrayCount; i++) {
                var downloadedProduct = Result.GetArrayElementAt(i);
                var product = new Product {
                    Id = downloadedProduct.GetString("product_url"),
                    Price = downloadedProduct.GetString("product_price_amount"),
                    Name = downloadedProduct.GetString("name"),
                    IsInstantPurchase = false,
                    Amount = 1
                };

                var imageUrl = GetImagePath(product.Id);
                using (var request = UnityWebRequest.GetTexture(imageUrl)) {
                    yield return request.Send();
                    var handler = request.downloadHandler as DownloadHandlerTexture;
                    var texture = handler.texture;
                    var textureRect = new Rect(0, 0, texture.width, texture.height);

                    product.Icon = Sprite.Create(texture, textureRect, Vector2.zero);
                }

                Products.Add(product);

            }
        }

        private string GetImagePath (string url)
        {
            return new StringBuilder(Constants.ServerURL).Append("/").Append(url).ToString();
        }

        public List<IProduct> Products { get; }

        private class Product : IProduct
        {
            public string Id { get; set; }
            public Sprite Icon { get; set; }
            public int Amount { get; set; }
            public string Name { get; set; }
            public string Price { get; set; }
            public bool IsInstantPurchase { get; set; }

            public void Consume()
            {
            }
        }
    }


}