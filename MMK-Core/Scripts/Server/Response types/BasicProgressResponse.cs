﻿using UnityEngine.Networking;

namespace MMK.Core.Server
{
    internal class BasicProgressResponse : BasicServerResponse, IProgressResponse
    {
        public BasicProgressResponse(UnityWebRequest request) : base(request)
        {
            Progress = request.downloadHandler.data;
        }

        public byte[] Progress { get; }
    }
}