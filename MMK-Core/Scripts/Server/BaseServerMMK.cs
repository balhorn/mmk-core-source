﻿using System.Collections;
using System.Web;
using MMK.Core;
using UnityEngine;
using UnityEngine.Networking;
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace MMK.Core.Server
{
    // ReSharper disable once InconsistentNaming
    public class BaseServerMMK : MonoBehaviour, IServer
    {
        public virtual string AccessToken { get; set; }

        public virtual void Auth(string accessKey, ServerCallback callback)
        {
            var request = new RequestBuilder()
                .Post("/auth")
                .AddParameter("FAC", accessKey)
                .Build();

            StartCoroutine(SendRequest(request, callback));
        }

        public virtual void GetPlayerData(PlayerDataCallback callback)
        {
            var request = new RequestBuilder()
                .Get("/user")
                .SetAccessToken(AccessToken)
                .Build();

            StartCoroutine(SendRequest(request, callback));
        }

        public void GetLives(ServerCallback callback)
        {
            var request = new RequestBuilder()
                .Get("/lives")
                .SetAccessToken(AccessToken)
                .Build();

            StartCoroutine(SendRequest(request, callback));
        }

        public void GetBoosters(ServerCallback callback)
        {
            var request = new RequestBuilder()
                .Get("/boosters")
                .SetAccessToken(AccessToken)
                .Build();

            StartCoroutine(SendRequest(request, callback));
        }

        public virtual void GetProducts(ProductsCallback callback)
        {
            var request = new RequestBuilder()
                .Get("/products")
                .SetAccessToken(AccessToken)
                .Build();

            StartCoroutine(SendRequest(request, callback));
        }

        public virtual void GetProgress(ProgressCallback callback)
        {
            var request = new RequestBuilder()
                .Get("/progress")
                .SetAccessToken(AccessToken)
                .Build();

            StartCoroutine(SendRequest(request, callback));
        }

        public virtual void UseBooster(string boosterId, PlayerDataCallback callback)
        {
            var request = new RequestBuilder()
                .Post("/boosters")
                .SetAccessToken(AccessToken)
                .AddParameter("booster_id", boosterId)
                .Build();

            StartCoroutine(SendRequest(request, callback));
        }

        public virtual void BuyBooster(string boosterId, int quantity, PlayerDataCallback callback)
        {
            var request = new RequestBuilder()
                .Post("/boosters/buy")
                .SetAccessToken(AccessToken)
                .AddParameter("booster_id", boosterId)
                .Build();

            StartCoroutine(SendRequest(request, callback));
        }

        public virtual void GetDailyBonus(ServerCallback callback)
        {

        }

        public virtual void ApplyDailyBonus(PlayerDataCallback callback)
        {

        }

        public virtual void UseLife(ServerCallback callback)
        {
            var request = new RequestBuilder()
                .Post("/lives")
                .SetAccessToken(AccessToken)
                .Build();

            StartCoroutine(SendRequest(request, callback));
        }

        public virtual void ConfirmPurchase(string confirmationKey, PlayerDataCallback callback)
        {
            var request = new RequestBuilder()
                .Post("/products")
                .SetAccessToken(AccessToken)
                .AddParameter("signed_request", confirmationKey)
                .Build();

            StartCoroutine(SendRequest(request, callback));
        }

        public virtual void SubmitProgress(byte[] progress, ServerCallback callback)
        {
            var request = new RequestBuilder()
                .Post("/progress")
                .SetAccessToken(AccessToken)
                .AddBynary("saved_game", progress)
                .Build();

            StartCoroutine(SendRequest(request, callback));
        }

        protected virtual IEnumerator SendRequest(UnityWebRequest request, ServerCallback callback)
        {
            yield return request.Send();

            var response = new BasicServerResponse(request);
            callback.Invoke(response);
        }

        protected virtual IEnumerator SendRequest(UnityWebRequest request, PlayerDataCallback callback)
        {
            yield return request.Send();

            var response = new BasicPlayerDataResponse(request);
            callback.Invoke(response);
        }

        protected virtual IEnumerator SendRequest(UnityWebRequest request, ProgressCallback callback)
        {
            yield return request.Send();

            var response = new BasicProgressResponse(request);
            callback.Invoke(response);
        }

        protected virtual IEnumerator SendRequest(UnityWebRequest request, ProductsCallback callback)
        {
            yield return request.Send();

            var response = new BasicProductsResponse(request, this);
            yield return response.WaitForDownload();

            callback.Invoke(response);
        }

        protected virtual void Start()
        {
            API.Server = this;
        }
    }
}