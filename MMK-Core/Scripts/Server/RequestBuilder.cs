﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace MMK.Core.Server
{
    public class RequestBuilder
    {
        private string urlHost;
        private StringBuilder getParamsBuilder;
        private WWWForm postForm;
        private bool isPost;
        private Dictionary<string, string> headers;

        private string FullPath => UrlHost + (!IsPost ? GetParamsBuilder.ToString() : "");

        public string UrlHost
        {
            get { return urlHost; }

            set { urlHost = value; }
        }

        public StringBuilder GetParamsBuilder
        {
            get { return getParamsBuilder; }

            set { getParamsBuilder = value; }
        }

        public WWWForm PostForm
        {
            get { return postForm; }

            set { postForm = value; }
        }

        public bool IsPost
        {
            get { return isPost; }

            set { isPost = value; }
        }

        public Dictionary<string, string> Headers
        {
            get { return headers; }

            set { headers = value; }
        }

        public RequestBuilder Get(string url)
        {
            IsPost = false;

            var uri = new Uri(Constants.ServerURL + url);
            UrlHost = uri.AbsoluteUri;
            GetParamsBuilder.Append(uri.Query);

            return this;
        }

        public RequestBuilder Post(string url)
        {
            IsPost = true;

            var uri = new Uri(Constants.ServerURL + url);
            UrlHost = uri.AbsoluteUri;

            return this;
        }

        public RequestBuilder AddParameter(string name, string value)
        {
            if (IsPost) {
                PostForm.AddField(name, value);
            } else {
                AddGetParameter(name, value);
            }

            return this;
        }

        public RequestBuilder AddParameter(string name, int value)
        {
            if (IsPost) {
                PostForm.AddField(name, value);
            } else {
                AddGetParameter(name, value);
            }

            return this;
        }

        public RequestBuilder AddBynary(string name, byte[] data)
        {
            if (IsPost) {
                PostForm.AddBinaryData(name, data);
            }

            return this;
        }

        public RequestBuilder SetAccessToken(string accessToken)
        {
            return SetHeader(Constants.AccessTokenHeader, accessToken);
        }

        public RequestBuilder SetHeader(string name, string value)
        {
            if (name != null && value != null) {
                Headers[name] = value;
            }

            return this;
        }

        public UnityWebRequest Build()
        {
            var result = IsPost ? UnityWebRequest.Post(FullPath, PostForm) : UnityWebRequest.Get(FullPath);
            foreach (var header in Headers) {
                result.SetRequestHeader(header.Key, header.Value);
            }

            return result;
        }

        private void AddGetParameter(string name, object value)
        {
            var currentParamsString = GetParamsBuilder.ToString();
            if (!currentParamsString.EndsWith("?")) {
                GetParamsBuilder.Append("&");
            }
            GetParamsBuilder.AppendFormat("{0}={1}", name, value);
        }

        public RequestBuilder()
        {
            Headers = new Dictionary<string, string>();
            UrlHost = string.Empty;
            GetParamsBuilder = new StringBuilder("?");
            PostForm = new WWWForm();
        }

        public override string ToString()
        {
            var builder = new StringBuilder()
                .Append($"Method: {(IsPost ? "POST" : "GET")}")
                .Append("\n")
                .Append($"Url: {FullPath}")
                .Append("\n");

            foreach (var header in Headers) {
                builder.AppendFormat("{0}: {1}", header.Key, header.Value);
            }

            return builder.ToString();
        }
    }
}