﻿namespace MMK.Core.Server
{
    public class FakeServer : IServer
    {
        public string AccessToken { get; set; }
        public void Auth(string accessKey, ServerCallback callback)
        {
        }

        public void GetLives(ServerCallback callback)
        {
        }

        public void GetBoosters(ServerCallback callback)
        {
        }

        public void GetPlayerData(PlayerDataCallback callback)
        {
        }

        public void GetDailyBonus(ServerCallback callback)
        {
        }

        public void GetProducts(ProductsCallback callback)
        {
        }

        public void GetProgress(ProgressCallback callback)
        {
        }

        public void UseBooster(string boosterId, PlayerDataCallback callback)
        {
        }

        public void BuyBooster(string boosterId, int quantity, PlayerDataCallback callback)
        {
        }

        public void ApplyDailyBonus(PlayerDataCallback callback)
        {
        }

        public void UseLife(ServerCallback callback)
        {
        }

        public void ConfirmPurchase(string confirmationKey, PlayerDataCallback callback)
        {
        }

        public void SubmitProgress(byte[] progress, ServerCallback callback)
        {
        }
    }
}