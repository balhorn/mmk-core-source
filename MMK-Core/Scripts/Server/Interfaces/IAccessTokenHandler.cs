﻿using UnityEngine.EventSystems;

namespace MMK.Core.Server
{
    public interface IAccessTokenHandler : IEventSystemHandler
    {
        void OnAccessTokenSuccess();
        void OnAccessTokenFail();
    }
}