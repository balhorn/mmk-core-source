﻿namespace MMK.Core.Server
{
    public interface IServerResponse
    {
        string Error { get; }
        string RawResult { get; }

        ServerResponse Result { get; }
    }
}