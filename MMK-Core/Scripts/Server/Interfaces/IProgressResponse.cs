﻿namespace MMK.Core.Server
{
    public interface IProgressResponse : IServerResponse
    {
        byte[] Progress { get; }
    }
}