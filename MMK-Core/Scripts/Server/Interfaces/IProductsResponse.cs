﻿using System.Collections.Generic;
using MMK.Core.InApp;

namespace MMK.Core.Server
{
    public interface IProductsResponse : IServerResponse
    {
        List<IProduct> Products { get; }
    }
}