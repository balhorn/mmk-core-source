﻿namespace MMK.Core.Server
{
    public interface IServer
    {
        string AccessToken { get; set; }

        void Auth(string accessKey, ServerCallback callback);

        void GetPlayerData(PlayerDataCallback callback);
        void GetDailyBonus(ServerCallback callback);
        void GetProducts(ProductsCallback callback);
        void GetProgress(ProgressCallback callback);
        void GetLives(ServerCallback callback);
        void GetBoosters(ServerCallback callback);

        void UseBooster(string boosterId, PlayerDataCallback callback);
        void BuyBooster(string boosterId, int quantity, PlayerDataCallback callback);

        void ApplyDailyBonus(PlayerDataCallback callback);

        void UseLife(ServerCallback callback);

        void ConfirmPurchase(string confirmationKey, PlayerDataCallback callback);

        void SubmitProgress(byte[] progress, ServerCallback callback);
    }

    public delegate void ServerCallback(IServerResponse response);
    public delegate void PlayerDataCallback(IPlayerDataResponse response);
    public delegate void ProductsCallback(IProductsResponse response);
    public delegate void ProgressCallback(IProgressResponse response);
}