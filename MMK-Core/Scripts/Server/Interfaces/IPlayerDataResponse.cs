﻿using System.Collections.Generic;

namespace MMK.Core.Server
{
    public interface IPlayerDataResponse : IServerResponse
    {
        int Money { get; }
        int Lifes { get; }

        List<string> Boosters { get; }
    }
}