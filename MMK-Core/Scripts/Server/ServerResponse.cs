﻿using System;
using SimpleJSON;
using UnityEngine;

namespace MMK.Core.Server
{
    public class ServerResponse
    {
        private readonly JSONNode jsonObject;

        public int ArrayCount => jsonObject.Count;

        public string GetString(string key)
        {
            return jsonObject[key].Value;
        }

        public int GetInteger(string key)
        {
            return jsonObject[key].AsInt;
        }

        public long GetLong(string key)
        {
            long result;
            long.TryParse(GetString(key), out result);
            return result;
        }

        public ServerResponse GetObject(string key)
        {
            var jsonObject = this.jsonObject[key].AsObject;
            return jsonObject != null ? new ServerResponse(jsonObject.ToJSON(0)) : new ServerResponse();
        }

        public ServerResponse GetArray(string key)
        {
            var jsonArray = jsonObject[key].AsArray;
            return jsonArray != null ? new ServerResponse(jsonArray.ToJSON(0)) : new ServerResponse();
        }

        public ServerResponse GetArrayElementAt(int index)
        {
            var jsonArray = jsonObject.AsArray;
            if (jsonArray == null) {
                return new ServerResponse();
            }

            var json = jsonArray[index].ToJSON(0);
            return new ServerResponse(json);
        }

        public string ToJson()
        {
            return jsonObject.ToJSON(0);
        }

        public ServerResponse(string json)
        {
            try {
                jsonObject = JSON.Parse(json);
            } catch (Exception e) {
                Debug.Log(e);
                jsonObject = new JSONData("");
            }
        }

        public ServerResponse()
        {
            jsonObject = new JSONData("");
        }

        public override string ToString()
        {
            return jsonObject.Value;
        }
    }
}