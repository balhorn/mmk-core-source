﻿    using UnityEngine;

namespace MMK.Core.UI
{
    public class OctopusAppear : MonoBehaviour
    {
        public Animator anim;

        public void Appear ()
        {
            anim.SetTrigger(Animator.StringToHash("Appear"));
        }
    }
}
