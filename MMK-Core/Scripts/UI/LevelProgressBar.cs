﻿using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class LevelProgressBar : MonoBehaviour
    {
        private ProgressBarFiller progressBar;
        private RectTransform rectTrans;

        public GameObject firstStar;
        public GameObject secondStar;
        public GameObject thirdStar;

        private float currentProgress;

        private float barLength;
        private float halfStarWidth;
        private float borderOffset;

        private float firstStarFillAmount;
        private float secondStarFillAmount;
        private float thirdStarFillAmount;

        private void Start()
        {
            rectTrans = GetComponent<RectTransform> ();
            progressBar = GetComponent<ProgressBarFiller> ();
            EstablishStars ();
        }

        private void EstablishStars()
        {
            var stars = new[] {firstStar, secondStar, thirdStar};
            var scores = new[] {LevelConfig.oneStarScore, LevelConfig.twoStarsScore, LevelConfig.threeStarsScore};

            firstStarFillAmount = (float)LevelConfig.oneStarScore / LevelConfig.threeStarsScore;
            secondStarFillAmount = (float) LevelConfig.twoStarsScore / LevelConfig.threeStarsScore;
            thirdStarFillAmount = 1f;

            halfStarWidth = thirdStar.GetComponent<RectTransform>().rect.width / 2;
            barLength = rectTrans.rect.width;
            borderOffset = barLength * 0.05f;

            var scorePerLength = barLength / LevelConfig.threeStarsScore;

            for (int i = 0; i < 3; i++) {
                var starPosition = scorePerLength * scores[i];
                SetPosition(stars[i], starPosition);
            }

            var middleStarPosition = stars[1].GetComponent<RectTransform>().anchoredPosition;
            var leftStarPosition = stars[0].GetComponent<RectTransform>().anchoredPosition;
            var rightStarPosition = stars[2].GetComponent<RectTransform>().anchoredPosition;

            if (Mathf.Abs(middleStarPosition.x - rightStarPosition.x) < 2 * halfStarWidth
                && Mathf.Abs(middleStarPosition.x - leftStarPosition.x) < 2 * halfStarWidth) {
                middleStarPosition.x = rightStarPosition.x - 2 * halfStarWidth;
                leftStarPosition.x = middleStarPosition.x - 2 * halfStarWidth;
            } else if (Mathf.Abs(middleStarPosition.x - rightStarPosition.x) < 2 * halfStarWidth) {
                middleStarPosition.x = rightStarPosition.x - 2 * halfStarWidth;
            } else if (Mathf.Abs(middleStarPosition.x - leftStarPosition.x) < 2 * halfStarWidth) {
                middleStarPosition.x = leftStarPosition.x + 2 * halfStarWidth;
            }

            stars[1].GetComponent<RectTransform>().anchoredPosition = middleStarPosition;
            stars[0].GetComponent<RectTransform>().anchoredPosition = leftStarPosition;
        }

        private float SetPosition(GameObject star, float xPos)
        {
            var borderPosition = barLength - halfStarWidth - borderOffset;
            var clampedX = Mathf.Clamp(xPos, borderOffset + halfStarWidth, borderPosition);
            var prevPos = star.GetComponent<RectTransform> ().anchoredPosition;

            prevPos.Set (clampedX, prevPos.y);

            star.GetComponent<RectTransform> ().anchoredPosition = prevPos;

            return clampedX;
        }

        private void HighlightStar(GameObject star)
        {
            var image = star.GetComponent<Image> ();
            var color = image.color;
            color.a = 1f;
            image.color = color;
        }

        public void UpdateProgress(int amount)
        {
            float normalizedScore = (float) amount / LevelConfig.threeStarsScore;

            if (currentProgress + normalizedScore >= 1f) {
                progressBar.SetProgress(1f);
                currentProgress = 1f;
                ActivateStar ();
                return;
            }

            progressBar.SetProgress(currentProgress + normalizedScore);
            currentProgress += normalizedScore;
            ActivateStar ();
        }

        private void ActivateStar()
        {
            if (currentProgress >= thirdStarFillAmount) {
                HighlightStar (thirdStar);
            } 

            if (currentProgress >= secondStarFillAmount) {
                HighlightStar (secondStar);
            }

            if (currentProgress >= firstStarFillAmount) {
                HighlightStar (firstStar);
            }
        }
    }
}
