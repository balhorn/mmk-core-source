﻿using UnityEngine;

namespace MMK.Core.UI
{
    public class HardCurrencyPurchase : MonoBehaviour
    {
        public int amount;

        public void Purchase ()
        {
            Player.Instance.GainCurrency(amount);
        }
    }
}
