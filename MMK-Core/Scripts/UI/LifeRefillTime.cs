﻿using MMK.Core;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class LifeRefillTime : MonoBehaviour
    {
        public GameObject plusButton;

        private float timeElapsed;
        private Text displayText;

        private const string FullMessage = "Full";

        private void Awake ()
        {
            displayText = GetComponent<Text>();

            if (Player.Instance.Lifes == Constants.MaximumLifes) {
                displayText.text = FullMessage;
                plusButton?.SetActive(false);
                enabled = false;
            } else {
                plusButton?.SetActive(true);
            }

        }

        private void FixedUpdate ()
        {
            if (Player.Instance.Lifes == Constants.MaximumLifes && !displayText.text.Equals(FullMessage)) {
                displayText.text = FullMessage;
                plusButton?.SetActive(false);
                return;
            }

            if (Player.Instance.Lifes == Constants.MaximumLifes && displayText.text.Equals(FullMessage)) {
                return;
            }

            timeElapsed += Time.fixedDeltaTime;

            if (timeElapsed < 1f) {
                return;
            }

            timeElapsed = 0;
            displayText.text = Player.Instance.TimeToRegainLife;
        }
    }
}
