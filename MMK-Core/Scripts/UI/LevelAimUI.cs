﻿using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class LevelAimUI : MonoBehaviour
    {
        public Image      mainImage;
        public Text       progressText;
        public GameObject tick;

        [HideInInspector] public int requiredAmount;
        [HideInInspector] public Sprite sprite;
        [HideInInspector] public bool displayCurrentAmount = true;

        public void Refresh()
        {
            if (mainImage != null) {
                mainImage.sprite = sprite;
            }

            if (progressText != null) {
                progressText.gameObject.SetActive (true);
                progressText.text = $"{(displayCurrentAmount ? "0/" : "")}{requiredAmount}";
            }

            if (tick != null) {
                tick.SetActive(false);
            }
        }
	
        public void UpdateUI (int amount)
        {
            if (amount >= requiredAmount) {
                if (progressText != null) {
                    progressText.gameObject.SetActive (false);
                }

                if (tick != null) {
                    tick.SetActive(true);
                }
                return;
            }

            if (progressText != null) {
                progressText.text = $"{(displayCurrentAmount ? $"{amount}/" : "")}{requiredAmount}";
            }
        }
    }
}
