﻿using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class FilledFiller : ProgressBarFiller
    {
        public override void SetProgress(float progress)
        {
            progressBar.fillAmount = progress;
        }
    }
}