﻿using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public abstract class ProgressBarFiller : MonoBehaviour
    {
        public Image progressBar;

        /// <summary>
        /// Updates progress bar for it to be filled up to progress value
        /// </summary>
        /// <param name="progress">Value from 0 to 1</param>
        public abstract void SetProgress(float progress);
    }
}