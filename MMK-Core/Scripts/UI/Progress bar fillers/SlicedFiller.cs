﻿using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class SlicedFiller : ProgressBarFiller
    {
        private RectTransform progressBarTransform;

        private float initialWidth;
        private float parentWidth;

        private Vector2 initialPosition;
        private Vector2 parentPosition;
        
        public override void SetProgress(float progress)
        {
            var width = Mathf.Lerp(initialWidth, parentWidth, progress);
            var position = Vector2.Lerp(initialPosition, parentPosition, progress);

            progressBarTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            progressBarTransform.anchoredPosition = position;
        }

        private void Start ()
        {
            progressBarTransform = progressBar.GetComponent<RectTransform>();
            var parentTransform = progressBarTransform.parent as RectTransform;

            parentPosition = parentTransform.anchoredPosition;
            parentWidth = parentTransform.rect.width;
            initialWidth = progressBarTransform.rect.width;
            initialPosition = progressBarTransform.anchoredPosition;
        }
    }
}