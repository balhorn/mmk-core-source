﻿using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class ValueUpdateListener : MonoBehaviour
    {
        public enum ValueType
        {
            Lifes, Currency
        }

        public ValueType valueType;
        private Text text;

        private void Start ()
        {
            text = GetComponent<Text>();
            switch (valueType) {
                case ValueType.Lifes:
                    UpdateLifes();
                    Player.LifesUpdated += UpdateLifes;
                    break;
                case ValueType.Currency:
                    UpdateCurrency();
                    Player.CurrencyUpdated += UpdateCurrency;
                    break;
            }
        
        }

        private void UpdateLifes()
        {
            if (text == null) {
                Debug.LogError("There is no text component assigned to the Value update listener object");
                return;
            }

            text.text = Player.Instance.Lifes.ToString();
        }

        private void UpdateCurrency()
        {
            if (text == null) {
                Debug.LogError("There is no text component assigned to the Value update listener object");
                return;
            }

            text.text = Player.Instance.HardCurrency.ToString();
        }

        private void OnDestroy ()
        {
            Player.LifesUpdated -= UpdateLifes;
            Player.CurrencyUpdated -= UpdateCurrency;
        }
    }
}
