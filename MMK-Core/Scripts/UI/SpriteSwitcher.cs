﻿using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class SpriteSwitcher : MonoBehaviour
    {
        public Sprite[] sprites;

        private Image image;
        private bool pressed;

        private void Awake ()
        {
            image = GetComponent<Image>();
        }

        public void Switch ()
        {
            if (sprites.Length <= 1) {
                return;
            }

            var index = pressed ? 0 : 1;
            pressed = !pressed;

            image.sprite = sprites[index];
        }
    }
}
