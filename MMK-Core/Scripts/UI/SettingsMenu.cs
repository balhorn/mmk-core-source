﻿using System.Collections.Generic;
using MMK.Core;
using MMK.Core.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public abstract class SettingsMenu : MonoBehaviour 
    {
        public GameObject expandedMenu;
        public PopUp animationSwitcher;

        private SpriteSwitcher switcher;
        private BaseEventData data;
        private Button button;

        private EventSystem eventSystem;

        private void Awake()
        {
            button = GetComponent<Button> ();
            data = new BaseEventData (EventSystem.current);
            eventSystem = EventSystem.current;
            switcher = GetComponent<SpriteSwitcher>();
        }

        private void Update()
        {
            if (!Input.GetMouseButtonDown(0) || !expandedMenu.activeSelf) {
                return;
            }

            var raycastResults = new List<RaycastResult> ();
            var eventData = new PointerEventData(eventSystem) {position = Input.mousePosition};


            eventSystem.RaycastAll (eventData, raycastResults);
            var uiElement = raycastResults [0].gameObject;
            if (uiElement != null && uiElement.layer == LayerMask.NameToLayer("Empty Space")) {
                ResumeGame ();
            }
        }

        public void OnButtonPressed()
        {
            if (GameController.state == GameState.Animating
                || GameController.state == GameState.WinBonus) {
                return;
            }

            var isPressed = expandedMenu.activeInHierarchy;

            //Time.timeScale = isPressed ? 1 : 0;
            ToggleMenu (!isPressed);

            //if (isPressed) {
            //    button.OnDeselect (data);
            //} else {
            //    button.OnSelect (data);
            //}
        }

        public void OnMusicPressed()
        {
            SoundManager.MusicOn = !SoundManager.MusicOn;
        }

        public void OnSoundPressed()
        {
            SoundManager.SoundOn = !SoundManager.SoundOn;
        }

        public abstract void OnExitPressed();

        public void OnResumePressed()
        {
            ResumeGame ();
        }

        protected void ToggleMenu(bool active)
        {
            //expandedMenu.SetActive(active);
            if (animationSwitcher == null) {
                return;
            }

            if (active) {
                animationSwitcher.Show();
            } else {
                animationSwitcher.Hide();
            }
        }

        protected void ResumeGame()
        {
            Time.timeScale = 1;
            ToggleMenu (false);
            switcher?.Switch();
        }
    }
}