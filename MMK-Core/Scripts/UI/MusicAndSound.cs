﻿using UnityEngine;

namespace MMK.Core.UI
{
    [RequireComponent(typeof(SpriteSwitcher))]
    public class MusicAndSound : MonoBehaviour
    {
        public Type type;

        private SpriteSwitcher switcher;

        private void Awake ()
        {
            switcher = GetComponent<SpriteSwitcher>();
        }

        private void Start ()
        {
            var condition = type == Type.Music ? SoundManager.MusicOn : SoundManager.SoundOn;
            if (!condition) {
                switcher.Switch();
            }
        }

        public enum Type { Music, Sound }
    }
}
