﻿using MMK.Core.UI;
using UnityEngine.UI;

namespace MMK.Core
{
    public class NotEnoughCurrencyWindow : PopUp
    {
        public Text currencyText;
        public Image icon;

        public void Show (int currencyLack)
        {
            if (currencyText != null) {
                currencyText.text = currencyLack.ToString();
            }

            Show();
        }
    }
}