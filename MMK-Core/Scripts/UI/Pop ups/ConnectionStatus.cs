﻿using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class ConnectionStatus : PopUp
    {
        public Text statusText;

        public void Show (string text)
        {
            Show();
            if (statusText != null) {
                statusText.text = text;
            }
        }
    }
}
