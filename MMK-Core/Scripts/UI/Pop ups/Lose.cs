﻿using MMK.Core.Socials.UI;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class Lose : RotatingPopup
    {
        public GameObject aimsPanel;

        public Text levelNumber;
        public Text livesAmount;

        public Highscores highscore;

        public override void Show ()
        {
            base.Show();

            DecreaseLife();

            PopulateUI();
        }

        protected override void OnOrientationChanged(Orientation before, Orientation current)
        {
            base.OnOrientationChanged(before, current);
            PopulateUI();
        }

        private void PopulateUI ()
        {
            Get(highscore)?.Populate(Level.Current.PremadeLevel, Level.Current.highScore);

            var levelNumber = Get(this.levelNumber);
            if (levelNumber != null) {
                levelNumber.text = $"LEVEL {Level.Current.number}";
            }

            var livesAmount = Get(this.livesAmount);
            if (livesAmount != null) {
                livesAmount.text = Player.Instance.Lifes.ToString();
            }

            var aimsPanel = Get(this.aimsPanel);
            if (aimsPanel != null) {
                foreach (var levelAim in Level.Current.aims) {
                    levelAim.InstantiateUIItem(aimsPanel);
                }
            }
        }

        private void DecreaseLife ()
        {
            if (Player.Instance.Lifes > 0) {
                Player.Instance.DecreaseLife();
            }
        }
    }
}
