﻿using System.Collections.Generic;
using System.Linq;
using MMK.Core.Aims;
using MMK.Core.Boosters;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public abstract class OutOfMoves : PopUp
    {
        public Text continueCost;
        public Transform preboostersBase;
        public GetBoosterWindow getBooster;

        public abstract void Continue();
    }
}
