﻿using MMK.Core.Socials.UI;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public abstract class Win : RotatingPopup
    {
        public Text scoreText;
        public Text highScoreText;
        public Text levelNumber;

        private Highscores highscore;

        private int currentScore;
        private int currentHighscore;

        protected override void OnOrientationChanged(Orientation before, Orientation current)
        {
            base.OnOrientationChanged(before, current);
            PopulateVariables(currentScore, currentHighscore);
        }

        public virtual void Activate (int score, int highScore, bool isLevelCompleted)
        {
            currentScore = score;
            currentHighscore = highScore;

            Show();

            SoundManager.Win();
            highscore?.Populate(Level.Current.PremadeLevel, Level.Current.highScore);

            PopulateVariables(score, highScore);

            HighlightStars(score, isLevelCompleted);
        }

        protected abstract void HighlightStars(int score, bool isLevelCompleted);

        protected virtual void PopulateVariables(int score, int highScore)
        {
            var scoreText = Get(this.scoreText);
            if (scoreText != null) {
                scoreText.text = score.ToString();
            }

            var highScoreText = Get(this.highScoreText);
            if (highScoreText != null) {
                highScoreText.text = highScore.ToString();
            }

            var levelNumber = Get(this.levelNumber);
            if (levelNumber != null) {
                levelNumber.text = $"LEVEL {Level.Current.number}";
            }
        }

        protected override void Awake ()
        {
            base.Awake();
            highscore = GetComponentInChildren<Highscores>();
        }
    }
}
