﻿using MMK.Core;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class Lives : PopUp
    {
        public Text timer;

        public override void Show ()
        {
            if (Player.Instance.Lifes == Constants.MaximumLifes) {
                base.Show();
            }
        }

        private void Update ()
        {
            if (timer != null) {
                timer.text = Player.Instance.TimeToRegainLife;
            }
        }
    }
}
