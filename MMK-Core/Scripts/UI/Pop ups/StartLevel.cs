﻿using MMK.Core.Socials.UI;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class StartLevel : RotatingPopup
    {
        public Text levelNumber;
        public Transform starsParent;

        public Sprite aquiredStarSprite;
        public Sprite emptyStarSprite;

        private Level level;
        private PremadeLevel levelContent;

        private Highscores highscores;

        public virtual void Setup(Level level, PremadeLevel levelContent)
        {
            this.level = level;
            this.levelContent = levelContent;

            Debug.Log($"Level content: {levelContent}");

            SetStars();
            SetLevelNumber();
            Setup();
        }

        protected override void OnOrientationChanged(Orientation before, Orientation current)
        {
            base.OnOrientationChanged(before, current);
            Setup(level, levelContent);
        }

        protected virtual void SetStars ()
        {
            var stars = level.starsCollected;
            var starsParent = Get(this.starsParent);
            if (starsParent == null) {
                return;
            }

            for (int i = 0; i < 3; ++i) {
                var starTransform = starsParent.GetChild(i);
                if (starTransform == null) {
                    continue;
                }

                starTransform.gameObject.GetComponent<Image>().sprite = i < stars ? aquiredStarSprite : emptyStarSprite;
            }
        }

        protected virtual void SetLevelNumber ()
        {
            var levelNumber = Get(this.levelNumber);
            if (levelNumber != null) {
                levelNumber.text = $"LEVEL {level.number}";
            }
        }

        protected virtual void Setup ()
        {
            highscores?.Populate(levelContent, level.highScore);
        }

        protected override void Awake ()
        {
            base.Awake();
            highscores = GetComponentInChildren<Highscores>();
        }
    }
}
