﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MMK.Core.UI
{
    public class RotatingPopup : PopUp
    {
        public AnimatorSwitcher horizontal;
        public AnimatorSwitcher vertical;

        private bool isActive;
        private Orientation currentOrientation;

        #region Inheritance

        protected override AnimatorSwitcher Switcher => GetSwitcherByOrientation(CurrentOrientation);

        public override void SwitchTo()
        {
            isActive = true;
            base.SwitchTo();
        }

        public override void Show()
        {
            isActive = true;
            base.Show();
        }

        public override void Hide()
        {
            isActive = false;
            base.Hide();
        }

        protected virtual void OnOrientationChanged (Orientation before, Orientation current)
        {
            SwitchPopups(before, current);
        }

        protected virtual void Update ()
        {
            if (!isActive) {
                return;
            }

            var newOrientation = CurrentOrientation;
            if (newOrientation != currentOrientation) {
                OnOrientationChanged(currentOrientation, newOrientation);
                currentOrientation = newOrientation;
            }
        }

        protected virtual void Start ()
        {
            currentOrientation = CurrentOrientation;

            SetInitialPosition(horizontal);
            SetInitialPosition(vertical);
        }

        private void SetInitialPosition (AnimatorSwitcher switcher)
        {
            initialPositions[switcher] = switcher.GetComponent<RectTransform>().anchoredPosition;
        }

        #endregion

        #region Rotating

        private void SwitchPopups (Orientation before, Orientation current)
        {
            var previousSwitcher = GetSwitcherByOrientation(before);
            previousSwitcher.gameObject.SetActive(false);
            previousSwitcher.GetComponent<RectTransform>().anchoredPosition = initialPositions[previousSwitcher];

            GetSwitcherByOrientation(current).gameObject.SetActive(true);
        }

        private AnimatorSwitcher GetSwitcherByOrientation (Orientation screenOrientation)
        {
            switch (screenOrientation) {
                case Orientation.Vertical:
                    return vertical;
                case Orientation.Horizontal:
                    return horizontal;
                default:
                    throw new ArgumentOutOfRangeException(nameof(screenOrientation), screenOrientation, null);
            }
        }

        protected Orientation CurrentOrientation
        {
            get
            {
                if (!Application.isMobilePlatform) {
                    return OrientationByRatio();
                }

                switch (Screen.orientation) {
                    case ScreenOrientation.Portrait:
                    case ScreenOrientation.PortraitUpsideDown:
                        return Orientation.Vertical;
                    case ScreenOrientation.LandscapeLeft:
                    case ScreenOrientation.LandscapeRight:
                        return Orientation.Horizontal;
                    default:
                        return OrientationByRatio();
                }
            }
        }

        private static Orientation OrientationByRatio ()
        {
            var ratio = (float)Screen.width / Screen.height;
            return ratio > 1f ? Orientation.Horizontal : Orientation.Vertical;
        }

        private readonly Dictionary<AnimatorSwitcher, Vector2> initialPositions = new Dictionary<AnimatorSwitcher, Vector2>();

        protected enum Orientation {Vertical, Horizontal}

        #endregion

        #region Component access

        protected T Get<T> (T component) where T : Component
        {
            if (component == null) {
                return null;
            }

            if (ExistsInCache(component)) {
                return GetFromCache(component);
            }

            var parent = Switcher.gameObject;
            var componentTransform = GetComponentTransform(parent.transform, component);

            if (componentTransform == null) {
                Debug.LogError(
                    $"Failed to found an equivalent to {component}. Probably it has different names on defferent popups");
                return null;
            }

            var result = componentTransform.GetComponent<T>();
            AddToCache(component, result);
            return result;
        }

        protected GameObject Get (GameObject original)
        {
            if (original == null) {
                return null;
            }

            if (ExistsInCache(original)) {
                return GetFromCache(original);
            }

            var result = GetEquivalentObject(Switcher.transform, original);
            if (result == null) {
                Debug.LogError(
                    $"Failed to found an equivalent to {original}. Probably it has different names on defferent popups");
                return null;
            }

            AddToCache(original, result);
            return result;
        }

        private GameObject GetEquivalentObject (Transform root, GameObject original)
        {
            foreach (Transform child in root) {
                if (child.gameObject.name == original.name) {
                    return child.gameObject;
                }

                var result = GetEquivalentObject(child, original);
                if (result != null) {
                    return result;
                }
            }

            return null;
        }

        private Transform GetComponentTransform (Transform root, Component component)
        {
            foreach (Transform child in root) {
                if (child.gameObject.name == component.gameObject.name) {
                    return child;
                }

                var result = GetComponentTransform(child, component);
                if (result != null) {
                    return result;
                }
            }

            return null;
        }

        private bool ExistsInCache (Component component)
        {
            return GetComponentsCache().ContainsKey(component);
        }

        private T GetFromCache<T> (T component) where T : Component
        {
            return GetComponentsCache()[component] as T;
        }

        private void AddToCache (Component key, Component value)
        {
            GetComponentsCache()[key] = value;
        }

        private bool ExistsInCache (GameObject original)
        {
            return GetObjectsCache().ContainsKey(original);
        }

        private GameObject GetFromCache (GameObject original)
        {
            return GetObjectsCache()[original];
        }

        private void AddToCache (GameObject key, GameObject value)
        {
            GetObjectsCache()[key] = value;
        }

        private Dictionary<Component, Component> GetComponentsCache ()
        {
            return CurrentOrientation == Orientation.Horizontal
                ? horizontalComponentsCache
                : verticalComponentsCache;
        }

        private Dictionary<GameObject, GameObject> GetObjectsCache ()
        {
            return CurrentOrientation == Orientation.Horizontal
                ? horizontalObjectsCache
                : verticalObjectsCache;
        }

        private readonly Dictionary<GameObject, GameObject>  verticalObjectsCache = new Dictionary<GameObject, GameObject>();
        private readonly Dictionary<GameObject, GameObject>  horizontalObjectsCache = new Dictionary<GameObject, GameObject>();
        private readonly Dictionary<Component, Component> verticalComponentsCache = new Dictionary<Component, Component>();
        private readonly Dictionary<Component, Component> horizontalComponentsCache = new Dictionary<Component, Component>();

        #endregion
    }
}