﻿using MMK.Core.UI;
using UnityEngine.UI;

namespace MMK.Core
{
    public class DailyBonusWindow : PopUp
    {
        public Text moneyText;
        public AnimatorSwitcher cacheFlyAnimation;

        private int money;

        public int Money
        {
            get { return money; }
            set
            {
                money = value;
                moneyText.text = $"+{money}";
            }
        }

        public void Accept ()
        {
            Player.Instance.GainCurrency(money);
            Hide();
        }

        public override void Hide()
        {
            if (cacheFlyAnimation == null) {
                BaseHide();
                return;
            }

            cacheFlyAnimation.gameObject.SetActive(true);
            cacheFlyAnimation.On();
            Invoke(nameof(BaseHide), cacheFlyAnimation.disableAfter);
        }

        private void BaseHide ()
        {
            base.Hide();
        }
    }
}