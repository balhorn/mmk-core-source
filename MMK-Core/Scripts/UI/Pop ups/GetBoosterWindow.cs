﻿using System;
using MMK.Core.Boosters;
using MMK.Core.InApp;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class GetBoosterWindow : PopUp
    {
        public PopUp shopWindow;
        public Text costText;

        private  int cost;

        private Type boosterType;
        private Sprite boosterSprite;

        private Type BoosterType
        {
            get
            {
                return boosterType;
            }
            set
            {
                boosterType = value;
                cost = GetBoosterCost(boosterType.Name);
                costText.text = cost.ToString();
            }
        }

        public void Show (Type boosterType, Sprite image)
        {
            if (!boosterType.IsSubclassOf(typeof(AbstractBooster))) {
                throw new ArgumentException(
                    $"Booster type should be a child of {nameof(AbstractBooster)}",
                    nameof(boosterType));
            }

            BoosterType = boosterType;
            Show();

            boosterSprite = image;
        }

        public void Purchase ()
        {
            var product = new PurchasableBooster("Use your power", 1, boosterSprite);
            try {
                if (Player.Instance.HardCurrency >= cost) {
                    Player.Instance.GainBooster(BoosterType, 1);
                    Player.Instance.SpendCurrency(cost);

                    Player.Instance.ShopController.StartPurchase(product);
                } else {
                    shopWindow.Show();
                }
            } catch (Exception e) {
                Debug.LogError("Error during booster purchase");
                Debug.LogError(e);

                Player.Instance.ShopController.PurchaseFailed();
            }
        }

        protected virtual int GetBoosterCost(string boosterName)
        {
            return 0;
        }

        private class PurchasableBooster : IProduct
        {
            public string Id { get; }
            public string Name { get; }
            public int Amount { get; }
            public Sprite Icon { get; }
            public bool IsInstantPurchase => true;
            public string Price => "$0";

            public PurchasableBooster(string name, int amount, Sprite icon)
            {
                Id = name;
                Name = name;
                Amount = amount;
                Icon = icon;
            }

            public void Consume()
            {
            }
        }
    }
}
