﻿using MMK.Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MMK.Core.UI
{
    public abstract class PopUpManager : MonoBehaviour
    {
        public GameObject         tutorial;
        public Lose               lose;
        public OutOfMoves         outOfMoves;
        public Win                win;

        public ShopWindow       shopWindow;
        public GetBoosterWindow getBoosterWindow;

        protected virtual string MapSceneName => Constants.MapScene;

        public void Quit ()
        {
            GameController.state = GameState.Map;
            UnloadLevel ();
            LoadingScreen.Run(MapSceneName);
        }

        public virtual void Win ()
        {
            GameController.state = GameState.Pause;

            var score = ScoreManager.Score;
            var highScore = ScoreManager.HighScore;

            win.Activate(score, highScore, Level.Current.isCompleted);
        }

        public virtual void Lose()
        {
            outOfMoves.Hide();
            lose.Show();
        }

        public virtual void StartTutorial ()
        {
            if (tutorial == null) {
                return;
            }

            var controller = Level.Current.TutorialController;
            if (controller == null) {
                return;
            }

            tutorial.GetComponent<PopUp>().Show();
            tutorial.GetComponentInChildren<TutorialWindow>().Controller = controller;
        }

        public virtual void StartGame ()
        {
            GameController.state = GameState.Idle;
            Level.Current.Constraint.enabled = true;
        }

        public virtual void OutOfMoves ()
        {
            GameController.state = GameState.Pause;
            outOfMoves.Show();
        }


        public void Restart ()
        {
            LoadingScreen.Run(Constants.MainGameScene);
        }

        public void Map ()
        {
            if (Level.Current.isTestMode) {
                ExitTestMode();
                return;
            }

            Quit();
        }

        public abstract void Continue();

        protected abstract void Start();

        private void ExitTestMode()
        {
            Application.Quit();
        }

        private void UnloadLevel()
        {
            Destroy (Level.Current.gameObject);
        }
    }
}
