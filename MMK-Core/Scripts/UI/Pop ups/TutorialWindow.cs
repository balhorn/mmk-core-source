﻿using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class TutorialWindow : PopUp
    {
        public Animator tutorialAnimator;
        public GameObject button;
        public Sprite activeButtonSprite;

        private Image buttonImage;
        private Button buttonScript;

        public RuntimeAnimatorController Controller
        {
            set { tutorialAnimator.runtimeAnimatorController = value; }
        }

        protected override void Awake ()
        {
            buttonImage = button.GetComponent<Image>();
            buttonScript = button.GetComponent<Button>();
        }

        private void Start ()
        {
            buttonScript.interactable = false;
        }

        private void OnTutorialFinished ()
        {
            ActivateButton();
        }

        private void ActivateButton ()
        {
            buttonImage.sprite = activeButtonSprite;
            buttonScript.interactable = true;
        }
    }
}
