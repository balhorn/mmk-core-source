﻿using UnityEngine;

namespace MMK.Core.UI
{
    public class ShopWindow : PopUp
    {
        public RectTransform productsBase;
        public GameObject shopItem;

        protected override void Awake ()
        {
            base.Awake();
            ShowProducts();
        }

        private void ShowProducts()
        {
            var products = API.Shop.Products;
            foreach (var item in products) {
                var instance = Instantiate(shopItem);
                instance.transform.SetParent(productsBase, false);

                instance.GetComponent<ShopItem>().Product = item;
            }
        }
    }
}