﻿using System.Collections.Generic;
using MMK.Core;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MMK.Core.UI
{
    public class PopUp : MonoBehaviour, IUndoable
    {
        public AnimatorSwitcher backgroundSwitcher;
        private AnimatorSwitcher animatorSwitcher;

        private static readonly Stack<PopUp> PopupsStack = new Stack<PopUp>();

        protected virtual AnimatorSwitcher Switcher => animatorSwitcher ?? GetComponent<AnimatorSwitcher>();

        protected virtual void Awake ()
        {
            animatorSwitcher = GetComponent<AnimatorSwitcher>();
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }

        private void OnSceneUnloaded(Scene arg0)
        {
            if (PopupsStack.Count > 0) {
                PopupsStack.Clear();
            }

            SceneManager.sceneUnloaded -= OnSceneUnloaded;
        }

        public virtual void SwitchTo ()
        {
            if (PopupsStack.Count == 0) {
                Show();
                return;
            }

            var previous = PopupsStack.Pop();
            previous.Disable();

            PopupsStack.Push(this);
            Enable(previous.Switcher.disableAfter);
        }

        public virtual void Show ()
        {
            var delay = 0f;

            if (PopupsStack.Count > 0) {
                var popup = PopupsStack.Peek();
                delay = popup.Switcher.disableAfter;
                popup.Disable();
            } else {
                backgroundSwitcher?.On();
            }

            Enable(delay);
            PopupsStack.Push(this);
        }

        public virtual void Hide ()
        {
            Disable();

            if (PopupsStack.Count > 0) {
                PopupsStack.Pop();
            }

            if (PopupsStack.Count > 0) {
                PopupsStack.Peek().Enable(Switcher.disableAfter);
            } else {
                backgroundSwitcher?.Off();
            }
        }

        private void Enable ()
        {
            Switcher?.gameObject.SetActive(true);
            Switcher?.On();
            GameController.AddUndoable(this);
        }

        private void Enable (float afterSeconds)
        {
            if (afterSeconds < float.Epsilon) {
                Enable();
            } else {
                Invoke(nameof(Enable), afterSeconds);
            }
        }

        private void Disable ()
        {
            CancelInvoke();
            Switcher?.Off();
            GameController.PopUndoable();
        }

        public void Undo()
        {
            Hide();
        }
    }
}
