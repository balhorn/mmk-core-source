﻿using MMK.Core.InApp;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class ShopItem : MonoBehaviour
    {
        public Image productImage;

        public Text productName;
        public Text productPrice;

        public Image button;
        public Outline outline;

        public Color normalButton;
        public Color specialPriceButton;

        public IProduct Product
        {
            set
            {
                product = value;
                Populate();
            }
        }

        private IProduct product;

        private void Populate ()
        {
            productImage.sprite = product.Icon;
            productName.text = product.Name;
            productPrice.text = product.Price;
        }

        public void Purchase ()
        {
            product.Consume();
        }
    }
}
