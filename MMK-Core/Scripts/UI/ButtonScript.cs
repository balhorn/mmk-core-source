﻿using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    [RequireComponent(typeof (Button))]
    public class ButtonScript : MonoBehaviour
    {
        protected Button button;

        void Start ()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(SoundManager.ButtonClicked);
        }

        private void OnDestroy ()
        {
            button.onClick.RemoveListener(SoundManager.ButtonClicked);
        }
    }
}
