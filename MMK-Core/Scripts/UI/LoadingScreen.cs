﻿using System;
using System.Collections;
using MMK.Core;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace MMK.Core.UI
{
    public class LoadingScreen : MonoBehaviour
    {
        public enum IndicatorType
        {
            ProgressBar,
            Spinner
        }

        public string initialSceneName;
        public float fadeDuration;
        public float rotatingSpeed;

        public IndicatorType indicator;
        public GameObject spinner;
        public ProgressBarFiller progressBar;
        public Text progressText;

        private AsyncOperation loadingOperation;
        private Animator animator;

        private bool   sceneIsReady;
        private bool   sceneCallbackExecuted;
        private float  pastProgress;
        private int    frameCount;
        private string nextLevel;

        private Scene currentScene;
        private Scene sceneToUnload;

        private readonly int hashFadeParam = Animator.StringToHash("fade");
        private const int MaximumFrameCount = 100;

        public static event Delegates.VoidDelegate OnLoadingFinished;
        private static LoadingScreen instance;

        private void Update()
        {
            if (loadingOperation != null && frameCount < MaximumFrameCount) {
                var currentProgress = loadingOperation.progress;
                UpdateProgress(currentProgress);

                if (Mathf.Approximately(currentProgress, pastProgress) && ++frameCount >= MaximumFrameCount) {
                    sceneIsReady = true;
                } else if (!Mathf.Approximately(currentProgress, pastProgress)) {
                    frameCount = 0;
                }

                pastProgress = currentProgress;
            }
        }

        private void UpdateProgress (float currentProgress)
        {
            switch (indicator) {
                case IndicatorType.ProgressBar:
                    if (progressBar != null) {
                        progressBar.SetProgress(currentProgress);
                    }
                    break;
                case IndicatorType.Spinner:
                    if (spinner != null) {
                        spinner.transform.Rotate(Vector3.forward, rotatingSpeed * Time.deltaTime);
                    }
                    break;
            }

            if (progressText != null) {
                progressText.text = $"{currentProgress * 100} %";
            }
        }

        private void InitComponents()
        {
            animator = GetComponent<Animator>();
            animator.speed = 1f / fadeDuration;
            //DontDestroyOnLoad(gameObject);

            if (progressBar != null) {
                progressBar.enabled = false;
            }
            if (progressText != null) {
                progressText.enabled = false;
            }
            if (spinner != null) {
                spinner.SetActive(false);
            }
        }

        public static void Run(string sceneName)
        {
            instance?.RunInternal(sceneName);
        }

        private void RunInternal(string sceneName, bool unloadScene = true)
        {
            nextLevel = sceneName;
            StartCoroutine(Loading(unloadScene));
        }

        private IEnumerator Loading(bool unloadScene = true)
        {
            animator.SetFloat(hashFadeParam, 1f);

            yield return new WaitForSeconds(fadeDuration);
            var stopwatch = System.Diagnostics.Stopwatch.StartNew();
            loadingOperation = LoadNextScene();
            loadingOperation.allowSceneActivation = false;

            yield return new WaitUntil(() => sceneIsReady);

            SceneManager.sceneLoaded += SceneLoadedHook;

            loadingOperation.allowSceneActivation = true;

            yield return new WaitUntil(() => sceneCallbackExecuted);

            Debug.Log($"Scene loaded in {stopwatch.ElapsedMilliseconds / 1000} sec");

            if (unloadScene) {
                yield return SceneManager.UnloadSceneAsync(sceneToUnload);
            }

            yield return new WaitForBlockingOperations();

            SceneManager.sceneLoaded -= SceneLoadedHook;
            OnLoadingFinished?.Invoke();
            sceneCallbackExecuted = false;

            if (progressBar != null) {
                progressBar.SetProgress(1f);
            }

            animator.SetFloat(hashFadeParam, 0f);
            //Destroy(gameObject, fadeDuration);
        }

        private void LoadInitialScene()
        {
            RunInternal(initialSceneName, false);
        }

        private AsyncOperation LoadNextScene()
        {
            var operation = SceneManager.LoadSceneAsync(nextLevel, LoadSceneMode.Additive);
            StartCoroutine(WaitAndSetActive(operation));
            return operation;
        }

        private IEnumerator WaitAndSetActive(AsyncOperation operation)
        {
            yield return operation;
            ChangeActiveScene();
        }

        private void ChangeActiveScene()
        {
            sceneToUnload = currentScene;
            currentScene = SceneManager.GetSceneAt(SceneManager.sceneCount - 1);
            Debug.Log($"Setting scene {currentScene.name} as active");
            SceneManager.SetActiveScene(currentScene);
        }

        private void SceneLoadedHook(Scene arg0, LoadSceneMode loadSceneMode)
        {
            sceneCallbackExecuted = true;
        }

        private void Start()
        {
            InitComponents();
            LoadInitialScene();
        }

        private void Awake()
        {
            if (instance == null) {
                instance = this;
            } else {
                Destroy(gameObject);
            }
        }
    }
}
