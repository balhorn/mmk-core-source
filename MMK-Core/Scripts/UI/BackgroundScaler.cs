﻿using UnityEngine;

namespace MMK.Core.UI
{
    public class BackgroundScaler : MonoBehaviour
    {
        private const float ReferenceRatio = (float) 1080 / 1920;

        private void Start ()
        {
            var aspectRatio = (float)Screen.width / Screen.height;
            var scale = transform.localScale;

            scale = scale * aspectRatio / ReferenceRatio;
            transform.localScale = scale;
        }
    }
}
