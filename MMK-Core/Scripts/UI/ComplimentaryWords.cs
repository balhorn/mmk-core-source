﻿using System.Collections;
using UnityEngine;

namespace MMK.Core.UI
{
    public class ComplimentaryWords : MonoBehaviour
    {
        public int matchesCount;
        public float animationLength;

        private Animator animator;

        private readonly int animationHash = Animator.StringToHash("On");

        public void Show ()
        {
            gameObject.SetActive(true);
            if (animator != null) {
                animator.SetTrigger(animationHash);
            }
            StartCoroutine(ShutDown());
        }

        private IEnumerator ShutDown ()
        {
            yield return new WaitForSeconds(animationLength);
            gameObject.SetActive(false);
        }

        private void Awake ()
        {
            animator = GetComponent<Animator>();
        }
    }
}
