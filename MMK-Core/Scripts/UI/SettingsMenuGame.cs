﻿using MMK.Core.UI;

namespace MMK.Core.UI
{
    public class SettingsMenuGame : SettingsMenu
    {
        public PopUpManager popUpManager;

        public override void OnExitPressed()
        {
            ToggleMenu(false);
            popUpManager.Lose();
        }
    }
}