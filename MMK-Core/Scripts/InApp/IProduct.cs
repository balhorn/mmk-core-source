﻿using UnityEngine;

namespace MMK.Core.InApp
{
    public interface IProduct
    {
        string Id { get; }
        Sprite Icon { get; }
        int Amount { get; }
        string Name { get; }
        string Price { get; }
        bool IsInstantPurchase { get; }

        void Consume();
    }
}