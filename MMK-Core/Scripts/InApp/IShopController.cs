﻿namespace MMK.Core.InApp
{
    public interface IShopController
    {
        void StartPurchase(IProduct item);
        void PurchaseSucessfull();
        void PurchaseFailed();
    }
}