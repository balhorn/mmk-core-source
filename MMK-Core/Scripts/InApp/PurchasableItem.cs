﻿using UnityEngine;

namespace MMK.Core.InApp
{
    public abstract class PurchasableItem : ScriptableObject, IProduct
    {
        public string id;
        public Sprite icon;
        public bool mostPopular;

        public string Price { get; set; }
        public string Name { get; set; }

        public void Purchase ()
        {
            API.Shop.Purchase(this);
        }

        public bool IsInstantPurchase => false;
        public Sprite Icon => icon;
        public string Id => id;

        public abstract int Amount { get; }
        public abstract void Consume();
    }
}