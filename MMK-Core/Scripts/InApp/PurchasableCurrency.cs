﻿using UnityEngine;

namespace MMK.Core.InApp
{
    [CreateAssetMenu(menuName = "MMK/Shop/Purchasable currency")]
    public class PurchasableCurrency : PurchasableItem
    {
        public int amount;

        public override int Amount => amount;

        public override void Consume()
        {
            Player.Instance.GainCurrency(amount);
        }
    }
}