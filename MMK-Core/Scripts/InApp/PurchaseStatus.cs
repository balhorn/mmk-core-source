﻿using MMK.Core.UI;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.InApp
{
    public class PurchaseStatus : PopUp
    {
        [Space]
        public Text  header;
        public Text  message;
        public Image icon;
        public Image art;
        public Text  currencyAmount;

        [Space]
        public string successHeader;
        public string failureHeader;

        //public string successMessage;
        public string failureMessage;

        public Sprite successIcon;
        public Sprite failureIcon;

        public void Success (IProduct product)
        {
            Setup(successHeader, product.Name, successIcon, product.Icon, product.Amount);
        }

        public void Failure (Sprite icon)
        {
            Setup(failureHeader, failureMessage, failureIcon, icon, 0);
        }

        private void Setup (string header, string message, Sprite icon, Sprite art, int amount)
        {
            //Show();

            if (this.header != null) {
                this.header.text = header;
            }

            if (this.message != null) {
                this.message.text = message;
            }

            if (this.icon != null) {
                this.icon.sprite = icon;
            }

            if (this.art != null) {
                this.art.sprite = art;
            }

            if (currencyAmount != null) {
                currencyAmount.text = $"+{amount}";
            }
        }
    }
}