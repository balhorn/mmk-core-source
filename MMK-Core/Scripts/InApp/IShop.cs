﻿using System.Collections.Generic;

namespace MMK.Core.InApp
{
    public interface IShop
    {
        IEnumerable<IProduct> Products { get; }
        void Purchase(IProduct item);
    }
}