﻿using MMK.Core.UI;
using UnityEngine;

namespace MMK.Core.InApp
{
    public class PurchaseController : MonoBehaviour, IShopController
    {
        public PurchaseStatus purchaseStatus;
        public PopUp processing;

        private IProduct currentlyPurchasing;

        public void StartPurchase(IProduct item)
        {
            currentlyPurchasing = item;
            if (!item.IsInstantPurchase) {
                processing.Show();
            } else {
                PurchaseSucessfull();
            }
        }

        public void PurchaseFailed()
        {
            purchaseStatus.SwitchTo();
            purchaseStatus.Failure(currentlyPurchasing.Icon);
        }

        public void PurchaseSucessfull()
        {
            purchaseStatus.SwitchTo();
            purchaseStatus.Success(currentlyPurchasing);
        }
    }
}