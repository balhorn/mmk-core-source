﻿using System.Collections.Generic;

namespace MMK.Core.InApp
{
    public class FakeShop : IShop
    {
        public IEnumerable<IProduct> Products { get; }
        public void Purchase(IProduct item)
        {
        }
    }
}