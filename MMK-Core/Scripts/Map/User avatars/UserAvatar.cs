﻿using System.Collections;
using MMK.Core.Socials;
using MMK.Core.UI;
using UnityEngine;

namespace MMK.Core
{
    public class UserAvatar : FriendAvatar
    {
        public float movementSpeed;

        private Vector3 currentVelocity;
        private Transform cachedTransform;

        private void OnLoadingFinished ()
        {
            if (!API.Social.IsLoggedIn || API.Social.Me == null) {
                gameObject.SetActive(false);
                return;
            }

            if (NeedToMove()) {
                StartCoroutine(MovingAnimation());
            }
        }

        private bool NeedToMove ()
        {
            var lastCompletedLevel = GameController.game.LastCompletedLevel;
            return lastCompletedLevel > 0 && lastCompletedLevel < Controller.Levels.Count;
        }

        private IEnumerator MovingAnimation ()
        {
            var targetPosition = GetTargetPosition();
            var currentPosition = cachedTransform.position;

            var distance = Vector2.Distance(currentPosition, targetPosition);
            while (distance > float.Epsilon) {
                var smoothTime = distance / movementSpeed;
                currentPosition = Vector3.SmoothDamp(currentPosition, targetPosition, ref currentVelocity, smoothTime,
                    movementSpeed);
                cachedTransform.position = currentPosition;
                distance = Vector2.Distance(currentPosition, targetPosition);
                yield return null;
            }
        }

        private Vector2 GetTargetPosition()
        {
            var nextLevel = Controller.GetLevelByNumber(currentLevel + 1);
            return nextLevel.GetComponent<AvatarMarker>().AvatarPosition;
        }

        protected override void Start()
        {
            Populate();
            base.Start();
        }

        private void Populate ()
        {
            var lastCompletedLevel = GameController.game.LastCompletedLevel;
            lastCompletedLevel = Mathf.Clamp(lastCompletedLevel, 1, Controller.Levels.Count);

            Populate(API.Social.Me, lastCompletedLevel);
        }

        private void Awake ()
        {
            cachedTransform = transform;
            LoadingScreen.OnLoadingFinished += OnLoadingFinished;
        }

        private void OnDestroy ()
        {
            LoadingScreen.OnLoadingFinished -= OnLoadingFinished;
        }
    }
}