﻿using MMK.Core;
using MMK.Core.Socials;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core
{
    public class FriendAvatar : MonoBehaviour
    {
        public Image avatarPlaceholder;

        protected IFriend currentUser;
        protected int currentLevel;
        protected LevelController controller;

        protected LevelController Controller => controller ?? (controller = FindObjectOfType<LevelController>());

        public void Populate (IFriend friend, int levelNumber)
        {
            currentUser = friend;
            currentLevel = levelNumber;
            SetSprite();
        }

        protected virtual void MoveToPosition ()
        {
            var level = Controller.GetLevelByNumber(currentLevel);

            if (level == null) {
                Destroy(gameObject);
                return;
            }

            var position = level.GetComponent<AvatarMarker>().AvatarPosition;
            transform.position = position;
        }

        protected virtual void Start ()
        {
            MoveToPosition();
        }

        private void SetSprite ()
        {
            var sprite = Sprite.Create(currentUser.Avatar,
                new Rect(0f, 0f, currentUser.Avatar.width, currentUser.Avatar.height), new Vector2(0.5f, 0.5f));
            avatarPlaceholder.sprite = sprite;
        }
    }
}