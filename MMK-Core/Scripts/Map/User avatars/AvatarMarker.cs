﻿using UnityEngine;

namespace MMK.Core
{
    public class AvatarMarker : MonoBehaviour
    {
        public Transform marker;

        public Vector2 AvatarPosition => marker.position;
    }
}