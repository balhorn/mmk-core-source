﻿using System.Collections;
using MMK.Core;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MapController : MonoBehaviour
{
    public bool moveCamera = true;
    public Transform initialCameraPosition;

    [Range(0f, 10f)] public float movingSpeed = 5f;
    [Range(0f, 30f)] public float brakingSpeed = 15f;
    [Range(0f, 10f)] public float yieldingSpeed = 5f;

    public float pixelsTopOffset = 290f;
    public float pixelsBottomOffset = 290f;

    public Color yieldingBoundsColor;
    public Color strictBoundsColor;

    private new Camera camera;
    private Transform cameraTransform;
    private EventSystem eventSystem;

    [HideInInspector] public Rect yieldingBounds;
    [HideInInspector] public Rect strictBounds;
    private Vector3[] brackingCorners;
    private Vector3[] strictCorners;

    private float yCameraPosition;
    private float initialCameraY;
    private float lerpTime = 1f;
    private float currentTime;

    private EventSystem EventSystem => eventSystem ?? (eventSystem = EventSystem.current);

    private void Awake()
    {
        camera = Camera.main;
        cameraTransform = camera.transform;
        eventSystem = EventSystem.current;
    }

    private void Start ()
    {
        if (initialCameraPosition != null) {
            cameraTransform.position = initialCameraPosition.position;
        }
    }

    private void Update ()
	{
	    if (Utilites.IsPointerOverUI(EventSystem)) {
	        return;
	    }

        currentTime += Time.deltaTime;
        if (currentTime > lerpTime) {
            currentTime = lerpTime;
        }

        if (Input.GetMouseButtonDown (0)) {
            acceleration = 0f;
            previousPosition = Input.mousePosition;
            yCameraPosition = previousPosition.y;
        } else if (Input.GetMouseButton (0)) {
            if (moveCamera) {
                MoveCamera();
            }
        } else if (Input.GetMouseButtonUp (0)) {
            StartCoroutine(Braking());
            currentTime = 0f;
        }

	}

    public void FocusOn (Transform target)
    {
        if (target.position.y > TopYieldBorder && target.position.y < BottomYieldBorder) {
            return;
        }

        var localTransform = cameraTransform;
        if (target.position.y > TopYieldBorder) {
            localTransform.position = new Vector3(localTransform.position.x, TopYieldBorder, -10);
        } else if (target.position.y < BottomYieldBorder) {
            localTransform.position = new Vector3(localTransform.position.x, BottomYieldBorder, -10);
        } else {
            localTransform.position = new Vector3(localTransform.position.x, target.position.y, -10);
        }
    }

    private void OnDrawGizmos ()
    {
        brackingCorners = new Vector3[] {
            new Vector2(yieldingBounds.max.x - yieldingBounds.size.x, yieldingBounds.max.y),
            yieldingBounds.max,
            new Vector3(yieldingBounds.min.x + yieldingBounds.size.x, yieldingBounds.min.y),
            yieldingBounds.min
        };

        strictCorners = new Vector3[] {
            new Vector2(strictBounds.max.x - strictBounds.size.x, strictBounds.max.y),
            strictBounds.max,
            new Vector3(strictBounds.min.x + strictBounds.size.x, strictBounds.min.y),
            strictBounds.min
        };

        for (int i = 0; i < brackingCorners.Length; i++) {
            var iNext = i < brackingCorners.Length - 1 ? i + 1 : 0;

            Gizmos.color = yieldingBoundsColor;
            Gizmos.DrawLine(brackingCorners[i], brackingCorners[iNext]);

            Gizmos.color = strictBoundsColor;
            Gizmos.DrawLine(strictCorners[i], strictCorners[iNext]);
        }
    }

    private Vector3 previousPosition;
    private float acceleration;

    private const float Epsilon = 0.1f;
    private const float BaseAcceleration = 12f;
    private const float YieldingAcceleration = 2.5f;

    private float LerpValue
    {
        get
        {
            var t = currentTime / lerpTime;
            return 5 * Mathf.Cos(t * Mathf.PI / 3) * Time.deltaTime;
        }
    }

    private void MoveCamera()
    {
        var anchorPoint = Mathf.Lerp(yCameraPosition, previousPosition.y, LerpValue);
        var offset = (anchorPoint - Input.mousePosition.y) * movingSpeed * Time.deltaTime;
        offset = Mathf.Abs(previousPosition.y - Input.mousePosition.y) >= Epsilon ? offset : 0f;

        var currentPosition = cameraTransform.position;
        var targetPosition = currentPosition + new Vector3(0, offset);

        var lerpedPosition = Vector3.Lerp(currentPosition, targetPosition, LerpValue);

        cameraTransform.position = ValidatePosition(lerpedPosition);
        yCameraPosition = previousPosition.y;
        previousPosition = Input.mousePosition;

        acceleration = offset;
    }

    private Vector3 ValidatePosition (Vector3 lerpedPosition)
    {
        var clampedYPosition = Mathf.Clamp(
            lerpedPosition.y,
            BottomStrictBorder,
            TopStrictBorder);

        return new Vector3(lerpedPosition.x, clampedYPosition, lerpedPosition.z);
    }

    private float BrakingDirection
    {
        get
        {
            var movingRect = new Rect(yieldingBounds);
            movingRect.min = new Vector2(movingRect.min.x, movingRect.min.y + camera.orthographicSize);
            movingRect.max = new Vector2(movingRect.max.x, movingRect.max.y - camera.orthographicSize);

            var cameraPosition = cameraTransform.position;
            if (movingRect.Contains(new Vector2(cameraPosition.x, cameraPosition.y))) {
                return 1;
            }

            var border = cameraPosition.y > movingRect.yMax ? TopYieldBorder : BottomYieldBorder;
            acceleration = Mathf.Approximately(border, TopYieldBorder) ? YieldingAcceleration : - YieldingAcceleration;
            var distance = cameraPosition.y - border;
            return - Mathf.Abs(distance / acceleration);
        }
    }

    private float TopStrictBorder    => strictBounds.max.y   - camera.orthographicSize;
    private float TopYieldBorder     => yieldingBounds.max.y - camera.orthographicSize;
    private float BottomStrictBorder => strictBounds.min.y   + camera.orthographicSize;
    private float BottomYieldBorder  => yieldingBounds.min.y + camera.orthographicSize;

    private IEnumerator Braking ()
    {
        do {
            acceleration -= Mathf.Sign(acceleration) * brakingSpeed * Time.deltaTime;
            var newPosition = cameraTransform.position + BrakingDirection * new Vector3(0f, acceleration, 0f);
            cameraTransform.position = ValidatePosition(Vector3.Lerp(cameraTransform.position, newPosition, LerpValue));

            yield return null;
        } while (Mathf.Abs(acceleration) >= Epsilon);
    } 
}
