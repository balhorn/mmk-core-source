﻿using System;
using UnityEngine;

using System.IO;
using MMK.Core;
using MMK.Core.UI;

namespace MMK.Core.Aims
{
    [Serializable]
    public abstract class LevelAim : MonoBehaviour, IEventListener
    {
        public int  requiredAmount;

        public virtual Level Level { get; set; }

        protected int currentAmount; 
        protected LevelAimUI uiElement;
        protected GameObject uiPrefab;

        public virtual void InstantiateUIItem (GameObject parent)
        {
            uiPrefab = LoadPrefab();
            uiPrefab.transform.SetParent (parent.transform, false);

            uiElement = uiPrefab.GetComponent<LevelAimUI> ();
            uiElement.sprite = GetSprite(GetType().Name);
            uiElement.requiredAmount = requiredAmount;
            uiElement.Refresh ();
            uiElement.UpdateUI (currentAmount);
        }

        public virtual bool IsCompleted => currentAmount >= requiredAmount;

        public abstract void BindEvent ();
        public abstract void UnbindEvent ();

        protected abstract Sprite GetSprite(string spriteName);

        protected virtual GameObject LoadPrefab ()
        {
            var prefab = Resources.Load<GameObject>(Path.Combine(Constants.PrefabsFolder, "Level aim"));
            return Instantiate(prefab);
        }

        protected virtual void Increment ()
        {
            uiElement.UpdateUI (++currentAmount);
            if (currentAmount >= requiredAmount) {
                Level.AimRiched (this);
            }
        }

        private void OnDestroy()
        {
            UnbindEvent ();
        }


    }
}
