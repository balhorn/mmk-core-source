﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.Collections;
using MMK.Core;

public class LevelView : MonoBehaviour 
{
    public GameObject centerStar;
    public GameObject leftStar;
    public GameObject rightStar;

    public GameObject levelItem;

    public Sprite mapItemActive;
    public Sprite mapItemDisabled;

    private SpriteRenderer itemRenderer;
	private Level model;

	private Level Model => model ?? GetComponent<Level>();

    public int Number
    {
        set
        {
            var textMesh = levelItem.GetComponentInChildren<TextMesh>();
            if (textMesh == null) {
                Debug.LogError($"Failed to update number of {value}th level. Text mesh wasn't found");
                return;
            }

            if (value > 0) {
                textMesh.text = value.ToString();
                textMesh.color = Color.white;
            } else {
                textMesh.text = "Odd";
                textMesh.color = Color.red;
            }
            var meshRenderer = textMesh.GetComponent<Renderer>();

            meshRenderer.sortingLayerName = "Tokens";
            meshRenderer.sortingOrder = 1;
        }
    }

    private void Awake ()
    {
        model = GetComponent<Level> ();
        itemRenderer = levelItem.GetComponent<SpriteRenderer>();
        Number = Model.number;
    }

    private void OnLevelWasLoaded(int level)
    {
        switch (level) {
            case 1: 
                break;
            case 2:
                SetVisible(false);
                break;
        }
    }

    public void Disable()
    {
        levelItem.GetComponentInChildren<MeshRenderer>().enabled = false;
        itemRenderer.sprite = mapItemDisabled;
    }

    public void SetVisible(bool visible)
    {
        GetComponent<Collider2D> ().enabled = visible;
        foreach (var renderer in GetComponentsInChildren<Renderer>()) {
            renderer.enabled = visible;
        }
    }
        
    public void Activate()
    {
        levelItem.GetComponentInChildren<MeshRenderer>().enabled = true;
        itemRenderer.sprite = mapItemActive;
    }

    public void CompleteLevel()
    {
        switch (Model.starsCollected) {
            case 3:
                rightStar.SetActive(true);
                goto case 2;
            case 2:
                centerStar.SetActive(true);
                goto case 1;
            case 1: 
                leftStar.SetActive(true);
                break;
        }
    }

    public void UpdateView()
    {
        if (!Model.isCompleted) {
            Disable();
        }

        if (Model.starsCollected != 0) {
            CompleteLevel ();
        }
    }
}
