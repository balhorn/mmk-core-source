﻿using UnityEngine;
using UnityEngine.EventSystems;

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using MMK.Core;
using MMK.Core.UI;

namespace MMK.Core
{
    [ExecuteInEditMode]
    public abstract class LevelController : MonoBehaviour
    {
        public StartLevel startLevelPopup;
        public PopUp livesPopUp;
        public LoadingScreen loadingScreen;

        protected List<Level> levels;
        protected Level choosenLevel;

        protected EventSystem eventSystem;
        protected MapController mapController;

        protected static bool firstInit = true;

        public List<Level> Levels => levels;

        protected EventSystem EventSystem => eventSystem ?? (eventSystem = EventSystem.current);

        protected virtual void Awake()
        {
            levels = FindObjectsOfType<Level>()
                .Where(lvl => lvl.number > 0)
                .OrderBy(lvl => lvl.number)
                .ToList();
            mapController = GetComponent<MapController>();
            eventSystem = EventSystem.current;
        }

        public void Quit ()
        {
            LoadingScreen.Run(Constants.MainMenuScene);
        }

        protected virtual void Start()
        {
            InitLevels ();
            firstInit = false;
        }

        protected void Update ()
        {
            if (!Input.GetMouseButtonDown(0) || Utilites.IsPointerOverUI(EventSystem)) {
                return;
            }

            var hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.collider == null) {
                return;
            }

            var go = hit.collider.gameObject;
            if (!go.CompareTag("Level")) {
                return;
            }

            var level = go.GetComponent<Level>();
            if (level.Active) {
                ShowPopup(level);
            }
        }

        protected virtual void ShowPopup (Level level)
        {
            if (Player.Instance.Lifes <= 0) {
                livesPopUp.Show();
                return;
            }

            if (startLevelPopup != null) {
                startLevelPopup.Show();
                startLevelPopup.Setup(level, GetLevelAsset(level));
            }

            choosenLevel = level;
        }

        protected abstract PremadeLevel GetLevelAsset(Level level);

        protected void ShowPopup ()
        {
            if (!PlayerPrefs.HasKey(Level.NextLevelPrefsKey)) {
                return;
            }

            try {
                var level = GetLevelByNumber(PlayerPrefs.GetInt(Level.NextLevelPrefsKey));
                ShowPopup(level);
            } finally {
                PlayerPrefs.DeleteKey(Level.NextLevelPrefsKey);
            }
        }

        public void StartGame ()
        {
            PrepareLevel();
            LoadingScreen.Run(Constants.MainGameScene);
        }

        protected virtual void PrepareLevel ()
        {
            choosenLevel.View.SetVisible(false);

            var levelObject = Instantiate(choosenLevel.gameObject);
            Level.Current = levelObject.GetComponent<Level>();
            Level.Current.premadeLevel = GetLevelAsset(Level.Current);
            DontDestroyOnLoad(levelObject);
        }

        public void ActivateNextLevel()
        {
            var lastCompleted = GetLastCompletedLevel ();

            if (lastCompleted == null) {
                GetLevelByNumber (1).Active = true;
            } else {
                var nextAfterCompleted = GetNextLevel(lastCompleted);
                if (nextAfterCompleted == null) {
                    return;
                }
                nextAfterCompleted.Active = true;

                mapController.FocusOn(nextAfterCompleted.transform);

                if (!firstInit) {
                    ShowPopup();
                }
            }
        }

        public Level GetNextLevel(Level current)
        {
            try {
                return levels[current.number];
            } catch (ArgumentOutOfRangeException) {
                return null;
            }
        }

        public Level GetPreviousLevel(Level current)
        {
            try {
                return levels[current.number - 2];
            } catch (ArgumentOutOfRangeException) {
                return null;
            }
        }

        public Level GetLevelByNumber(int number)
        {
            try {
                return levels[number - 1];
            } catch (ArgumentOutOfRangeException) {
                return null;
            }
        }

        public Level GetLastCompletedLevel()
        {
            try {
                return levels
                    .Where (lvl => lvl.isCompleted)
                    .Max ();
            } catch (InvalidOperationException) {
                return null;
            }
        }

        protected virtual void InitLevels()
        {
            if (!Application.isPlaying) {
                return;
            }

            var levelsData = GameController.Levels;

            try {
                for (int i = 0; i < levels.Count; i++) {
                    var level = GetLevelByNumber(i + 1);

                    if (i < levelsData.Count) {
                        level.AdjustSavedData(levelsData[i]);
                    }

                    level.View.UpdateView();
                }
            } catch (Exception e) {
                Debug.Log("Error during applying saved levels data");
                Debug.Log(e);
            }

            ActivateNextLevel ();
        }


    }
}