﻿using UnityEngine;
using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using MMK.Core.Aims;

namespace MMK.Core
{
    [ExecuteInEditMode]
    public abstract class Level : MonoBehaviour, IComparable<Level>, IEquatable<Level>
    {
        public int number;

        /// <summary>
        /// Scriptable object storing all data for this level
        /// </summary>
        public PremadeLevel premadeLevel;

        [HideInInspector] public bool isCompleted;
        [HideInInspector] public int  starsCollected;
        [HideInInspector] public int  highScore;

        [HideInInspector] public bool isTestMode;

        public const string NextLevelPrefsKey = "nextLevelNumber";

        private LevelView view;
        private AbstractLevelBuilder builder;
        private LevelController controller;
        private Constraint constraint;

        [HideInInspector]
        public LevelAim[] aims;

        private static Level current;
        public static Level Current
        {
            get { return current ?? (current = FindObjectOfType<Level>()); }
            set { current = value; }
        }

        public RuntimeAnimatorController TutorialController => PremadeLevel.tutorialAnimator;
        public PremadeLevel PremadeLevel => premadeLevel ?? LoadPremadeLevel<PremadeLevel>();

        public LevelView View => view ?? GetComponent<LevelView>();
        public AbstractLevelBuilder Builder => builder ?? (builder = GetComponent<AbstractLevelBuilder>());
        private LevelController Controller => controller ?? (controller = FindObjectOfType<LevelController>());

        public abstract TLevelType LoadPremadeLevel<TLevelType>() where TLevelType : PremadeLevel;

        public virtual void AimRiched(LevelAim levelAim)
        {
            levelAim.UnbindEvent();
            Constraint.IsLevelCompleted = aims.All (aim => aim.IsCompleted);
        }

        public int Number
        {
            get { return number; }
            set
            {
                number = value;
                try {
                    View.Number = value;
                } catch (NullReferenceException) {
                    Debug.LogError($"Failed to update number of {value}th level");
                }
            }
        }

        private bool active;

        public bool Active
        {
            get { return active; }
            set
            {
                active = value;
                if (active) {
                    View.Activate ();
                } else {
                    View.Disable();
                }

            }
        }

        public Constraint Constraint => Constraint.Current;

        public void AdjustSavedData(LevelProperties properties)
        {
            isCompleted = properties.isCompleted;
            highScore = properties.highScore;
            starsCollected = properties.starsCollected;

            active = isCompleted;
        }

        public void RecordHighScore()
        {
            var newHighScore = ScoreManager.HighScore;
            if (highScore < newHighScore) {
                highScore = newHighScore;
            }
        }

        public virtual void UpdateStars()
        {
            var score = ScoreManager.Score;
            if (score >= LevelConfig.threeStarsScore) {
                starsCollected = 3;
            } else if (score >= LevelConfig.twoStarsScore) {
                starsCollected = starsCollected <= 2 ? 2 : starsCollected;
            } else if (score >= LevelConfig.oneStarScore) {
                starsCollected = starsCollected <= 1 ? 1 : starsCollected;
            }
        }

        protected virtual void OnDestroy ()
        {
            if (Current != null && gameObject == Current.gameObject) {
                Current = null;
            }
        }

        protected virtual void Start()
        {
            LevelConfig.winBonusEnabled = true;

            view = GetComponent<LevelView> ();
        }

        #region Map editing

        public List<Collider2D> Colliders { get; private set; }

        public float radius = 2f;

        private readonly Color halfTransparenYellow = new Color(1f, 0.92f, 0.016f, 0.5f);

        private void OnDrawGizmosSelected ()
        {
            try {
                Gizmos.color = Color.red;
                if (Colliders == null) {
                    return;
                }

                if (Colliders.Count == 0) {
                    return;
                }

                if (Colliders.Count == 1) {
                    if (Colliders[0].transform.position.y > transform.position.y) {
                        DrawArrow(transform, Colliders[0].transform);
                    } else {
                        DrawArrow(Colliders[0].transform, transform);
                    }
                }

                if (Colliders.Count >= 2) {
                    Colliders = Colliders.GetRange(0, 2);
                    DrawArrow(transform, Colliders[1].transform);
                    DrawArrow(Colliders[0].transform, transform);
                }
            } catch (Exception e) {
                Debug.LogWarning(e);
            }
        }

        private void OnDrawGizmos ()
        {
            try {
                if (Application.isPlaying) {
                    return;
                }

                Gizmos.color = halfTransparenYellow;
                var previous = Controller.GetPreviousLevel(this);

                if (previous == null) {
                    return;
                }

#if UNITY_EDITOR
        if (UnityEditor.Selection.activeGameObject == gameObject
         || UnityEditor.Selection.activeGameObject == previous.gameObject) {
            return;
        }
#endif

                if (previous.number != 0) {
                    DrawArrow(previous.transform, transform);
                }
            } catch (Exception e) {
                Debug.LogWarning(e);
            }
        }

        public static void DrawArrow (Transform from, Transform to)
        {
            var start = from.position;
            var end = to.position;
            var lerped = Vector3.Lerp(start, end, 0.7f);
            var one = new Vector3(end.y - start.y, -(end.x - start.x)).normalized * 0.2f + lerped;
            var another = new Vector3(-(end.y - start.y), end.x - start.x).normalized * 0.2f + lerped;

            Gizmos.DrawLine(start, end);
            Gizmos.DrawLine(end, one);
            Gizmos.DrawLine(end, another);
        }

        #endregion

        public int CompareTo (Level other)
        {
            if (number > other.number) {
                return 1;
            }

            if (number == other.number) {
                return 0;
            }

            return -1;
        }

        public bool Equals(Level other)
        {
            if (ReferenceEquals(null, other)) {
                return false;
            }
            if (ReferenceEquals(this, other)) {
                return true;
            }
            return number == other.number;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) {
                return false;
            }
            if (ReferenceEquals(this, obj)) {
                return true;
            }
            if (obj.GetType() != this.GetType()) {
                return false;
            }
            return Equals((Level) obj);
        }

        public override int GetHashCode()
        {
            unchecked {
                return (base.GetHashCode() * 397) ^ number;
            }
        }
    }
}