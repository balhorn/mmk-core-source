﻿using UnityEngine;

namespace MMK.Core
{
    public class CoreLevelController : LevelController
    {
        protected override PremadeLevel GetLevelAsset(Level level)
        {
            return ScriptableObject.CreateInstance<PremadeLevel>();
        }
    }
}