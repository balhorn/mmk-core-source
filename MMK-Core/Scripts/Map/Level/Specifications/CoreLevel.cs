﻿using UnityEngine;

namespace MMK.Core
{
    public class CoreLevel : Level
    {
        public override TLevelType LoadPremadeLevel<TLevelType>()
        {
            return ScriptableObject.CreateInstance<TLevelType>();
        }
    }
}