﻿using UnityEngine;

namespace MMK.Core
{
    public abstract class AbstractLevelBuilder : MonoBehaviour, ILevelBuilder
    {
        public PremadeLevel premadeLevel;

        private Level level;

        protected Vector2 fieldOrigin;

        public Vector2 FieldOrigin => fieldOrigin;

        public PremadeLevel PremadeLevel
        {
            set { premadeLevel = value; }
        }

        public Level Level
        {
            get { return level ?? (level = GetComponent<Level>()); }
            set { level = value; }
        }

        public abstract void BuildPremadeLevel();
    }
}