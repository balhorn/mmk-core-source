﻿using UnityEngine;
using System.Collections.Generic;

public static class LevelConfig
{
    public static int turns;
    public static int rows;
    public static int columns;


    public static int oneStarScore;
    public static int twoStarsScore;
    public static int threeStarsScore;

    public static bool winBonusEnabled = true;
    public static bool withIngridient;

    public static string quizQuestion;
    public static string answer;
}
