﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using MMK.Core.UI;

public class ScoreManager : MonoBehaviour
{
    private int score;

    public Text scoreText;
    public LevelProgressBar progressBar;
    public GameObject scoreTextPrefab;
    public ComplimentaryWords[] complimentaryWords;

	public static event Delegates.WithIntParam OnScoreAdded;

    protected static ScoreManager instance;

    public static int HighScore  { get; private set; }

    public static int Score => instance.score;


    public static void RecordScore (IScoreEntity entity)
    {
        if (entity == null) {
            return;
        }

        instance.RecordScoreInternal(entity);
    }

    protected virtual void RecordScoreInternal(IScoreEntity scoreEntity)
    {
        score += scoreEntity.Score;
        SpawnScoreText(scoreEntity);
        FireEvent(scoreEntity.Score);
        UpdateProgressBar(scoreEntity.Score);
        UpdateHighScore();
        ShowScore();
    }

    protected virtual void Awake ()
    {
        if (instance != null) {
            Destroy(this);
            return;
        }

        instance = this;
    }

    protected virtual void Start()
    {
        HighScore = Level.Current.highScore;
    }

    private void UpdateProgressBar(int amount)
    {
        progressBar?.UpdateProgress(amount);
    }

    private void FireEvent(int amount)
    {
        OnScoreAdded?.Invoke(amount);
    }

    private void UpdateHighScore()
    {
        if (score > HighScore) {
            HighScore = score;
        }
    }



    private void SpawnScoreText (IScoreEntity entity)
    {
        if (scoreTextPrefab == null) {
            return;
        }

        var textInstance = Instantiate(scoreTextPrefab);
        var textTransform = textInstance.transform as RectTransform;

        if (textTransform == null) {
            return;
        }

        //textTransform.SetParent(uiCanvas.transform, false);
        textTransform.position = entity.Coordinates;

        var textComponent = textInstance.GetComponentInChildren<Text>();
        if (textComponent == null) {
            return;
        }

        textComponent.text = entity.Score.ToString();
        textComponent.color = entity.TextColor;

        textTransform.positionTo(1f, textTransform.position + new Vector3(0f, 0.4f, 0f));
        var targetColor = textComponent.color;
        targetColor.a = 0f;
        Go.to(textComponent, 1f, new GoTweenConfig().colorProp("color", targetColor));

        //TODO Remove hardcoded delay
        Destroy(textInstance, 2f);
    }

    public void ShowScore()
    {
        scoreText.text = score.ToString();
    }

    private void OnDestroy ()
    {
        instance = null;
    }
}
