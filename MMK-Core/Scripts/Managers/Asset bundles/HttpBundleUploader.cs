﻿using System;
using UnityEngine;
using UnityEngine.Networking;

namespace MMK.Core
{
    public class HttpBundleUploader : EditorBundlesUploader
    {
        public HttpBundleUploader (int bundleSize)
        {
            levelsPerLocation = bundleSize;
        }

        public override void UploadBundles(string sourceDirectory, Uri targetUri, string platform)
        {
            var archive = ArchivateDirectory(sourceDirectory);

            var formData = new WWWForm();
            formData.AddField("name_format", Constants.AssetBundleNameFormat);
            formData.AddField("bundle_size", levelsPerLocation);
            formData.AddField("platform", platform);
            formData.AddBinaryData("archive", archive, "archive.zip");

            using (var request = UnityWebRequest.Post(targetUri.ToString(), formData)) {
                SendRequest(request);
            }
        }

        private void SendRequest (UnityWebRequest request)
        {
            var operation = request.Send();

            while (!operation.isDone) {
            }

            if (request.isError) {
                Debug.LogError("An error occured with the request");
                Debug.LogError(request.error);
            } else {
                Debug.Log(request.downloadHandler.text);
            }
        }

        private byte[] ArchivateDirectory (string directoryName)
        {
            return AssetBundleManager.compression(directoryName);
        }
    }
}