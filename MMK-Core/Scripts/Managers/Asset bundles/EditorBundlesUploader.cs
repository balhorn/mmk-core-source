﻿using System;
using System.Collections.Generic;

namespace MMK.Core
{
    public abstract class EditorBundlesUploader
    {
        protected int levelsPerLocation;

        public abstract void UploadBundles(string sourceDirectory, Uri targetUri, string platform);
    }
}