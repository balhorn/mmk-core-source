﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace MMK.Core
{
    public abstract class EditorBundlesLoader
    {
        protected string LevelsFolder => Constants.LevelAssetBundleNameFormat.Split('/')[0];

        public virtual List<T> GetLevelsFromBundles<T>(Uri uri, string platform) where T : PremadeLevel
        {
            var result = new List<T>();
            var bundles = LoadBundles(uri, platform);

            foreach (var bundle in bundles) {
                var jsonFiles = bundle.LoadAllAssets<TextAsset>();
                foreach (var json in jsonFiles) {
                    var level = ScriptableObject.CreateInstance<T>();
                    JsonUtility.FromJsonOverwrite(json.text, level);
                    result.Add(level);
                }
            }

            result.Sort((lvl1, lvl2) => lvl1.number.CompareTo(lvl2.number));
            return result;
        }

        public abstract List<AssetBundle> LoadBundles(Uri uri, string platform);
    }
}