﻿using System;
using System.Collections.Generic;

namespace MMK.Core
{
    public static class AssetBundleManager
    {
        public static string assetBundlesUri;

        public static Delegates.CompressionDelegate compression;
        public static Delegates.DecompressionDelegate decompression;

        public static List<T> LoadLevelsFromAssetBundles<T>(int levelsPerLocation, string platform) where T : PremadeLevel
        {
            var uri = new Uri(assetBundlesUri);
            var loader = GetBundlesLoader(uri, levelsPerLocation);

            return loader.GetLevelsFromBundles<T>(uri, platform);
        }

        public static void UpdateAssetBundles (string sourceLocation, int levelsPerLocation, string platform)
        {
            var uri = new Uri(assetBundlesUri);
            var uploader = GetBundleUploader(uri, levelsPerLocation);

            uploader.UploadBundles(sourceLocation, uri, platform);
        }

        private static EditorBundlesUploader GetBundleUploader (Uri uri, int levelsPerLocation)
        {
            return new HttpBundleUploader(levelsPerLocation);
        }

        private static EditorBundlesLoader GetBundlesLoader (Uri uri, int levelsPerLocation)
        {
            return new HttpBundleLoader(levelsPerLocation);
        }
    }
}