﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace MMK.Core
{
    public class HttpBundleLoader : EditorBundlesLoader
    {
        private const string QueryFormat = "?name_format={0}&bundle_size={1}&platform={2}";

        private readonly int bundleSize;

        public HttpBundleLoader(int bundleSize)
        {
            this.bundleSize = bundleSize;
        }

        public override List<AssetBundle> LoadBundles(Uri uri, string platform)
        {
            List<AssetBundle> result;
            var url = uri + string.Format(QueryFormat, Constants.AssetBundleNameFormat, bundleSize, platform);
            var request = UnityWebRequest.Get(url);

            try {
                var bundleUrl = FetchBundleUrl(request);
                request = UnityWebRequest.Get(GetUrl(uri) + "/" + bundleUrl);
                var bundleZip = FetchBundleZip(request);
                result = FetchAndSaveBundles(bundleZip);
            } catch (Exception e) {
                Debug.Log(e);
                result = new List<AssetBundle>();
            } finally {
                request.Dispose();
            }

            return result;
        }

        private static string GetUrl (Uri uri)
        {
            return uri.AbsoluteUri.Replace(uri.AbsolutePath, string.Empty);
        }

        private string FetchBundleUrl (UnityWebRequest request)
        {
            var operation = request.Send();
            while (!operation.isDone) {
            }

            if (request.isError || request.responseCode != 200) {
                Debug.Log(request.isError ? request.error : request.downloadHandler.text);
                throw new Exception("Error fetching bundle zip");
            }

            var response = JsonUtility.FromJson<BundleUrlResponse>(request.downloadHandler.text);
            if (string.IsNullOrEmpty(response.path)) {
                Debug.Log(request.downloadHandler.text);
                throw new Exception("Failed to fetch bundle url");
            }

            return response.path;
        }

        private byte[] FetchBundleZip (UnityWebRequest request)
        {
            var operation = request.Send();
            while (!operation.isDone) {
            }

            if (request.isError) {
                Debug.Log(request.error);
                throw new Exception("Error fetching bundle zip");
            }

            Debug.Log("Handler text: " + request.downloadHandler.text);

            return request.downloadHandler.data;
        }

        protected virtual List<AssetBundle> FetchAndSaveBundles (byte[] zip)
        {
            var result = new List<AssetBundle>();
            var bundleBuffers = AssetBundleManager.decompression(zip);

            foreach (var bundleBuffer in bundleBuffers) {
                result.Add(AssetBundle.LoadFromMemory(bundleBuffer));
            }

            return result;
        }

        [Serializable]
        private struct BundleUrlResponse
        {
            public string path;
        }
    }
}