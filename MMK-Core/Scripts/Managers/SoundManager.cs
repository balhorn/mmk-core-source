﻿using UnityEngine;
using System.Collections;
using MMK.Core;

public class SoundManager : MonoBehaviour
{
    public static bool MusicOn
    {
        get { return !instance.music.mute; }
        set { instance.music.mute = !value; }
    }

    public static bool SoundOn
    {
        get { return !instance.sounds.mute; }
        set { instance.sounds.mute = !value; }
    }

    public static void LevelStart ()
    {
        if (instance == null) {
            Debug.Log("Sound manager instance does not exist");
            return;
        }

        instance.Clip = Player.Settings.levelStartSound;
    }

    public static void Win ()
    {
        if (instance == null) {
            Debug.Log("Sound manager instance does not exist");
            return;
        }

        instance.Clip = Player.Settings.winSound;
    }

    public static void Shuffle ()
    {
        if (instance == null) {
            Debug.Log("Sound manager instance does not exist");
            return;
        }

        instance.Clip = Player.Settings.shuffleSound;
    }

    public static void ButtonClicked ()
    {
        if (instance == null) {
            Debug.Log("Sound manager instance does not exist");
            return;
        }

        instance.Clip = Player.Settings.buttonTapSound;
    }

    private static GameSettings Settings => instance.settings ?? (instance.settings = Player.Settings);

    protected AudioClip Clip
    {
        set
        {
            sounds.clip = value;
            sounds.Play();
        }
    }

    protected static SoundManager instance;

    private AudioSource music;
    private AudioSource sounds;

    private GameSettings settings;

    private void Awake ()
    {
        InitSingleton();
        //settings = Resources.Load<GameSettings>("GameSettings");
    }

    private void Start ()
    {
        InitSources();
    }

    private void InitSingleton()
    {
        if (instance != null) {
            Destroy(this);
            return;
        }

        instance = this;
    }

    private void InitSources()
    {
        music = gameObject.AddComponent<AudioSource>();
        music.clip = Player.Settings.backgroundMusic;
        music.volume = 0.228f;
        music.loop = true;
        music.Play();

        sounds = gameObject.AddComponent<AudioSource>();
    }

    private void OnDestroy ()
    {
        instance = null;
    }
}
