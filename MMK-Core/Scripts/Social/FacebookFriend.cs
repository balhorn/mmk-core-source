﻿using UnityEngine;

namespace MMK.Core.Socials
{
    public class FacebookFriend : IFriend
    {
        public string Id { get; }
        public string Name { get; }
        public string LastName { get; }
        public Texture2D Avatar { get; }
        public bool IsPlaying { get; }

        public FacebookFriend(string id, string name, string lastName, Texture2D avatar, bool isPlaying)
        {
            Id = id;
            Name = name;
            LastName = lastName;
            Avatar = avatar;
            IsPlaying = isPlaying;
        }

        public override string ToString()
        {
            return $"{Name} {LastName}";
        }
    }
}