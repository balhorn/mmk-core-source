﻿using UnityEngine;

namespace MMK.Core.Socials
{
    public class WaitForRequest : CustomYieldInstruction
    {
        public override bool keepWaiting => !request.IsDone;

        private readonly IFriendRequest request;

        public WaitForRequest(IFriendRequest request)
        {
            this.request = request;
        }
    }
}