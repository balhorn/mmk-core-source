﻿using UnityEngine;

namespace MMK.Core.Socials
{
    public interface IFriend
    {
        string Id { get; }

        string Name { get; }

        string LastName { get; }

        Texture2D Avatar { get; }

        bool IsPlaying { get; }
    }
}