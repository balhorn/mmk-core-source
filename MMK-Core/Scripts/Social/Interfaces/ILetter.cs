﻿using System;

namespace MMK.Core.Socials
{
    public interface ILetter
    {
        string Id { get; }

        string Header { get; }

        string Text { get; }

        long ObjectId { get; }

        IFriend Sender { get; }

        DateTime SendTime { get; }
    }
}