﻿using System.Collections.Generic;

namespace MMK.Core.Socials
{
    /// <summary>
    /// Provides general interface to the social networks
    /// </summary>
    public interface ISocialNetwork
    {
        bool IsLoggedIn { get; }

        bool IsInitialized { get; }

        IFriend Me { get; }

        void Login();

        void Logout();

        void LoadData();

        void InviteFriends(IEnumerable<string> recepients = null);

        IEnumerable<IFriend> GetFriends();

        IEnumerable<IFriend> GetInvitableFriends();

        IFriendRequest GetInvitableFriends(int currentOffset, int limit);

        void SendLetter(ILetter letter, params string[] to);

        IEnumerable<ILetter> GetLetters();

        void DeleteLetters(string[] ids);

        void ReceiveFakeLetter(ILetter letter);
    }
}

