﻿namespace MMK.Core.Socials
{
    public interface ISocialManager
    {
        void Connect();

        void CancelConnection(bool isError);

        void Disconnect();

        void InviteFriends();

        void SendLifes();

        void SendCurrency();

        void AskForLifes();

        void ShowMessages();
    }
}