﻿using System.Collections.Generic;

namespace MMK.Core.Socials
{
    public interface IFriendRequest
    {
        bool IsDone { get; }
        string Error { get; }
        IEnumerable<IFriend> Friends { get; }
    }
}