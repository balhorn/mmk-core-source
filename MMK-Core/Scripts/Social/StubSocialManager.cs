﻿namespace MMK.Core.Socials
{
    public class StubSocialManager : ISocialManager
    {
        public void Connect()
        {
        }

        public void CancelConnection(bool isError)
        {
        }

        public void Disconnect()
        {
        }

        public void InviteFriends()
        {
        }

        public void SendLifes()
        {
        }

        public void SendCurrency()
        {
        }

        public void AskForLifes()
        {
        }

        public void ShowMessages()
        {
        }
    }
}