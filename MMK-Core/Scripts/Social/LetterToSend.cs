﻿using System;
using UnityEngine;

namespace MMK.Core.Socials
{
    [CreateAssetMenu(menuName = "MMK/Social/Letter", fileName = "New letter")]
    public class LetterToSend : ScriptableObject, ILetter
    {
        public string header;

        [TextArea]
        public string text;

        public SocialObject objectToSend;

        public long ObjectId => objectToSend?.Id ?? -1;

        public string Header => header;

        public string Text => text;

        public IFriend Sender => null;

        public string Id => "";

        public DateTime SendTime => DateTime.MinValue;
    }
}