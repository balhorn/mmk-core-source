﻿using System.Collections.Generic;

namespace MMK.Core.Socials
{
    public class FakeSocial : ISocialNetwork
    {
        public bool IsLoggedIn { get; }
        public bool IsInitialized { get; }
        public IFriend Me { get; }
        public void Login()
        {
        }

        public void Logout()
        {
        }

        public void LoadData()
        {
        }

        public void InviteFriends(IEnumerable<string> recepients = null)
        {
        }

        public IEnumerable<IFriend> GetFriends()
        {
            yield break;
        }

        public IEnumerable<IFriend> GetInvitableFriends()
        {
            yield break;
        }

        public IFriendRequest GetInvitableFriends(int currentOffset, int limit)
        {
            return null;
        }

        public void SendLetter(ILetter letter, params string[] to)
        {
        }

        public IEnumerable<ILetter> GetLetters()
        {
            yield break;
        }

        public void DeleteLetters(string[] ids)
        {
        }

        public void ReceiveFakeLetter(ILetter letter)
        {
        }

        public int NewLettersCount { get; }
    }
}