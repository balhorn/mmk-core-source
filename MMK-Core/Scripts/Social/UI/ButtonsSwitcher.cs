﻿using System.Collections;
using UnityEngine;

namespace MMK.Core.Socials.UI
{
    public class ButtonsSwitcher : MonoBehaviour
    {
        public GameObject playerConnected;
        public GameObject playerNotConnected;

        public void Switch (bool isPlayerConnected)
        {
            if (playerConnected != null) {
                playerConnected.SetActive(isPlayerConnected);
            }
            if (playerNotConnected != null) {
                playerNotConnected.SetActive(!isPlayerConnected);
            }
        }

        private void Start ()
        {
            StartCoroutine(SwitchingCoroutine());
        }

        private IEnumerator SwitchingCoroutine ()
        {
            yield return new WaitUntil(() => API.Social.IsInitialized);
            Switch(API.Social?.IsLoggedIn ?? false);
        }
    }
}