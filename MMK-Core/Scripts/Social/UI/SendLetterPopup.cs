﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMK.Core.UI;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.Socials.UI
{
    public class SendLetterPopup : PopUp
    {
        public RectTransform friendsGrid;
        public GameObject friendItemPrefab;
        public Toggle selectAllToggle;
        public LetterToSend letter;

        protected readonly HashSet<FriendUIItem> selectedFriends = new HashSet<FriendUIItem>();
        protected readonly HashSet<FriendUIItem> allFriends = new HashSet<FriendUIItem>();

        protected Vector2 initialPosition;

        public void SelectFriend (FriendUIItem friend, bool isChecked)
        {
            if (isChecked) {
                selectedFriends.Add(friend);

                if (selectedFriends.Count == allFriends.Count && !selectAllToggle.isOn) {
                    selectAllToggle.isOn = true;
                }
            } else {
                selectedFriends.Remove(friend);

                if (selectedFriends.Count == 0 && selectAllToggle.isOn) {
                    selectAllToggle.isOn = false;
                }
            }
        }

        public void SelectAll (bool isChecked)
        {
            foreach (var friend in allFriends) {
                friend.checkbox.isOn = isChecked;
            }
        }

        public void SelectAll ()
        {
            selectAllToggle.isOn = !selectAllToggle.isOn;
        }

        public void FilterFriends (string filter)
        {
            foreach (var friendUiItem in allFriends) {
                var satisfyFilter = friendUiItem.Name.IndexOf(filter, StringComparison.OrdinalIgnoreCase) >= 0;
                friendUiItem.gameObject.SetActive(satisfyFilter);
            }
        }

        public virtual void Submit ()
        {
            var recepients = selectedFriends.Where(item => item.gameObject.activeSelf).Select(friend => friend.Id.ToString()).ToArray();
            API.Social?.SendLetter(letter, recepients);
        }

        protected override void Awake()
        {
            base.Awake();
            initialPosition = friendsGrid.anchoredPosition;

            ClearAll();
            ShowFriends();
        }

        private void ClearAll()
        {
            foreach (Transform child in friendsGrid) {
                Destroy(child.gameObject);
            }
        }

        protected virtual void ShowFriends()
        {
            var loadedFriends = GetLoadedFriends();
            foreach (var friend in loadedFriends) {
                var instance = Instantiate(friendItemPrefab);
                instance.transform.SetParent(friendsGrid, false);

                var uiItem = instance.GetComponent<FriendUIItem>();
                uiItem.Init(friend);
                uiItem.ParentPopup = this;
                allFriends.Add(uiItem);
                selectedFriends.Add(uiItem);
            }

            friendsGrid.anchoredPosition = initialPosition;
        }

        protected virtual IEnumerable<IFriend> GetLoadedFriends ()
        {
            return API.Social.GetFriends();
        }
    }
}