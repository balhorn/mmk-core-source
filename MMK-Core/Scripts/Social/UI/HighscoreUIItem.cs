﻿using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.Socials.UI
{
    public class HighscoreUIItem : MonoBehaviour
    {
        public Image avatarHolder;
        public Text nameText;
        public Text scoreText;
        public Text numberText;

        public int Number
        {
            set { numberText.text = value.ToString(); }
        }

        public void Populate (string name, Texture2D avatar, int score = -1)
        {
            Populate(name, CreateSprite(avatar), score);
        }

        public void Populate (string name, Sprite avatar, int score = -1)
        {
            nameText.text = name;
            avatarHolder.sprite = avatar;
            scoreText.text = score.ToString();
        }

        private Sprite CreateSprite (Texture2D texture)
        {
            var rect = new Rect(0, 0, texture.width, texture.height);
            return Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f));
        }
    }
}