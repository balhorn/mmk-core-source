﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.Socials.UI
{
    public class MessagesWindow : MonoBehaviour
    {
        public Text messagesCountText;
        public RectTransform messagesBase;
        public int maximumAvatarsOnMessages = 5;

        public void ShowMessages ()
        {
            var groupedMessages = API.Social.GetLetters()
                .Where(letter => letter.ObjectId > 0)
                .Cast<FacebookMessage>()
                .GroupBy(message => message.AttachedObject)
                .ToList();

            Debug.Log($"Found {groupedMessages.Count} letters with objects");

            if (messagesCountText != null) {
                messagesCountText.text = groupedMessages.Count.ToString();
            }

            foreach (var messageItem in groupedMessages) {
                Debug.Log($"Object {messageItem.Key}");

                var messageInstance = Instantiate(messageItem.Key.messageItemPrefab);
                messageInstance.transform.SetParent(messagesBase, false);

                var messageUiItem = messageInstance.GetComponent<MessageUIItem>();
                messageUiItem.MaximumAvatars = maximumAvatarsOnMessages;
                messageUiItem.AttachedObject = messageItem.Key;
                messageUiItem.Letters = messageItem;
            }
        }
    }
}