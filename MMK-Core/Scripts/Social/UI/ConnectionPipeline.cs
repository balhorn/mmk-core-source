﻿using System.Collections;
using MMK.Core.UI;
using UnityEngine;

namespace MMK.Core.Socials.UI
{
    public class ConnectionPipeline : MonoBehaviour
    {
        public PopUp connecting;
        public PopUp connected;
        public PopUp disconnected;

        private IEnumerator connectionCoroutine;
        private ButtonsSwitcher[] switches;

        public void Connect ()
        {
            connectionCoroutine = ConnectionCoroutine();
            StartCoroutine(connectionCoroutine);
        }

        public void Disconnect (bool isError)
        {
            StopCoroutine(connectionCoroutine);
            connecting.Hide();
            SwitchButtons(false);
            if (!isError) {
                disconnected.Show();
            }
        }

        private IEnumerator ConnectionCoroutine ()
        {
            connecting.Show();

            yield return new WaitUntil(() => API.Social.IsLoggedIn);

            connecting.Hide();
            connected.Show();

            SwitchButtons(true);
        }

        private void SwitchButtons (bool isPlayerLoggedIn)
        {
            foreach (var buttonsSwitcher in switches) {
                buttonsSwitcher.Switch(isPlayerLoggedIn);
            }
        }

        private void Awake ()
        {
            switches = FindObjectsOfType<ButtonsSwitcher>();
        }

        private void Start ()
        {
            SwitchButtons(API.Social.IsLoggedIn);
        }
    }
}