﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.Socials.UI
{
    public class Highscores : MonoBehaviour
    {
        public GameObject friendPrefab;
        public RectTransform baseTransform;
        public ScrollRect scrollRect;
        public float scrollingDelta = 0.2f;

        private GameSettings Settings => Player.Settings;
        private PremadeLevel currentLevel;
        private int highscore;
        private Vector2 initialPosition;

        private readonly SortedList<int, HighscoreUIItem> sortedFaces = new SortedList<int, HighscoreUIItem>(new DescendingComparer());

        public void Populate (PremadeLevel level, int highscore)
        {
            currentLevel = level;
            this.highscore = highscore;

            ClearAll();
            GenerateFaces();

            baseTransform.anchoredPosition = initialPosition;
        }

        public void Scroll (float direction)
        {
            if (scrollRect == null) {
                return;
            }

            direction = Mathf.Sign(direction);
            StartCoroutine(ScrollingRoutine(direction));
        }

        private void GenerateFaces ()
        {
            //Create profile picture
            var profile = API.Social.Me;
            var profileItem = CreateNewItem();
            profileItem.Populate(profile.Name, profile.Avatar, highscore);
            sortedFaces.Add(highscore, profileItem);

            //Create friends pictures
            var index = 0;
            foreach (var friend in API.Social.GetFriends()) {
                var uiItem = CreateNewItem();
                var score = GetRandomScore();
                uiItem.Populate(friend.Name, friend.Avatar, score);
                sortedFaces.Add(score, uiItem);
                index++;
            }


            if (index >= Settings.maximumFakeFriends) {
                SetFaceNumbers();
                return;
            }

            //Add some fake friends, if needed
            for (; index < Settings.maximumFakeFriends; index++) {
                var uiItem = CreateNewItem();
                var fakeFriend = Settings.fakeFriends[index];
                var score = GetRandomScore();
                uiItem.Populate(fakeFriend.name, fakeFriend.avatar, score);
                sortedFaces.Add(score, uiItem);
            }

            SetFaceNumbers();
        }

        private void SetFaceNumbers ()
        {
            var index = 1;
            foreach (var item in sortedFaces) {
                item.Value.Number = index++;
                item.Value.transform.SetParent(baseTransform, false);
            }
        }

        private void ClearAll ()
        {
            sortedFaces.Clear();
            foreach (Transform child in baseTransform) {
                Destroy(child.gameObject);
            }
        }

        private HighscoreUIItem CreateNewItem ()
        {
            var instance = Instantiate(friendPrefab);
            var uiItem = instance.GetComponent<HighscoreUIItem>();
            return uiItem;
        }

        private int GetRandomScore ()
        {
            return Random.Range(0, currentLevel.progressValues[2]);
        }

        private IEnumerator ScrollingRoutine (float direction)
        {
            var currentOffset = 0f;
            while (currentOffset <= scrollingDelta) {
                currentOffset += Time.deltaTime;
                var offset = scrollRect.horizontalNormalizedPosition + direction * currentOffset;
                scrollRect.horizontalNormalizedPosition = Mathf.Clamp01(offset);

                yield return null;
            }
        }

        private void Awake ()
        {
            initialPosition = baseTransform.anchoredPosition;
        }

        /// <summary>
        /// Used for sorting integers in descending order. Equality is considered as "bigger" relation
        /// </summary>
        private class DescendingComparer : IComparer<int>
        {
            public int Compare(int x, int y)
            {
                var result = -x.CompareTo(y);
                return result == 0 ? 1 : result;
            }
        }
    }
}