﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MMK.Core.Socials.UI
{
    public class InviteFriendsPopup : SendLetterPopup
    {
        private bool isLoading;

        public override void Submit()
        {
            var friendsToBeInvited = selectedFriends.Select(friend => friend.Id.ToString());
            API.Social.InviteFriends(friendsToBeInvited);
        }

        public void OnScroll (Vector2 currentPosition)
        {
            //Debug.Log(currentPosition);
            if (currentPosition.y < 0f && !isLoading) {
                //StartCoroutine(LoadingCoroutine());
            }
        }

        private readonly List<IFriend> loadedFriends = new List<IFriend>();

        private IEnumerator LoadingCoroutine ()
        {
            var request = API.Social.GetInvitableFriends(allFriends.Count, 20);

            yield return new WaitForRequest(request);

            if (request.Error == null) {
                DisplayMoreFriends(request.Friends);
            } else {
                Debug.Log("Error loading additional friends: " + request.Error);
            }

            isLoading = false;
        }

        private void DisplayMoreFriends (IEnumerable<IFriend> friends)
        {
            initialPosition = friendsGrid.anchoredPosition;
            loadedFriends.AddRange(friends);
            ShowFriends();
        }

        protected override IEnumerable<IFriend> GetLoadedFriends()
        {
            var result = loadedFriends.Count != 0 ? loadedFriends : API.Social.GetInvitableFriends();
            loadedFriends.Clear();
            return result;
        }
    }
}