﻿using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.Socials.UI
{
    public class FriendUIItem : MonoBehaviour, IFriend
    {
        public RawImage avatar;
        public Text friendName;
        public Toggle checkbox;

        public string Id { get; private set; }
        public string Name { get; private set; }
        public string LastName { get; private set; }
        public Texture2D Avatar { get; private set; }
        public bool IsPlaying { get; private set; }

        public SendLetterPopup ParentPopup { get; set; }

        public void Select(bool @checked)
        {
            ParentPopup.SelectFriend(this, @checked);
        }

        public void Select ()
        {
            checkbox.isOn = !checkbox.isOn;
        }

        public void Init (IFriend friend)
        {
            Id = friend.Id;
            Name = friend.Name;
            LastName = friend.LastName;
            Avatar = friend.Avatar;
            IsPlaying = friend.IsPlaying;

            avatar.texture = Avatar;
            friendName.text = Name;
        }

        private void Awake ()
        {
            checkbox.isOn = true;
        }
    }
}