﻿using System.Collections.Generic;
using System.Linq;
using MMK.Core;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.Socials.UI
{
    public class MessageUIItem : MonoBehaviour
    {
        public GameObject awatarPrefab;
        public RectTransform awatarsBase;

        public int MaximumAvatars { get; set; } = 5;
        public SocialObject AttachedObject { get; set; }
        public IEnumerable<FacebookMessage> Letters { get; set; }

        private AnimatorSwitcher animatorSwitcher;

        public void Receive ()
        {
            AttachedObject.Use();
            API.Social.DeleteLetters(Letters.Select(letter => letter.Id).ToArray());

            if (animatorSwitcher != null) {
                animatorSwitcher.Off();
            } else {
                gameObject.SetActive(false);
            }
        }

        private void ShowAvatars ()
        {
            var friends = Letters
                .Select(letter => letter.Sender)
                .Where(sender => sender != null)
                .ToList();

            for (int index = 0; index < friends.Count && index < MaximumAvatars; index++) {
                var friend = friends[index];
                var avatarInstance = Instantiate(awatarPrefab);
                avatarInstance.transform.SetParent(awatarsBase, false);

                avatarInstance.GetComponentInChildren<RawImage>().texture = friend.Avatar;
            }
        }

        private void Awake ()
        {
            animatorSwitcher = GetComponent<AnimatorSwitcher>();
        }

        private void Start ()
        {
            ShowAvatars();
        }
    }
}