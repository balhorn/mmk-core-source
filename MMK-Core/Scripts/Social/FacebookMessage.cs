﻿using System;
using System.Linq;

namespace MMK.Core.Socials
{
    public class FacebookMessage : ILetter
    {
        public FacebookMessage(string id, string header, string text, DateTime sendTime, long senderId, SocialObject attachedObject)
        {
            Id = id;
            Header = header;
            Text = text;
            SendTime = sendTime;
            AttachedObject = attachedObject;

            this.senderId = senderId.ToString();
        }

        public string Id { get; }
        public string Header { get; }
        public string Text { get; }
        public DateTime SendTime { get; }
        public SocialObject AttachedObject { get; set; }

        public long ObjectId => AttachedObject?.Id ?? -1;
        public IFriend Sender => API.Social.GetFriends().FirstOrDefault(friend => friend.Id == senderId);

        private string senderId;

        public override string ToString()
        {
            return $"From: {(Sender != null ? Sender.Name + " " + Sender.LastName : "Unknown")}\n" +
                   $"Text: {Text}\n" +
                   $"Object attached: {(AttachedObject != null ? "Id: " + ObjectId : "None")}\n" +
                   $"Send at: {SendTime}";

        }
    }
}