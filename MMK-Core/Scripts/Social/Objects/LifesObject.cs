﻿using UnityEngine;

namespace MMK.Core.Socials
{
    [CreateAssetMenu(menuName = "MMK/Social/Lifes object")]
    public class LifesObject : SocialObject
    {
        public int lifesNumber;

        public override void Use()
        {
            Player.Instance.GainLifes(lifesNumber);
        }
    }
}