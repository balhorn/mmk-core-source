﻿using System;
using UnityEngine;

namespace MMK.Core.Socials
{
    [CreateAssetMenu(menuName = "MMK/Social/Booster object")]
    public class BoosterObject : SocialObject
    {
        public string boosterName;
        public int boostersNumber;

        public override void Use ()
        {
            Player.Instance.GainBooster(Type.GetType(boosterName), boostersNumber);
        }
    }
}