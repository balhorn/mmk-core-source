﻿using UnityEngine;

namespace MMK.Core.Socials
{
    public abstract class SocialObject : ScriptableObject
    {
        public GameObject messageItemPrefab;

        public long Id => Mathf.Abs(GetInstanceID());

        public abstract void Use();
    }
}