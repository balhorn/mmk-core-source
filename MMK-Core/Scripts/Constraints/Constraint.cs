﻿using UnityEngine;
using System.Collections;
using MMK.Core.UI;
using UnityEngine.UI;

namespace MMK.Core
{
    public abstract class Constraint : MonoBehaviour
    {
        public static Constraint Current { get; private set; }

        public PopUpManager popUpManager;
        public Text label;
        public Text outputText;
        public Image sliderImage;

        protected Level level;
        protected bool isLevelCompleted;

        public bool PlayToTheEnd { get; set; }

        public bool IsLevelCompleted
        {
            set
            {
                isLevelCompleted = value;
                if (isLevelCompleted) {
                    CheckGameOver();
                }
            }
        }



        public abstract int Amount { get; }

        public abstract void CheckGameOver();
        public abstract void Increment (int amount);
        public abstract void Decrement ();
        protected abstract void UIOutput ();

        protected virtual void Start ()
        {
            Current = this;
            level = Level.Current;
        }

        protected void UpdateAndSave ()
        {
            if (level.isTestMode) {
                return;
            }

            level.RecordHighScore();
            if (isLevelCompleted) {
                level.UpdateStars();
            }

            GameController.UpdateLevelData(level);
            GameController.SaveGame();
        }

        protected IEnumerator Win ()
        {
            yield return new WaitForSeconds(1f);
            level.isCompleted = true;
            PlayerPrefs.SetInt(Level.NextLevelPrefsKey, level.number + 1);

            UpdateAndSave();
            popUpManager.Win();
        }

        protected IEnumerator GameOver ()
        {
            yield return new WaitForSeconds(1f);
            popUpManager.OutOfMoves();
        }

        protected virtual void OnDestroy ()
        {
        }
    }
}

