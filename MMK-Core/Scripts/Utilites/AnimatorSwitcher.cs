﻿using System.Collections;
using UnityEngine;

namespace MMK.Core
{
    [RequireComponent(typeof(Animator))]
    public class AnimatorSwitcher : MonoBehaviour
    {
        public string onTrigger = "On";
        public string offTrigger = "Off";

        public bool disableWhenOff = true;
        public float disableAfter = 0.5f;

        private Animator animator;

        public void On ()
        {
            var hash = Animator.StringToHash(onTrigger);
            //gameObject.SetActive(true);
            animator.SetTrigger(hash);
        }

        public void Off ()
        {
            var hash = Animator.StringToHash(offTrigger);
            animator.SetTrigger(hash);

            if (disableWhenOff) {
                StartCoroutine(DisableCoroutine());
            }
        }

        private IEnumerator DisableCoroutine ()
        {
            yield return new WaitForSeconds(disableAfter);
            gameObject.SetActive(false);
        }

        private void Awake ()
        {
            animator = GetComponent<Animator>();
        }
    }
}

