﻿using MMK.Core.UI;
using UnityEngine;

namespace MMK.Core
{
    public class LoadingScreenRunner : MonoBehaviour
    {
        public string sceneName;

        public void Run()
        {
            LoadingScreen.Run(sceneName);
        }
    }
}