﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MMK.Core
{
    public static class Utilites
    {
        /// <summary>
        /// Prints exeption to console
        /// </summary>
        /// <param name="e"></param>
        public static void PrintException (Exception e)
        {
            Debug.Log ("Exception caught.\nMessage: " + e.Message + "\n" + e.StackTrace);
        }

        /// <summary>
        /// Checks internet availability
        /// </summary>
        public static bool InternetAvailable
        {
            get
            {
                try {
                    using (var webClient = new WebClient()) {
                        using (var stream = webClient.OpenRead("http://www.google.com")) {
                            return true;
                        }
                    }
                } catch (Exception e) {
                    return false;
                }
            }
        }

        /// <summary>
        /// Returns non-abstract children of the given type
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static IEnumerable<Type> GetDerivedTypes (Type t)
        {
            return Assembly.GetAssembly(t)
                .GetTypes()
                .Where(type => !type.IsAbstract && type.IsSubclassOf(t));
        }

        /// <summary>
        /// Splits CamelCase string with spaces
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string SplitCamelCase(string input)
        {
            return System.Text.RegularExpressions.Regex.Replace(input, "([A-Z])", " $1").Trim();
        }

        // ReSharper disable once InconsistentNaming
        public static bool IsPointerOverUI(EventSystem eventSystem = null)
        {
            eventSystem = eventSystem ?? EventSystem.current;
            var eventDataCurrentPosition = new PointerEventData(eventSystem) {
                position = Input.mousePosition
            };
            var results = new List<RaycastResult>();
            eventSystem.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }
    }
}