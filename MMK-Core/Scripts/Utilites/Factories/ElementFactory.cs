﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MMK.Core.Factories
{
    public static class ElementFactory
    {
        internal static List<GameObject> regularElements = new List<GameObject>();


        /// <summary>
        /// Loads Empty prefab in the Resources/Prefabs folder and returns it instantiated
        /// </summary>
        /// <returns></returns>
        public static GameObject LoadEmptyPrefab ()
        {
            var path = Path.Combine(Constants.PrefabsFolder, "Empty prefab");
            return LoadAndInstantiate(path);
        }

        private static GameObject LoadAndInstantiate (string path)
        {
            var prefab = Resources.Load<GameObject>(path);
            return prefab != null ? Object.Instantiate(prefab) : null;
        }
    }
}
