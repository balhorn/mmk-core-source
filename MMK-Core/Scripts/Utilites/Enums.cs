﻿using System;

namespace MMK.Core
{
    /// <summary>
    /// Our simple game state
    /// </summary>
    public enum GameState
    {
        Idle,
        SelectionStarted,
        Animating,
        BoosterTargetSelection,
        Pause,
        Map,
        WinBonus
    }

    public enum Edition
    {
        Community,
        Lite,
        Premium,
        Enterprize
    }

    public enum AcheivmentType
    {
        Usual,
        Incremental
    }
}