﻿using System.Collections.Generic;
using UnityEngine;

namespace MMK.Core
{
    public class Delegates
    {
        public delegate void SpriteDelegate(Sprite sprite);
        public delegate void VoidDelegate ();
        public delegate void WithIntParam (int amount);
        public delegate bool BoolDelegate ();

        public delegate byte[] CompressionDelegate (string folderName);
        public delegate List<byte[]> DecompressionDelegate (byte[] zipArchive);
    }
}