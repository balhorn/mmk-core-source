﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MMK.Core
{
    public class DisposableBundle : IDisposable
    {
        private readonly AssetBundle bundle;
        private readonly bool unloadAssets;

        public T LoadAsset<T>(string name) where T : Object
        {
            return bundle.LoadAsset<T>(name);
        }

        public Object mainAsset
        {
            get { return bundle.mainAsset; }
        }

        public string[] GetAllAssetNames()
        {
            return bundle.GetAllAssetNames();
        }

        public T[] LoadAllAssets<T>() where T : Object
        {
            return bundle.LoadAllAssets<T>();
        }

        public AssetBundleRequest LoadAllAssetsAsync<T>()
        {
            return bundle.LoadAllAssetsAsync<T>();
        }

        public T[] LoadAssetWithSubAssets<T>(string name) where T : Object
        {
            return bundle.LoadAssetWithSubAssets<T>(name);
        }

        public AssetBundleRequest LoadAssetWithSubAssetsAsync<T>(string name)
        {
            return bundle.LoadAssetWithSubAssetsAsync<T>(name);
        }

        public AssetBundleRequest LoadAssetAsync<T>(string name)
        {
            return bundle.LoadAssetAsync<T>(name);
        }

        public bool Contains(string name)
        {
            return bundle.Contains(name);
        }

        public DisposableBundle(AssetBundle bundle, bool unloadAssets)
        {
            if (bundle == null) {
                throw new NullReferenceException("Asset bundle wasn't loaded correctly");
            }

            this.bundle = bundle;
            this.unloadAssets = unloadAssets;
        }

        public void Dispose()
        {
            bundle.Unload(unloadAssets);
        }
    }
}
