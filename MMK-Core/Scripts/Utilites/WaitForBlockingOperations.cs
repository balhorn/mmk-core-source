﻿using UnityEngine;

namespace MMK.Core
{
    public class WaitForBlockingOperations : CustomYieldInstruction
    {
        public override bool keepWaiting
        {
            get
            {
                GameController.ShowWarningIfNecessary();
                return GameController.BlockingOperationsRunning;
            }
        }
    }
}