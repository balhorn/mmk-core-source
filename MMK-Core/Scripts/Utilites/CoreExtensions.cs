﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MMK.Core
{
    public static class CoreExtensions
    {

        /// <summary>
        /// Checks whether game object has specified component or not
        /// </summary>
        /// <typeparam name="ComponentType"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool HasComponent<ComponentType> (this GameObject obj) where ComponentType : Component
        {
            return obj != null && obj.GetComponent<ComponentType>() != null;
        }

        public static bool HasComponentInChildren<ComponentType> (this GameObject obj) where ComponentType : Component
        {
            return obj.GetComponentInChildren<ComponentType>() != null;
        }

        /// <summary>
        /// Checks whether game object has specified component or not
        /// </summary>
        /// <typeparam name="ComponentType"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool HasComponent (this GameObject obj, Type type)
        {
            return obj?.GetComponent(type) != null;
        }

        /// <summary>
        /// Generator method to iterate through transform's children
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static IEnumerable<Transform> GetChildren (this Transform target)
        {
            for (int index = 0; index < target.childCount; index++) {
                yield return target.GetChild(index);
            }
        }

        public static GameObject GetChildWithTag (this Transform trans, string tag)
        {
            for (int i = 0; i < trans.childCount; i++) {
                var go = trans.GetChild(i).gameObject;
                if (go.CompareTag(tag)) {
                    return go;
                }
            }

            return null;
        }

        public static bool HasFlag (this Enum variable, Enum flag)
        {
            ulong baseValue = Convert.ToUInt64(variable);
            ulong flagValue = Convert.ToUInt64(flag);

            return (baseValue & flagValue) == flagValue;
        }

        public static List<Enum> GetFlags (this Enum variable)
        {
            return Enum.GetValues(variable.GetType())
                .Cast<Enum>()
                .Where(variable.HasFlag)
                .ToList();
        } 
    }
}


