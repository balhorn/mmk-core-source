﻿using System;

namespace MMK.Core
{
    public class ElementEditorMenuAttribute : Attribute
    {
        public string Path { get; set; }
        public bool Ignore { get; set; }

        public ElementEditorMenuAttribute(string path, bool ignore = false)
        {
            Path = path;
            Ignore = ignore;
        }
    }
}