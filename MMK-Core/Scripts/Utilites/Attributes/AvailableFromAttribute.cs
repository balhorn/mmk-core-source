﻿using UnityEngine;

namespace MMK.Core
{
    public class AvailableFromAttribute : PropertyAttribute
    {
        public Edition availableFrom;

        public AvailableFromAttribute(Edition availableFrom)
        {
            this.availableFrom = availableFrom;
        }
    }
}
