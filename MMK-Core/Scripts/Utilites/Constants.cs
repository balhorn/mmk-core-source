﻿using System.IO;
using UnityEngine;

namespace MMK.Core
{
    public static class Constants
    {
        public const int MaximumLifes = 5;

        public const string MMKRoot = "Assets/MMK/";
        public const string MMKCoreRoot = MMKRoot + "Core/";
        public const string SpritesFolder = "Sprites";
        public const string AnimatorsFolder = "Animators";
        public const string PrefabsFolder = "Prefabs";
        public const string LevelsFolder = MMKRoot + "Resources/Levels";

        public const string StartingMenuName = "Persistant";
        public const string MainMenuScene = "FirstScreen";
        public const string MapScene = "Map";
        public const string MainGameScene = "GameScreen";
        public const string ElementsEditorScene = "Elements editor";

        public const string AppURL = "";
        public const string ServerURL = "http://octopus.int";
        public const string AccessTokenHeader = "access-token";
        public const string OnAccessTokenSuccessFunction = "OnAccessTokenSuccess";
        public const string OnAccessTokenFailFunction = "OnAccessTokenFail";
        public const string LevelNameFormat = "Level_{0}";

        public const string LevelAssetBundleNameFormat = "levels/{0}";
        public static string AssetBundleNameFormat => LevelAssetBundleNameFormat.Split('/')[0];
        public static string AssetBundleFolder => LevelAssetBundleNameFormat.Split('/')[1];

        public static string LevelsPersistantPath => Path.Combine(Application.persistentDataPath, "levels");
    }
}