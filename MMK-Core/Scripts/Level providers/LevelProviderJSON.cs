﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace MMK.Core
{
    // ReSharper disable once InconsistentNaming
    /// <summary>
    /// Loads each level from previously saved json file
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class LevelProviderJSON<T> : AbstractLevelProvider<T>, IBlockingOperation where T : PremadeLevel
    {
        private readonly List<T> levels = new List<T>();
        private int levelCount;

        public override void LoadLevels()
        {
            StartInstallation();
        }

        public override T GetLevelByNumber(int number)
        {
            return GetPreloadedLevel(number) ?? LoadFromFile(number);
        }

        public override List<T> GetLevels()
        {
            var result = new List<T>();
            for (int i = 1; i <= levelCount; i++) {
                result.Add(LoadFromFile(i));
            }

            return result;
        }

        protected LevelProviderJSON(MonoBehaviour coroutineHandler) : base(coroutineHandler)
        {
            lockFilePath = Path.Combine(Application.persistentDataPath, FileName);
        }

        private const string FileName = "lockfile";
        private string lockFilePath;
        private GameSettings settings;

        private GameSettings Settings
        {
            get { return settings ?? (settings = Resources.Load<GameSettings>("GameSettings")); }
        }

        private T GetPreloadedLevel (int number)
        {
            return levels.Find(lvl => lvl.number == number);
        }

        private T LoadFromFile(int number)
        {
            if (!Directory.Exists(Constants.LevelsPersistantPath)) {
                return null;
            }

            var fileName = string.Format(Constants.AssetBundleNameFormat, number);
            var filePath = Path.Combine(Constants.LevelsPersistantPath, fileName);
            if (!File.Exists(filePath)) {
                return null;
            }

            var levelJson = File.ReadAllText(filePath);
            var newLevel = ScriptableObject.CreateInstance<T>();
            JsonUtility.FromJsonOverwrite(levelJson, newLevel);
            levels.Add(newLevel);

            return newLevel;
        }

        private void StartInstallation ()
        {
            if (File.Exists(lockFilePath)) {
                return; //game was already installed
            }

            File.Create(lockFilePath);

            GameController.RegisterBlockingOperation(this, 1);
            StartCoroutine(InstallationCoroutine());
        }

        private IEnumerator InstallationCoroutine ()
        {
            yield return null;
            try { CreateLevelFiles(); } catch (Exception e) {
                Debug.Log("Error loading levels: " + e);
                GameController.StopBlockingOperation(this);
                File.Delete(lockFilePath);
            }
        }

        private void CreateLevelFiles ()
        {
            var levels = LoadLevelsFromServer();
            levelCount = levels.Count;

            if (levels.Count == 0) {
                File.Delete(lockFilePath);
                GameController.CallbackExecuted(this);
                return;
            }

            if (!Directory.Exists(Constants.LevelsPersistantPath)) {
                Directory.CreateDirectory(Constants.LevelsPersistantPath);
            }

            foreach (var mahjongLevel in levels) {
                var levelJson = JsonUtility.ToJson(mahjongLevel);
                var fileName = string.Format(Constants.AssetBundleNameFormat, mahjongLevel.number);
                File.WriteAllText(Path.Combine(Constants.LevelsPersistantPath, fileName), levelJson);
            }

            Debug.Log("Game has been successfully installed!");
            GameController.CallbackExecuted(this);
        }

        protected abstract List<T> LoadLevelsFromServer();
    }
}