﻿using System.Collections.Generic;
using UnityEngine;

namespace MMK.Core
{
    public static class LevelProvider<T> where T : PremadeLevel
    {
        private static ILevelProvider<T> instance;

        public static void SetInstance(ILevelProvider<T> newInstance)
        {
            instance = newInstance;
            Debug.Log($"Level provider is now: {newInstance}");
        }

        public static T GetLevel(int number)
        {
            return instance.GetLevelByNumber(number);
        }

        public static void LoadLevels()
        {
            instance.LoadLevels();
        }

        public static List<T> GetLevels()
        {
            return instance.GetLevels();
        }
    }
}