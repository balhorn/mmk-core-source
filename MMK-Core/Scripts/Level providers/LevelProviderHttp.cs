﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

namespace MMK.Core
{
    /// <summary>
    /// Loads levels packed into bundles from server and fetches each level from corresponding bundle
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LevelProviderHttp<T> : AbstractLevelProvider<T>, IBlockingOperation where T : PremadeLevel
    {
        protected AssetBundle[] levelBundles;
        protected AssetBundle[] locationBundles;

        private int LevelBatchSize => Player.Settings.levelsPerLocation;
        private const int LoadPreviousLocations = 2;

        #region Interface members

        public override void LoadLevels()
        {
            trackingCoroutine = StartCoroutine(TrackMapLoading());
        }

        public override T GetLevelByNumber(int number)
        {
            var bundle = levelBundles[number / LevelBatchSize];

            if (bundle == null) {
                return null;
            }

            return LoadLevelFromBundle(bundle, number);
        }

        public override List<T> GetLevels()
        {
            var result = new List<T>();

            if (levelBundles.Length == 0) {
                return result;
            }

            var bundleIndex = 0;
            var loadedLevel = ScriptableObject.CreateInstance<T>();

            while (true) {
                var bundle = levelBundles[bundleIndex];
                if (bundle == null) {
                    break;
                }

                for (int i = 1; i <= LevelBatchSize; i++) {
                    var levelNumber = i + bundleIndex * LevelBatchSize;
                    var level = LoadLevelFromBundle(bundle, levelNumber);
                    if (level == null) {
                        break;
                    }

                    result.Add(level);
                }
            }

            return result;
        }

        protected LevelProviderHttp(MonoBehaviour coroutineHandler) : base(coroutineHandler)
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }

        ~LevelProviderHttp()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        #endregion

        #region Bundles loading

        private IEnumerator LoadBundlesRoutine (int lastLevelNumber)
        {
            var currentLocationNumber = lastLevelNumber / LevelBatchSize + 1;
            levelBundles = new AssetBundle[currentLocationNumber];

            var loadFrom = currentLocationNumber - LoadPreviousLocations;
            loadFrom = loadFrom > 0 ? loadFrom : 1;
            var callbacksCount = currentLocationNumber - loadFrom + 1;
            GameController.RegisterBlockingOperation(this, callbacksCount);

            yield return new WaitUntil(() => !string.IsNullOrEmpty(Player.AccessToken));

            for (int i = loadFrom; i <= currentLocationNumber; i++) {
                locationNumber = i;
                StartCoroutine(LoadingCoroutine());
            }
        }

        private const int DontVerifyChecksum = 0;
        private const int BundlesVersion = 1;

        private IEnumerator LoadingCoroutine ()
        {
            using (request = UnityWebRequest.Get(GetUri())) {
                request.SetRequestHeader("access-token", Player.AccessToken);
                yield return StartCoroutine(GetRequestCoroutine(request.Send()));

                if (!requestSuccessfull) { yield break; }

                request = UnityWebRequest.GetAssetBundle(Constants.ServerURL + getBundleUrl, BundlesVersion, DontVerifyChecksum);
                yield return StartCoroutine(GetBundleCoroutine(request.Send()));
            }
        }

        private UnityWebRequest request;
        private int locationNumber;
        private string getBundleUrl;
        private bool requestSuccessfull;

        private const string QueryFormat = "?name_format={0}&bundle_size={1}&number={2}&platform={3}";

        

        private string GetUri ()
        {
            var nameFormat = Constants.AssetBundleNameFormat;
            var bundleSize = Player.Settings.levelsPerLocation;
            var number = locationNumber;

            var uri = new StringBuilder(Constants.ServerURL)
                .Append("/bundles")
                .AppendFormat(QueryFormat, nameFormat, bundleSize, number, Platform)
                .ToString();

            Debug.Log("Trying to access " + uri);

            return uri;
        }

        private IEnumerator GetRequestCoroutine (AsyncOperation operation)
        {
            yield return operation;

            if (request.isError || request.responseCode != 200) {
                Debug.Log($"Is error {request.isError}. Response code: {request.responseCode}");
                Debug.Log("Error performing GET request:");
                Debug.Log(request.isError ? request.error : request.downloadHandler.text);
                GameController.CallbackExecuted(this);

                yield break;
            }

            var requestData = JsonUtility.FromJson<BundleRequestData>(request.downloadHandler.text);
            Debug.Log("Parsed request data: " + requestData);
            getBundleUrl = "/" + requestData.path;
            requestSuccessfull = true;
        }

        private IEnumerator GetBundleCoroutine (AsyncOperation operation)
        {
            var timer = Stopwatch.StartNew();
            yield return operation;

            var downloadHandler = request.downloadHandler as DownloadHandlerAssetBundle;
            var bundle = downloadHandler.assetBundle;
            Debug.Log($"Bundle loaded in {timer.ElapsedMilliseconds} ms");

            levelBundles[locationNumber - 1] = bundle;

            GameController.CallbackExecuted(this);
        }

        private T LoadLevelFromBundle(AssetBundle bundle, int number)
        {
            if (bundle == null) {
                throw new ArgumentNullException(nameof(bundle));
            }

            var levelName = string.Format(Constants.LevelNameFormat, number);
            var jsonFile = bundle.LoadAsset<TextAsset>(levelName);

            if (jsonFile == null) {
                return null;
            }

            var level = ScriptableObject.CreateInstance<T>();
            JsonUtility.FromJsonOverwrite(jsonFile.text, level);

            return level;
        }

        [Serializable]
        public class BundleRequestData
        {
            public string path;
            public string name_format;
            public int bundle_size;

            public override string ToString()
            {
                return $"Url: {path}, NameFormat: {name_format}, BundleSize: {bundle_size}";
            }
        }

        #endregion

        #region Scene loading tracking

        private bool isMapLoaded;
        private Coroutine trackingCoroutine;

        /// <summary>
        /// Loads level bundles each time the map scene gets loaded
        /// </summary>
        /// <returns></returns>
        private IEnumerator TrackMapLoading()
        {
            while (true) {
                yield return new WaitUntil(() => isMapLoaded);
                isMapLoaded = false;

                yield return StartCoroutine(LoadBundlesRoutine(GameController.LastCompletedLevel));
            }
        }

        private void OnSceneLoaded(Scene loadedScene, LoadSceneMode loadSceneMode)
        {
            isMapLoaded = loadedScene.name == Constants.MapScene;
        }

        #endregion
    }
}