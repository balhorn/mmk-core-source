﻿using System.Collections.Generic;

namespace MMK.Core
{
    public interface ILevelProvider<T> where T : PremadeLevel
    {
        void LoadLevels();
        T GetLevelByNumber(int number);
        List<T> GetLevels();
    }
}