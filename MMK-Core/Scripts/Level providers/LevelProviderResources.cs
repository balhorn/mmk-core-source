﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MMK.Core
{
    /// <summary>
    /// Uses Unity Resources system to load levels directly as scriptable objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LevelProviderResources<T> : AbstractLevelProvider<T> where T : PremadeLevel
    {
        protected string levelsFolder;
        protected List<T> levels;

        public override void LoadLevels()
        {
            levels = Resources.LoadAll<T>(levelsFolder).ToList();
            levels.Sort((lvl1, lvl2) => lvl1.number.CompareTo(lvl2.number));
        }

        public override T GetLevelByNumber(int number)
        {
            return levels[number - 1];
        }

        public override List<T> GetLevels()
        {
            return levels;
        }

        public LevelProviderResources(MonoBehaviour coroutineHandler, string levelsFolder) : base(coroutineHandler)
        {
            this.levelsFolder = levelsFolder;
            LoadLevels();
        }
    }
}