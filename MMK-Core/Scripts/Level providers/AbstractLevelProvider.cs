﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MMK.Core
{
    public abstract class AbstractLevelProvider<T> : ILevelProvider<T> where T : PremadeLevel
    {
        private MonoBehaviour coroutineHandler;
        private GameSettings settings;

        protected GameSettings Settings => settings ?? (settings = Resources.Load<GameSettings>("GameSettings"));

        protected string Platform
        {
            get
            {
                switch (Application.platform) {
                    case RuntimePlatform.Android:
                        return "android";
                    case RuntimePlatform.OSXEditor:
                    case RuntimePlatform.WindowsEditor:
                    case RuntimePlatform.WebGLPlayer:
                        return "fb";
                    case RuntimePlatform.IPhonePlayer:
                        return "ios";
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        protected AbstractLevelProvider(MonoBehaviour coroutineHandler)
        {
            this.coroutineHandler = coroutineHandler;
            settings = Resources.Load<GameSettings>("GameSettings");
        }

        protected Coroutine StartCoroutine(IEnumerator routine)
        {
            return coroutineHandler.StartCoroutine(routine);
        }
          
        public abstract void LoadLevels();
        public abstract T GetLevelByNumber(int number);
        public abstract List<T> GetLevels();
    }
}