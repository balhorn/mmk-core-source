﻿using System;
using MMK.Core;
using UnityEngine;

namespace MMK.Core.Publishers
{
    public class AcheivmentTracker : MonoBehaviour
    {
        public string achievmentId;
        public AcheivmentType type;

        /// <summary>
        /// Unlocks acheivment
        /// </summary>
        public void TrackAcheivment ()
        {
            if (type == AcheivmentType.Usual) {
                API.Publisher.UnlockAcheivment(achievmentId);
            }
        }

        /// <summary>
        /// Increment acheivment by the given score
        /// </summary>
        /// <param name="steps">steps count</param>
        public void TrackAcheivment (int steps)
        {
            switch (type) {
                case AcheivmentType.Usual:
                    TrackAcheivment();
                    break;
                case AcheivmentType.Incremental:
                    API.Publisher.IncrementAcheivment(achievmentId, steps);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}