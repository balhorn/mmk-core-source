﻿namespace MMK.Core.Publishers
{
    public class FakePublisher : IPublisher
    {
        public void Init()
        {
        }

        public void Login()
        {
        }

        public void SaveGame()
        {
        }

        public void LoadGame()
        {
        }

        public void ShowLeaderboard()
        {
        }

        public void AddScoreToLeaderboard(int score)
        {
        }

        public void UnlockAcheivment(string achievmentId)
        {
        }

        public void IncrementAcheivment(string achievmentId, int score)
        {
        }
    }
}