﻿using MMK.Core;
using UnityEngine;

public class RateUs : MonoBehaviour
{
    public void Rate ()
    {
        var url = Player.Settings.Monetization.rateUsUrl;
        Application.OpenURL(url);
    }
}
