﻿using MMK.Core.Analytics;
using MMK.Core.InApp;
using MMK.Core.Publishers;
using MMK.Core.Server;
using MMK.Core.Socials;
// ReSharper disable All

namespace MMK.Core
{
    public static class API
    {
        private static ISocialNetwork social;
        private static IPublisher publisher;
        private static IShop shop;
        private static IAnalyticsManager analyticsManager;
        private static IServer server;

        public static ISocialNetwork Social
        {
            get { return social ?? new FakeSocial(); }
            set { social = value; }
        }

        public static IPublisher Publisher
        {
            get { return publisher ?? new FakePublisher(); }
            set { publisher = value; }
        }

        public static IShop Shop
        {
            get { return shop ?? new FakeShop(); }
            set { shop = value; }
        }

        public static IAnalyticsManager AnalyticsManager
        {
            get { return analyticsManager ?? new FakeAnalytics(); }
            set { analyticsManager = value; }
        }

        public static IServer Server
        {
            get { return server ?? new FakeServer(); }
            set { server = value; }
        }
    }
}