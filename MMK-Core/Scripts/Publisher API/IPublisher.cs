﻿namespace MMK.Core.Publishers
{
    /// <summary>
    /// Provides general interface for publishing platforms.
    /// </summary>
    public interface IPublisher
    {
        /// <summary>
        /// Initialize platform. Call it within Awake or Start method.
        /// </summary>
        void Init ();

        /// <summary>
        /// Logs user to the platform
        /// </summary>
        void Login ();

        /// <summary>
        /// Saves current game to platform server
        /// </summary>
        void SaveGame ();

        /// <summary>
        /// Loads saved game from the platform server. Make sure it doesn't override more developed local game!
        /// </summary>
        void LoadGame ();

        void ShowLeaderboard();

        void AddScoreToLeaderboard(int score);

        void UnlockAcheivment(string achievmentId);

        void IncrementAcheivment(string achievmentId, int score);
    }
}