﻿using System;
using MMK.Core.Boosters;
using UnityEngine;

namespace MMK.Core
{
    public class PlayerStub : Player
    {
        public override int Lifes => 5;

        public override int HardCurrency => 1500;

        public override int GetBoosterAmount (AbstractBooster booster)
        {
            return 3;
        }

        public override void SpendBooster (AbstractBooster booster)
        {
            return;
        }

        public override void DecreaseLife ()
        {
            return;
        }

        public override void GainLifes (int amount)
        {
            return;
        }

        public override void SpendCurrency (int amount)
        {
            return;
        }

        public override void GainCurrency (int amount)
        {
            return;
        }

        public override void GainBooster(Type boosterType, int amount)
        {
            return;
        }

        protected override void Update()
        {
            
        }


        private void Save ()
        {
            return;
        }

        protected override void Awake ()
        {
            if (instance != null) {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            settings = Resources.Load<GameSettings>("GameSettings");
        }

        protected override void Start()
        {
            return;
        }

        protected override void OnDestroy ()
        {
            Instance = null;
        }
    }
}