﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using MMK.Core.Boosters;
using MMK.Core.InApp;
using MMK.Core.Publishers;
using MMK.Core;
using MMK.Core.Server;
using MMK.Core.Socials;
using UnityEngine.SceneManagement;

namespace MMK.Core
{
    public class Player : MonoBehaviour, IAccessTokenHandler
    {
        public static Player Instance
        {
            get { return instance ?? CreateStub(); }
            set { instance = value; }
        }

        public static string AccessToken
        {
            get { return Instance.accessToken; }
            set { Instance.accessToken = value; }
        }

        public static GameSettings Settings => Instance.settings;

        protected static Player instance;

        public ISocialManager SocialManager { get; set; }
        public IShopController ShopController { get; set; }

        [Obsolete("Player.Social is obsolete. Use API.Social instead")]
        public ISocialNetwork Social { get; set; }

        [Obsolete("Player.Publisher is obsolete. Use API.Publisher instead")]
        public IPublisher Publisher { get; set; }

        public IShop Shop { get; set; }

        public virtual int Lifes => data.lifes;
        public virtual int HardCurrency
        {
            get { return data.HardCurrency; }
            set
            {
                data.HardCurrency = value;
                CurrencyUpdated?.Invoke();
            }
        }

        public virtual int Id => data.Id;

        protected GameSettings settings;

        private PlayerServiceData data;
        private DateTime lifeLost;
        private TimeSpan lifesRegainTime;

        private XmlSerializer serializer;
        private bool created;
        private string accessToken;

        private const string FileName = "player.xml";

        public static event Delegates.VoidDelegate LifesUpdated;
        public static event Delegates.VoidDelegate CurrencyUpdated;
        public static event Delegates.VoidDelegate BoosterUpdated;

        /// <summary>
        /// Returns time till the next life gain in format i:s
        /// </summary>
        public string TimeToRegainLife
        {
            get
            {
                if (SecondsToRestoreLife == 0) {
                    return string.Empty;
                }

                var timeSpan = new TimeSpan((int) SecondsToRestoreLife);
                var seconds = timeSpan.Seconds >= 10 ? timeSpan.Seconds.ToString() : $"0{timeSpan.Seconds}";
                return $"{timeSpan.Minutes}:{seconds}";
            }
        }

        private float SecondsToRestoreLife
        {
            get { return data.SecondsToRestoreLife; }
            set { data.SecondsToRestoreLife = value; }
        }

        private TimeSpan TimeElapsed => lifeLost != DateTime.MinValue ? DateTime.Now.Subtract(lifeLost) : new TimeSpan(0);

        /// <summary>
        ///
        /// </summary>
        /// <param name="booster"></param>
        /// <returns>Number of given boosters left</returns>
        public virtual int GetBoosterAmount (AbstractBooster booster)
        {
            return GetBoosterAmount(booster.GetType().Name);
        }

        protected int GetBoosterAmount (string name)
        {
            return data.GetBoosterAmount(name);
        }

        public BoosterInfo GetBoosterInfo (AbstractBooster booster)
        {
            return GetBoosterInfo(booster.GetType());
        }

        public BoosterInfo GetBoosterInfo (Type boosterType)
        {
            var alias = BoosterInfo.GetAliasByTypename(boosterType.Name);
            return data.GetBoosterInfo(alias);
        }

        /// <summary>
        /// Reduces the number of given booster by 1. Invokes BoosterUpdated event
        /// </summary>
        /// <param name="booster"></param>
        public virtual void SpendBooster (AbstractBooster booster)
        {
            SpendBooster(booster.GetType().Name);
        }

        protected void SpendBooster (string name)
        {
            data.SpendBooster(name);
            BoosterUpdated?.Invoke();
        }

        /// <summary>
        /// Increases the number of given booster or prebooster by given amount. Invokes BoosterUpdated event
        /// </summary>
        /// <param name="boosterType">Type derived from AbstractBooster or Prebooster</param>
        /// <param name="amount"></param>
        public virtual void GainBooster (Type boosterType, int amount)
        {
            if (!boosterType.IsSubclassOf(typeof(AbstractBooster))) {
                Debug.LogFormat("Impossible to gain {0} because it's not a booster", boosterType.Name);
                return;
            }

            data.GainBooster(boosterType, amount);
            BoosterUpdated?.Invoke();
        }

        /// <summary>
        /// Decreses amount of lifes by one. Invokes LifesUpdated event
        /// </summary>
        public virtual void DecreaseLife ()
        {
            data.DecreaseLife();
            lifeLost = DateTime.Now;
            SaveLifeLostTime();
            LifesUpdated?.Invoke();
        }

        /// <summary>
        /// Increses number of lifes by given amount. Invokes LifesUpdated event
        /// </summary>
        /// <param name="amount"></param>
        public virtual void GainLifes (int amount)
        {
            data.GainLifes(amount);
            lifeLost = data.lifes == Constants.MaximumLifes ? DateTime.MinValue : DateTime.Now;
            SaveLifeLostTime();
            LifesUpdated?.Invoke();
        }

        private void SaveLifeLostTime ()
        {
            PlayerPrefs.SetString("Life lost", lifeLost.ToString("G"));
            PlayerPrefs.Save();
        }

        /// <summary>
        /// Decreases number of currency by given amount. Invokes CurrencyUpdated event
        /// </summary>
        /// <param name="amount"></param>
        public virtual void SpendCurrency (int amount)
        {
            data.SpendCurrency(amount);
            CurrencyUpdated?.Invoke();
        }

        /// <summary>
        /// Increases the number of currency by given amount. Invokes CurrencyUpdated event and displays Purchase status window
        /// </summary>
        /// <param name="amount"></param>
        public virtual void GainCurrency (int amount)
        {
            data.GainCurrency(amount);
            CurrencyUpdated?.Invoke();
        }

        public IEnumerator UpdateValues ()
        {
            yield return StartCoroutine(data.UpdatePlayerValues());
            CurrencyUpdated?.Invoke();
            BoosterUpdated?.Invoke();
        }

        private Coroutine playerInitCoroutine;

        public void OnAccessTokenSuccess ()
        {
            GameController.LoadGame();
            data.InitPlayer();
        }

        public void OnAccessTokenFail ()
        {
            GameController.StopBlockingOperation(data);
            if (playerInitCoroutine != null) {
                StopCoroutine(playerInitCoroutine);
            }
        }

        private static Player CreateStub ()
        {
            var obj = new GameObject("Player", typeof (SoundManager)) {tag = "Player"};
            return obj.AddComponent<PlayerStub>();
        }

        protected virtual void Update ()
        {
            TrackBackButton();
            UpdateLifeRestoreTimer();
        }

        private void UpdateLifeRestoreTimer ()
        {
            if (SecondsToRestoreLife <= 0f) {
                return;
            }

            SecondsToRestoreLife -= Time.deltaTime;

            if (SecondsToRestoreLife <= 0) {
                SecondsToRestoreLife = 0;
                data.UpdateLifes();
            }
        }

        private static void TrackBackButton ()
        {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                GameController.GoBack();
            }
        }

        private void Save()
        {
            var encoding = Encoding.GetEncoding("utf-8");
            var fileStream = new FileStream(FilePath, FileMode.OpenOrCreate);
            var writer = new StreamWriter(fileStream, encoding);

            try {
                serializer.Serialize(writer, data);
            } finally {
                fileStream.Dispose();
                writer.Dispose();
            }
        }

        private string FilePath => Path.Combine(Application.persistentDataPath, FileName);

        protected virtual void Awake ()
        {
            if (instance != null) {
                Destroy(gameObject);
                return;
            }

            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            SceneManager.sceneLoaded += OnSceneLoaded;

            settings = Resources.Load<GameSettings>("GameSettings");

            Instance = this;

            lifeLost = PlayerPrefs.HasKey("Life lost") ? DateTime.Parse(PlayerPrefs.GetString("Life lost")) : DateTime.MinValue;
            lifesRegainTime = new TimeSpan(0, settings.regainLifesHours, settings.regainLifesMinutes, settings.regainLifesSeconds);

            serializer = new XmlSerializer(typeof(PlayerServiceData));
        }

        protected virtual void Start ()
        {
            Load();
            GameController.Init();
        }

        private void Load()
        {
            if (!File.Exists(FilePath)) {
                data = new PlayerServiceData();
                return;
            }

            try {
                using (var reader = new StreamReader(new FileStream(FilePath, FileMode.Open))) {
                    data = (PlayerServiceData) serializer.Deserialize(reader);
                }
            } catch (XmlException) {
                File.Delete(FilePath);
                data = new PlayerServiceData();
            }
        }

        private void OnSceneLoaded (Scene scene, LoadSceneMode loadSceneMode)
        {
            var socialManager = FindObjectsOfType<MonoBehaviour>()
                    .FirstOrDefault(mono => mono is ISocialManager)
                as ISocialManager;

            ShopController = FindObjectsOfType<MonoBehaviour>()
                    .FirstOrDefault(mono => mono is IShopController)
                as IShopController;

            if (socialManager == null) {
                Debug.Log("No ISocialManager instance found on the current scene. It will be replaced by stub");
                SocialManager = new StubSocialManager();
            } else {
                SocialManager = socialManager;
            }
        }

        protected virtual void OnDestroy ()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;

            if (!created) {
                return;
            }

            if (Level.Current != null) {
                DecreaseLife();
            }

            //GameController.SaveGame();
            Save();
            Instance = null;
        }
    }
}