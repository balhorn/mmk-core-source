﻿using UnityEngine;

namespace MMK.Core
{
    /// <summary>
    /// Describes entity that gives player score
    /// Correct way to use is ScoreManager.RecordScore() function
    /// </summary>
    public interface IScoreEntity
    {
        /// <summary>
        /// Amount of score to be added
        /// </summary>
        int Score { get; set; }

        /// <summary>
        /// Coordinates in the screen space where the score text to appear
        /// </summary>
        Vector2 Coordinates { get; set; }

        /// <summary>
        /// Color of the score text
        /// </summary>
        Color TextColor { get; set; }
    }
}