﻿namespace MMK.Core
{
    public interface IUndoable
    {
        void Undo();
    }
}