﻿namespace MMK.Core
{
    public interface ILevelBuilder
    {
        void BuildPremadeLevel();
    }
}