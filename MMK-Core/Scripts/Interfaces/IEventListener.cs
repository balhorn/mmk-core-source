﻿using UnityEngine;
using System.Collections;

namespace MMK.Core
{
    public interface IEventListener
    {
        void BindEvent();
        void UnbindEvent();
    }
}

