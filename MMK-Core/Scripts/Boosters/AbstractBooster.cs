﻿using System;
using MMK.Core;
using MMK.Core.UI;
using UnityEngine;
using UnityEngine.UI;

namespace MMK.Core.Boosters
{
    public abstract class AbstractBooster : MonoBehaviour
    {
        protected Button button;

        protected Text    indicatorText;
        protected Image   indicatorImage;
        protected Button  indicatorButton;
        protected Outline outline;

        protected GetBoosterWindow getBoosterWindow;

        protected int  amount;
        protected bool running;

        private readonly Color iconColor = new Color(0.9765f, 0.4549f, 0.3216f);

        public int Amount => Player.Instance.GetBoosterAmount(this);

        protected delegate void BoosterDelegate (AbstractBooster booster);
        protected static event BoosterDelegate BoosterPressed;

        private Sprite Image => GetComponent<Image>()?.sprite;

        protected void Start()
        {
            Player.BoosterUpdated += UpdateUI;
            BoosterPressed += OnBoosterPressed;

            InitProperties();
            UpdateUI();
        }

        private void OnBoosterPressed(AbstractBooster booster)
        {
            if (booster == this) {
                return;
            }

            Deactivate();
        }

        protected virtual void InitProperties()
        {
            getBoosterWindow = FindObjectOfType<PopUpManager>().getBoosterWindow;

            button = GetComponent<Button>();
            button.onClick.AddListener(ButtonPressed);

            amount = Player.Instance.GetBoosterAmount(this);
        }

        protected void DecreaseAmount ()
        {
            amount = Amount - 1;
            Player.Instance.SpendBooster(this);

            indicatorText.text = Amount.ToString();
        }

        public abstract void UpdateUI();
        public abstract void Trigger(GameObject targetToken);

        public virtual void ButtonPressed ()
        {
            if (Amount <= 0) {
                PrepareBoosterPurchase();
                return;
            }

            BoosterPressed?.Invoke(this);

            if (GameController.state == GameState.Pause || GameController.state == GameState.WinBonus) {
                return;
            }

            if (running) {
                Deactivate();
            } else {
                Activate();
            }
        }

        public virtual void Activate()
        {
            if (Amount <= 0) {
                return;
            }

            if (running) {
                Deactivate();
                return;
            }

            GameController.state = GameState.BoosterTargetSelection;

            running = true;
        }

        public virtual void Deactivate()
        {
            running = false;
            GameController.state = GameState.Idle;
        }

        protected abstract void Update();

        protected virtual void PrepareBoosterPurchase ()
        {
            if (getBoosterWindow == null) {
                Debug.LogError("Please, assign Get Booster Window script to the Pop Up script");
                return;
            }

            getBoosterWindow.Show(GetType(), Image);
        }

        protected virtual void OnDestroy ()
        {
            button.onClick.RemoveAllListeners();
            Player.BoosterUpdated -= UpdateUI;
            BoosterPressed -= OnBoosterPressed;
        }

    }
}