﻿using System.Collections.Generic;
using SimpleJSON;

namespace MMK.Core.Analytics
{
    public static class JsonUtilite
    {
        public static string ToJson (Dictionary<string, object> data)
        {
            return GetJsonClass(data).ToJSON(0);
        }

        public static string ToJson (List<object> data)
        {
            var array = new JSONArray();
            
            foreach (Dictionary<string, object> entity in data) {
                array.Add(GetJsonClass(entity));
            }

            return array.ToJSON(0);
        }

        private static JSONClass GetJsonClass (Dictionary<string, object> data)
        {
            var jsonClass = new JSONClass();

            foreach (var field in data) {
                var key = field.Key;
                var value = field.Value;
                if (value is string) {
                    jsonClass[key] = value as string;
                } else if (value is int) {
                    jsonClass[key] = new JSONData((int)value);
                } else if (value is float) {
                    jsonClass[key] = new JSONData((float)value);
                }
            }

            return jsonClass;
        }
    }
}