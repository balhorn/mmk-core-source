﻿using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

namespace MMK.Core.Analytics
{
    public class AnalyticsManager : MonoBehaviour, IAnalyticsManager
    {
        private readonly List<IAnalyticsModule> modules = new List<IAnalyticsModule>();
         
        public void LogEvent(AnalyticsEvent eventType, Dictionary<string, object> data)
        {
            foreach (var module in modules) {
                module.LogEvent(eventType, data);
            }
        }

        public void AddModule(IAnalyticsModule module)
        {
            modules.Add(module);
        }

        private void Awake ()
        {
            API.AnalyticsManager = this;
        }
    }
}