﻿using System.Collections.Generic;

namespace MMK.Core.Analytics
{
    public interface IAnalyticsModule : IBlockingOperation
    {
        void LogEvent (AnalyticsEvent eventType, Dictionary<string, object> data);
    }
}