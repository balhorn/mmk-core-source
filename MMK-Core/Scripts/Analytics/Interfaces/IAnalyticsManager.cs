﻿using System.Collections.Generic;

namespace MMK.Core.Analytics
{
    public interface IAnalyticsManager
    {
        void LogEvent(AnalyticsEvent eventType, Dictionary<string, object> data);
        //void LogEvent(AnalyticsEvent eventType, List<object> data);

        void AddModule(IAnalyticsModule module);
    }
}