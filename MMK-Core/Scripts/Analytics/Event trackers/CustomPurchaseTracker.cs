﻿namespace MMK.Core.Analytics
{
    public class CustomPurchaseTracker : EventTracker
    {
        private void Start ()
        {
            API.AnalyticsManager.LogEvent(AnalyticsEvent.CustomPurchase, null);
        }    
    }
}