﻿using System.Collections.Generic;

namespace MMK.Core.Analytics
{
    public class InitiatePurchaseTracker : EventTracker
    {
        private void OnEnable ()
        {
            API.AnalyticsManager.LogEvent(AnalyticsEvent.InitiateCheckout, new Dictionary<string, object>());
        }
    }
}