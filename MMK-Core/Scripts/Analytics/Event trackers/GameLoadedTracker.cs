﻿using UnityEngine;

namespace MMK.Core.Analytics
{
    public class GameLoadedTracker : EventTracker
    {
        private void OnBuildLoaded ()
        {
            Application.ExternalCall("buildLoaded");
        }

        private void OnSceneLoaded ()
        {
            Application.ExternalCall("sceneLoaded");
        }

        private void Start ()
        {
            OnSceneLoaded();
        }

        private void Awake ()
        {
            OnBuildLoaded();
        }
    }
}