﻿namespace MMK.Core.Analytics
{
    public enum AnalyticsEvent
    {
        TutorialStep,
        TutorialComplete,
        LevelComplete,
        InitiateCheckout,
        Purchase,
        CustomPurchase,
        BuildLoaded,
        SceneLoaded
    }
}