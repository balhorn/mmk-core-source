﻿using System.Collections.Generic;

namespace MMK.Core.Analytics
{
    public class FakeAnalytics : IAnalyticsManager
    {
        public void LogEvent(AnalyticsEvent eventType, Dictionary<string, object> data)
        {
        }

        public void AddModule(IAnalyticsModule module)
        {
        }
    }
}