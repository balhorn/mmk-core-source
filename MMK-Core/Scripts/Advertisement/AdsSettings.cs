﻿using System;
using System.Collections.Generic;

namespace MMK.Core.Ads
{
    [Serializable]
    public class AdsSettings
    {
        private HashSet<AdsSpot> adsSpots = new HashSet<AdsSpot>();

        public void AddSpot(AdsSpot newSpot)
        {
            adsSpots.Add(newSpot);
        }

        public void RemoveSpot(AdsSpot spot)
        {
            adsSpots.Remove(spot);
        }

        public bool IsSpotEnabled(AdsSpot spot)
        {
            return adsSpots.Contains(spot);
        }
    }
}