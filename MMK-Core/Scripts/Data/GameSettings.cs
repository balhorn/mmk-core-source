﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MMK.Core
{
    public class GameSettings : ScriptableObject
    {
        [Header("Game mode constraints")]

        public int maximumBoostersAmount = 3;
        public int maximumScoreMilestones = 3;
        public int maximumLifes = 5;

        public int regainLifesHours = 0;
        public int regainLifesMinutes = 1;
        public int regainLifesSeconds = 0;

        public int levelsPerLocation = 40;
        public int fogOffset = 20;

        public int tokenScore = 60;

        public AudioClip backgroundMusic;
        public AudioClip levelStartSound;
        public AudioClip winSound;
        public AudioClip matchSound;
        public AudioClip furtherMatchSound;
        public AudioClip shuffleSound;
        public AudioClip tokenLandingSound;
        public AudioClip buttonTapSound;

        public int maximumFakeFriends;
        public List<FakeFriendSettings> fakeFriends;

        [HideInInspector]
        public List<BoosterInfo> boosterCosts;
        public MonetizationSettings Monetization { get; private set; }

        public const string MonetizationSettingsName = "Monetization Settings";

        protected virtual void OnEnable ()
        {
            Monetization = Resources.Load<MonetizationSettings>(MonetizationSettingsName);

            if (Monetization == null) {
                Debug.LogError($"Could not load monetization settings object. Please, change its name to '{MonetizationSettingsName}' or create a new one");
            }

        }

    }
}

[Serializable]
public class FakeFriendSettings
{
    public string name;
    public Sprite avatar;
}