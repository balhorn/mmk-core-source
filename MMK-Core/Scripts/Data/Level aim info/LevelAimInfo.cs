using System;
using System.Collections.Generic;
using MMK.Core.Aims;
using UnityEngine;

namespace MMK.Core
{
    [Serializable]
    public class LevelAimInfo
    {
        public string name;

        [HideInInspector]
        public string fullName;

        public int    amount;

        public Type   Type
        {
            get { return Type.GetType(fullName); }
            set
            {
                name = value.Name;
                fullName = value.AssemblyQualifiedName;
            }
        }

        public LevelAimInfo (Type aimType, int amount)
        {
            if (!aimType.IsSubclassOf(typeof(LevelAim))) {
                throw new ArgumentException("Provided type is not a level aim!", nameof(aimType));
            }

            Type = aimType;

            this.amount = amount;
        }

        public LevelAimInfo(string name, int amount)
        {
            this.name = name;
            this.amount = amount;
        }

        public LevelAimInfo()
        {

        }

        public LevelAim AttachTo (GameObject obj)
        {
            var aim = obj.AddComponent(Type) as LevelAim;

            if (aim == null) {
                return null;
            }

            aim.requiredAmount = amount;
            aim.UnbindEvent();
            aim.BindEvent();
            aim.InstantiateUIItem(obj);

            return aim;
        }
    }
}