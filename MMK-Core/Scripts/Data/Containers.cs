﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using MMK.Core;
using UnityEngine;

namespace MMK.Core
{
    [Serializable]
    public class Game : ISerializable
    {
        public List<LevelProperties> levels;
        public int nextLevelNumber = 2;

        public Game ()
        {
            levels = new List<LevelProperties>();
        }

        public Game (SerializationInfo info, StreamingContext context)
        {
            levels = (List<LevelProperties>)info.GetValue("levels", typeof(List<LevelProperties>));
            nextLevelNumber = info.GetInt32("nextLevelNumber");
        }

        public int LastCompletedLevel
        {
            get
            {
                try {
                    return levels.OrderBy(prop => prop.number).Last().number;
                } catch (InvalidOperationException) {
                    return 0;
                }
            }
        }

        public void UpdateLevel (Level level)
        {
            var item = new LevelProperties(level);

            if (levels.Contains(item)) {
                levels.Remove(item);
            }

            levels.Add(item);
        }

        public void GetObjectData (SerializationInfo info, StreamingContext context)
        {
            info.AddValue("levels", levels);
            info.AddValue("nextLevelNumber", nextLevelNumber);
        }

        public override string ToString ()
        {
            var builder = new StringBuilder();
            foreach (var level in levels) {
                builder.Append(level);
            }

            return builder.ToString();
        }

    }
}


[Serializable]
public class LevelProperties : IEquatable<LevelProperties>, ISerializable
{
    public int number;
    public bool isCompleted;
    public int highScore;
    public int starsCollected;

    public LevelProperties(){}

    public LevelProperties (Level level)
    {
        this.number = level.number;
        this.isCompleted = level.isCompleted;
        this.highScore = level.highScore;
        this.starsCollected = level.starsCollected;
    }

    public LevelProperties (SerializationInfo info, StreamingContext context)
    {
        number = info.GetInt32 ("number");
        isCompleted = info.GetBoolean ("isCompleted");
        highScore = info.GetInt32 ("highScore");
        starsCollected = info.GetInt32 ("starsCollected");
    }

    public bool Equals (LevelProperties other)
    {
        return number == other.number;
    }

    public void GetObjectData (SerializationInfo info, StreamingContext context)
    {
        info.AddValue ("number", number);
        info.AddValue ("isCompleted", isCompleted);
        info.AddValue ("highScore", highScore);
        info.AddValue ("starsCollected", starsCollected);
    }

    public override string ToString ()
    {
        var builder = new StringBuilder ();

        builder.Append ("Level #").Append (number).Append(":\n");
        builder.Append (isCompleted ? "Completed" : "Not completed").Append ('\n');
        builder.Append ("High score: ").Append (highScore).Append('\n');
        builder.Append (isCompleted ? "Stars: " + starsCollected : "").Append('\n');

        return builder.ToString ();
    }
    
}

public sealed class VersionDeserializationBinder : SerializationBinder 
{ 
    public override Type BindToType( string assemblyName, string typeName )
    {
        if (string.IsNullOrEmpty(assemblyName) || string.IsNullOrEmpty(typeName)) {
            return null;
        }

        Type typeToDeserialize = null; 

        assemblyName = Assembly.GetExecutingAssembly().FullName; 

        // The following line of code returns the type. 
        typeToDeserialize = Type.GetType($"{typeName}, {assemblyName}"); 

        return typeToDeserialize;
    } 
}

[Serializable]
public class BoosterInfo
{
    public int id;
    [XmlAttribute] public string name;
    [XmlAttribute] public int amount;
    public string alias;
    public string cost;

    public static string GetAliasByTypename (string typeName)
    {
        return AliasesMap.ContainsKey(typeName) ? AliasesMap[typeName] : string.Empty;
    }

    private static readonly Dictionary<string, string> AliasesMap = new Dictionary<string, string> {
        //{ nameof(ShuffleBooster), "shuffle_booster" }
    };

    public override string ToString()
    {
        return $"Id: {id}, Name: {name}, Amount: {amount}, Alias: {alias}, Cost: {cost}";
    }

    public bool Equals(BoosterInfo other)
    {
        if (ReferenceEquals(null, other)) { return false; }
        if (ReferenceEquals(this, other)) { return true; }
        return string.Equals(alias, other.alias);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) { return false; }
        if (ReferenceEquals(this, obj)) { return true; }
        if (obj.GetType() != this.GetType()) { return false; }
        return Equals((BoosterInfo) obj);
    }

    public override int GetHashCode()
    {
        return alias != null ? alias.GetHashCode() : 0;
    }
}
