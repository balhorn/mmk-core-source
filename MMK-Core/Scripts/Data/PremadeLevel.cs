﻿using UnityEngine;
using System.Collections.Generic;
using MMK.Core;
using UnityEngine.SceneManagement;

namespace MMK.Core
{
    public class PremadeLevel : ScriptableObject
    {
        public int number;

        public int constraintAmount;

        public List<int> progressValues = new List<int> {0, 0, 0};

        public string constraint;

        public RuntimeAnimatorController tutorialAnimator;

        public List<LevelAimInfo> aims = new List<LevelAimInfo>();
        public List<string> boosters = new List<string>();

        private readonly string LevelPrefabPath = Constants.MMKRoot + "Prefabs/Map/Level.prefab";

        public virtual void RunInTestMode(GameObject levelPrefab)
        {
            var levelInstance = Instantiate(levelPrefab);
            if (levelInstance == null) {
                Debug.LogErrorFormat("Level prefab wasn't found. Please, place it in this folder: {0}", LevelPrefabPath);
                return;
            }

            levelInstance.GetComponent<Collider2D>().enabled = false;

            var level = levelInstance.GetComponent<Level>();
            level.number = number;
            level.isTestMode = true;
            level.premadeLevel = this;
            Level.Current = level;
        }
    }
}