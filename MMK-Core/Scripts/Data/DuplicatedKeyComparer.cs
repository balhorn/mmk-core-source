﻿using System.Collections.Generic;

public class DuplicatedKeyComparer : IComparer<int>
{
    public int Compare(int x, int y)
    {
        var result = x.CompareTo(y);

        return result == 0 ? 1 : result;
    }
}
