﻿using System.Collections.Generic;
using System.Linq;
using MMK.Core.InApp;
using UnityEngine;

namespace MMK.Core
{
    [CreateAssetMenu(menuName = "MMK/Monetization settings")]
    public class MonetizationSettings : ScriptableObject
    {
        public List<IProduct> products;
        public string rateUsUrl;

        private void OnEnable()
        {
            products = Resources.LoadAll<PurchasableItem>("Products").Cast<IProduct>().ToList();
        }
    }
}