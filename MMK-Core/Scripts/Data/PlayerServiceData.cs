using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using MMK.Core;
using MMK.Core.Server;
using UnityEngine;
using UnityEngine.Networking;

namespace MMK.Core
{
    [Serializable]
    public class PlayerServiceData : IBlockingOperation
    {
        public int lifes = 5;

        private int hardCurrency = 100;
        private List<BoosterInfo> availableBoosters;
        private List<BoosterInfo> allBoosters;

        public int Id { get; private set; }
        public float SecondsToRestoreLife { get; set; }

        public int HardCurrency
        {
            get { return hardCurrency; }
            set { hardCurrency = value; }
        }

        public void GainLifes (int amount)
        {
            lifes = Mathf.Clamp(lifes + amount, 0, Constants.MaximumLifes);
        }

        public void DecreaseLife ()
        {
            lifes--;
            API.Server.UseLife(LivesCallback);
        }

        public void SpendCurrency (int amount)
        {
            hardCurrency = amount >= HardCurrency ? 0 : HardCurrency - amount;
        }

        public void GainCurrency (int amount)
        {
            hardCurrency = HardCurrency + amount;
        }

        public void GainBooster (Type boosterType, int amount)
        {
            var alias = BoosterInfo.GetAliasByTypename(boosterType.Name);
            var boosterInfo = allBoosters.Find(b => b.alias.Equals(alias));
            if (boosterInfo == null) {
                return;
            }

            boosterInfo.amount += amount;

            API.Server.BuyBooster(alias, amount, BoosterCallback);
        }

        private void BoosterCallback(IPlayerDataResponse response)
        {
            if (response.Error != null) {
                Debug.Log("Error on booster request: " + response.Error);
            }
        }

        public int GetBoosterAmount (string typeName)
        {
            var alias = BoosterInfo.GetAliasByTypename(typeName);
            try {
                return allBoosters.Find(b => b.alias.Equals(alias)).amount;
            } catch (NullReferenceException) {
                return 0;
            }
        }

        public BoosterInfo GetBoosterInfo (string alias)
        {
            return allBoosters.Find(b => b.alias == alias);
        }

        public void SpendBooster (string typeName)
        {
            var alias = BoosterInfo.GetAliasByTypename(typeName);
            var boosterInfo = allBoosters.Find(b => b.alias.Equals(alias));
            if (boosterInfo == null) {
                return;
            }

            boosterInfo.amount--;

            API.Server.UseBooster(alias, BoosterCallback);
        }

        public PlayerServiceData ()
        {
            availableBoosters = new List<BoosterInfo>();
            allBoosters = new List<BoosterInfo>();
        }

        public void InitPlayer ()
        {
            GameController.RegisterBlockingOperation(this, 3);

            try {
                API.Server.GetBoosters(GetBoostersCallback);
                API.Server.GetPlayerData(GetPlayerCallback);
                UpdateLifes();
            } catch (Exception e) {
                Debug.Log("Error initializating player: " + e);
                GameController.StopBlockingOperation(this);
            }
        }

        public void UpdateLifes()
        {
            API.Server.GetLives(LivesCallback);
        }

        private void LivesCallback(IServerResponse response)
        {
            GameController.CallbackExecuted(this);
            if (response.Error != null) {
                Debug.Log("Error getting lifes: " + response.Error);
                return;
            }

            lifes = response.Result.GetInteger("lives");
            var currentTimeString = response.Result.GetString("server_timestamp");
            var updateTimeString = response.Result.GetString("next_update");

            DateTime currentTime, updateTime;
            if (!DateTime.TryParse(currentTimeString, out currentTime) ||
                !DateTime.TryParse(updateTimeString, out updateTime)) {
                Debug.Log("Failed to parse the date string from server!");
                return;
            }

            var timeToPass = updateTime.Subtract(currentTime);
            SecondsToRestoreLife = (float) timeToPass.TotalSeconds;
        }

        private void GetPlayerCallback (IPlayerDataResponse response)
        {
            GameController.CallbackExecuted(this);

            if (response.Error != null) {
                Debug.Log("Error getting player data: " + response.Error);
                return;
            }

            hardCurrency = response.Money;
            lifes = 5;//response.Lifes;
            var boosters = response.Result.GetArray("boosters");
            for (int i = 0; i < boosters.ArrayCount; i++) {
                var boosterItem = boosters.GetArrayElementAt(i);
                var boosterInfo = JsonUtility.FromJson<BoosterInfo>(boosterItem.ToJson());

                availableBoosters.Add(boosterInfo);
            }
        }

        private void GetBoostersCallback(IServerResponse response)
        {
            GameController.CallbackExecuted(this);

            if (response.Error != null) {
                Debug.Log("Error getting boosters data: " + response.Error);
                return;
            }

            for (int i = 0; i < response.Result.ArrayCount; i++) {
                var booster = response.Result.GetArrayElementAt(i);
                var boosterInfo = JsonUtility.FromJson<BoosterInfo>(booster.ToJson());

                allBoosters.Add(boosterInfo);
            }
        }

        public IEnumerator UpdatePlayerValues()
        {
            using (var playerRequest = UnityWebRequest.Get(Constants.ServerURL + "/me")) {
                playerRequest.SetRequestHeader("access-token", API.Server.AccessToken);
                yield return playerRequest.Send();
                ProcessPlayerRequest(playerRequest);
            }
        }

        private void ProcessBoosterRequest(UnityWebRequest request)
        {
            if (request.isError) {
                Debug.Log("Error fetching boosters data: " + request.error);
                Debug.Log(request.downloadHandler.text);
                return;
            }

            if (request.responseCode != 200) {
                Debug.Log("Error fetching boosters data: " + request.downloadHandler.text);
                Debug.Log(request.downloadHandler.text);
                return;
            }

            //        foreach (var jsonObject in Utilites.FetchJSONObjects(request.downloadHandler.text)) {
            //            var boosterInfo = JsonUtility.FromJson<BoosterInfo>(jsonObject);
            //            allBoosters.Add(boosterInfo);
            //        }
        }

        private void ProcessPlayerRequest (UnityWebRequest request)
        {
            if (request.isError) {
                Debug.Log("Error fetching player data: " + request.error);
                Debug.Log(request.downloadHandler.text);
                return;
            }

            if (request.responseCode != 200) {
                Debug.Log("Error fetching player data: " + request.downloadHandler.text);
                Debug.Log(request.downloadHandler.text);
                return;
            }

            var jsonData = JsonUtility.FromJson<PlayerJsonData>(request.downloadHandler.text);
            hardCurrency = jsonData.money;
            Id = jsonData.id;
            foreach (var boosterInfo in jsonData.boosters) {
                var booster = allBoosters.Find(b => b.alias.Equals(boosterInfo.alias));
                if (booster == null) {
                    continue;
                }

                booster.amount = boosterInfo.amount;
                availableBoosters.Add(booster);
            }
        }

        [Serializable]
        private class PlayerJsonData
        {
            public int id;
            public int money;
            public List<BoosterInfo> boosters;
        }
    }
}