﻿using System;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using MMK.Core;
using MMK.Core.Server;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace MMK.Core
{
    public static class GameController
    {
        public static void Init ()
        {
            SceneManager.sceneUnloaded += OnSceneUnloaded;
        }

        private static void OnSceneUnloaded (Scene arg0)
        {
            ClearStack();
        }

        #region Serialization
        public static Game game = new Game ();
        public static GameState state = GameState.Map;

        private static readonly string FileName = Path.Combine(Application.persistentDataPath, "savedGame.game");

        public static byte[] GameBynary
        {
            get
            {
                var formatter = new BinaryFormatter();
                using (var stream = new MemoryStream()) {
                    formatter.Serialize(stream, game);
                    return stream.ToArray();
                }
            }
        }

        public static int LastCompletedLevel => game.LastCompletedLevel;
        public static List<LevelProperties> Levels => game.levels;

        public static void UpdateLevelData (Level level)
        {
            game.UpdateLevel(level);
        }

        public static void LoadGame ()
        {
            LoadFromServer();
        }

        public static void SaveGame()
        {
            SaveToServer();
        }

        private static void SaveToServer ()
        {
            API.Server.SubmitProgress(GameBynary, SaveGameCallback);
        }

        private static void SaveGameCallback(IServerResponse response)
        {
            if (response.Error != null) {
                Debug.Log("Error saving game: " + response.Error);
            }
        }

        private static IBlockingOperation loadingOperation;

        private static void LoadFromServer ()
        {
            loadingOperation = new ServerLoadingOperation();
            RegisterBlockingOperation(loadingOperation, 1);

            API.Server.GetProgress(GetProgressCallback);
        }

        private static void GetProgressCallback(IProgressResponse response)
        {
            CallbackExecuted(loadingOperation);
            if (response.Error != null) {
                Debug.Log("Error loading progress: " + response.Error);
                return;
            }

            try {
                var bynary = response.Progress;
                var formatter = new BinaryFormatter {Binder = new VersionDeserializationBinder()};
                using (var stream = new MemoryStream(bynary)) {
                    game = (Game) formatter.Deserialize(stream);
                }
            } catch (Exception e) {
                Debug.Log("Exception on game deserialization: " + e);
                CreateNewGame();
            }
        }

        private static void CreateNewGame ()
        {
            game = new Game();
            StopBlockingOperation(loadingOperation);
        }

        private class ServerLoadingOperation : IBlockingOperation {}

        #endregion

        #region Blocking operations

        private static readonly Dictionary<IBlockingOperation, int> blockingOperations
            = new Dictionary<IBlockingOperation, int>();

        private static float waitTime;

        private const float MaxWaitTime = 10f;
        private const float ShowWarningsOnceIn = 5f;

        public static bool BlockingOperationsRunning => blockingOperations.Any();

        public static void RegisterBlockingOperation (IBlockingOperation operation, int callbacksNumber)
        {
            waitTime = Time.unscaledTime + MaxWaitTime;
            blockingOperations[operation] = callbacksNumber;
        }

        public static void CallbackExecuted (IBlockingOperation operation)
        {
            if (!blockingOperations.ContainsKey(operation)) {
                return;
            }

            if (--blockingOperations[operation] <= 0) {
                blockingOperations.Remove(operation);
            }
        }

        public static void StopBlockingOperation (IBlockingOperation operation)
        {
            blockingOperations.Remove(operation);
        }

        public static void ShowWarningIfNecessary ()
        {
            if (Time.unscaledTime < waitTime || blockingOperations.Count == 0) {
                return;
            }

            waitTime = Time.unscaledTime + ShowWarningsOnceIn;

            Debug.Log("Some of blocking operations seem to hang up:");
            foreach (var blockingOperation in blockingOperations) {
                Debug.Log(blockingOperation.Key);
            }
        }

        #endregion

        #region Undoables

        private static readonly Stack<IUndoable> Undoables = new Stack<IUndoable>();

        public static void GoBack ()
        {
            try {
                var topItem = Undoables.Peek();
                topItem.Undo();
            } catch (InvalidOperationException) {
            }
        }

        public static void AddUndoable (IUndoable undoable)
        {
            Undoables.Push(undoable);
        }

        public static void PopUndoable ()
        {
            try {
                Undoables.Pop();
            } catch (InvalidOperationException) {
            }
        }

        private static void ClearStack ()
        {
            Undoables.Clear();
        }

        #endregion
    }
}