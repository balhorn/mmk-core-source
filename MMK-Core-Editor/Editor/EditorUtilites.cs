﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MMK.Core.Editor;
using UnityEditor;
using UnityEngine;

namespace MMK.Core.Editor
{
    public static class EditorUtilites
    {
        public const string PlayerPrefsKey = "lastEditedLevel";

        private static Color defaultColor;

        public static void BeginColoredArea (Color color)
        {
            defaultColor = GUI.backgroundColor;
            GUI.backgroundColor = color;
        }

        public static void EndColoredArea ()
        {
            GUI.backgroundColor = defaultColor;
        }

        public static void LabelFitContent (string label, GUIStyle style)
        {
            var content = new GUIContent(label);
            LabelFitContent(content, style);
        }

        public static void LabelFitContent (GUIContent label, GUIStyle style)
        {
            var rect = GUILayoutUtility.GetRect(label, style);
            EditorGUI.LabelField(rect, label, style);
        }

        public static bool ButtonFitContent (string label, GUIStyle style, params GUILayoutOption[] options)
        {
            var content = new GUIContent(label);
            return ButtonFitContent(content, style, options);
        }

        public static bool ButtonFitContent (GUIContent label, GUIStyle style, params GUILayoutOption[] options)
        {
            var rect = GUILayoutUtility.GetRect(label, style, options);
            return GUI.Button(rect, label);
        }

        public static int IntField (string label, int value)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(label, MMKStyles.label);
            value = EditorGUILayout.IntField(value, MMKStyles.centeredIntField, Height(28f), Width(42f));
            EditorGUILayout.EndHorizontal();

            return value;
        }

        public static float FloatField (string label, float value)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(label, MMKStyles.label);
            value = EditorGUILayout.FloatField(value, MMKStyles.centeredIntField, Height(28f), Width(42f));
            EditorGUILayout.EndHorizontal();

            return value;
        }

        public static int Popup (string label, int index, string[] options)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(label, MMKStyles.label);
            index = EditorGUILayout.Popup(index, options, MMKStyles.popup, Height(28f));
            EditorGUILayout.EndHorizontal();

            return index;
        }

        public static void Title (string title)
        {
            LabelFitContent(title, MMKStyles.boldLabel);
            Space(17f);
        }

        public static void BoldTitle (string title)
        {
            LabelFitContent(title, EditorStyles.boldLabel);
            Space(13f);
        }

        public static string TextField (string label, string text)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(label, MMKStyles.label);
            text = EditorGUILayout.TextField(text, MMKStyles.centeredTextField, Height(28f));
            EditorGUILayout.EndHorizontal();

            return text;
        }

        public static T ObjectField<T> (string label, T obj) where T : UnityEngine.Object
        {
            EditorGUILayout.BeginHorizontal();
            if (!string.IsNullOrEmpty(label)) {
                EditorGUILayout.PrefixLabel(label, MMKStyles.label);
            }
            obj = EditorGUILayout.ObjectField(obj, typeof (T), false, Height(28f)) as T;
            EditorGUILayout.EndHorizontal();

            return obj;
        }

        public static void Space (float referencePixels)
        {
            GUILayout.Space(CalculateSize(referencePixels));
        }

        public static void BeginOffsetBlock ()
        {
            EditorGUILayout.BeginVertical(MMKStyles.blockOffset);
        }

        public static void BeginOffsetBlock (string title)
        {
            BeginOffsetBlock();
            BoldTitle(title);
        }

        public static void EndOffsetBlock ()
        {
            EditorGUILayout.EndVertical();
        }

        public static Color HexToColor (string hex)
        {
            hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
            hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
            byte a = 255;//assume fully visible unless specified in hex
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            //Only use alpha if the string has enough characters
            if (hex.Length == 8) {
                a = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return new Color32(r, g, b, a);
        }

        private static readonly Dictionary<Sprite, Texture2D> spritesToTextures = new Dictionary<Sprite, Texture2D>();
        private static readonly Dictionary<Color, Texture2D> colorsToTextures = new Dictionary<Color, Texture2D>();

        public static Texture2D GetTexture (Sprite sprite)
        {
            if (sprite == null) {
                return null;
            }


            if (spritesToTextures.ContainsKey(sprite) && spritesToTextures[sprite] != null) {
                return spritesToTextures[sprite];
            }

            spritesToTextures[sprite] = EditorUtilites.FetchTexture(sprite);
            return spritesToTextures[sprite];
        }

        public static Texture2D GetTexture (Color color)
        {
            if (colorsToTextures.ContainsKey(color) && colorsToTextures[color] != null) {
                return colorsToTextures[color];
            }

            colorsToTextures[color] = EditorUtilites.FetchTexture(color);
            return colorsToTextures[color];
        }

        public static Texture2D GetTexture (string hexColor)
        {
            return GetTexture(EditorUtilites.HexToColor(hexColor));
        }

        public static Texture2D FetchTexture (Sprite sprite)
        {
            var primaryTexture = sprite != null ? sprite.texture : null;
            if (primaryTexture == null) {
                return null;
            }

            var texture = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
            var x = (int)sprite.rect.x;
            var y = (int)sprite.rect.y;
            var width = (int)sprite.rect.width;
            var height = (int)sprite.rect.height;

            try {
                var colors = primaryTexture.GetPixels (x, y, width, height);
                texture.SetPixels (0, 0, width, height, colors);
                texture.Apply ();

                return texture;
            } catch (UnityException) {
                Debug.LogError("Some texture is not readable. Please, close the level editor and click Setup in the MMK top menu");
                return new Texture2D (width, height);
            }
        }

        public static Texture2D FetchTexture (string hexColor)
        {
            return FetchTexture(HexToColor(hexColor));
        }

        public static Texture2D FetchTexture (Color color)
        {
            var colors = new Color32[60 * 60];
            for (int index = 0; index < colors.Length; index++) {
                colors[index] = color;
            }

            var texture = new Texture2D(60, 60);
            texture.SetPixels32(colors);
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.Apply();

            return texture;
        }

        public static GUILayoutOption Width (float referenceWidth)
        {
            return GUILayout.Width(CalculateSize(referenceWidth));
        }

        public static GUILayoutOption Height (float referenceHeight)
        {
            return GUILayout.Height(CalculateSize(referenceHeight));
        }

        public const float To1920x1080 = 1f;
        public const float To1600x900 = 0.86f;
        public const float To1366x768 = 0.8f;
        public const float To1280x720 = 0.8f;

        public static float CalculateSize (float referenceSize)
        {
            float actualSize;
            switch (Screen.currentResolution.width) {
                case 1920:
                    actualSize = referenceSize * To1920x1080;
                    break;
                case 1600:
                    actualSize = referenceSize * To1600x900;
                    break;
                case 1366:
                    actualSize = referenceSize * To1366x768;
                    break;
                case 1280:
                    actualSize = referenceSize * To1280x720;
                    break;
                default:
                    actualSize = referenceSize;
                    break;
            }

            return actualSize;
        }

        public static string[] GetTypeNames (Type t)
        {
            return GetDerivedTypes(t)
                .Select(type => type.Name)
                .ToArray();
        }

        public static Type[] GetDerivedTypes (Type t)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(assembly => assembly
                    .GetTypes()
                    .Where(type => type.IsSubclassOf(t) && !type.IsAbstract))
                .ToArray();
            return Assembly.GetAssembly(t)
                .GetTypes()
                .Where(type => !type.IsAbstract && type.IsSubclassOf(t))
                .ToArray();
        }
    }
}
