﻿using UnityEditor;
using UnityEngine;

namespace MMK.Core.Editor
{
    public class AddLocationWindow : EditorWindow
    {
        public MapEditorTab MapEditor { get; set; }

        private GameObject selectedPrefab;

        private void OnGUI ()
        {
            selectedPrefab =
                EditorGUILayout.ObjectField("Location sprite", selectedPrefab, typeof (GameObject), false) as GameObject;

            EditorGUILayout.Space();

            EditorGUI.indentLevel += 6;
            if (GUILayout.Button("Done", GUILayout.Width(150f))) {
                MapEditor.AddAnotherLocation(Instantiate(selectedPrefab));
                Close();
            }
        }
    }
}
