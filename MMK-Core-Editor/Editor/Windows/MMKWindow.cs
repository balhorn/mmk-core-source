﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace MMK.Core.Editor
{
    public class MMKWindow : EditorWindow, IDisposable
    {
        public static MMKWindow Instance => instance;

        protected readonly List<EditorTab> tabs = new List<EditorTab>();
        protected GUIContent[] tabsContents;
        protected GUIContent helpMessage;
        protected int currentTab = -1;

        protected static Type windowType = typeof(MMKWindow);
        protected static MMKWindow instance;

        [MenuItem("Window/MMK/Map editor", false, 3)]
        public static MMKWindow OpenMapEditor()
        {
            if (instance == null) {
                OpenWindow();
            }

            instance.OpenTab<MapEditorTab>();
            return instance;
        }

        [MenuItem("Window/MMK/Settings editor", false, 5)]
        public static MMKWindow OpenSettingsEditor()
        {
            if (instance == null) {
                OpenWindow();
            }

            instance.OpenTab<SettingsTab>();
            return instance;
        }

        public static bool Active()
        {
            return instance != null;
        }

        public static void CloseWindow()
        {
            if (instance == null) {
                return;
            }

            instance.Dispose();
            instance.Close();
            instance = null;
        }

        public static MMKWindow OpenWindow()
        {
            if (instance == null) {
                instance = GetWindow(windowType, true, "MMK", true) as MMKWindow;
                instance.ShowUtility();
            }

            instance.maxSize = new Vector2(EditorUtilites.CalculateSize(668), 724);
            instance.Repaint();

            return instance;
        }

        public static T FindTab<T>() where T : EditorTab
        {
            return instance.tabs.OfType<T>().FirstOrDefault();
        }

        public virtual void Dispose()
        {

        }

        protected virtual void DrawWindow()
        {
            EditorGUILayout.BeginHorizontal();
            {
                EditorGUILayout.BeginVertical(MMKStyles.leftTab, EditorUtilites.Width(148f));
                EditorGUI.BeginChangeCheck();
                var selectedTab = GUILayout.SelectionGrid(
                    currentTab,
                    tabsContents,
                    1,
                    MMKStyles.selectionGrid,
                    EditorUtilites.Width(148f),
                    EditorUtilites.Height(tabsContents.Length * 110));

                if (EditorGUI.EndChangeCheck() && selectedTab != currentTab) {
                    tabs[currentTab].OnTabClosed();
                    tabs[selectedTab].OnTabOpened();
                    currentTab = selectedTab;
                }

                GUILayout.FlexibleSpace();

                GUILayout.Box(helpMessage, MMKStyles.helpBox, EditorUtilites.Width(143f));
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical(MMKStyles.body);

                tabs[currentTab].Draw();

                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
        }

        protected virtual void InitTabs()
        {
            AddTab<MapEditorTab>();
            AddTab<SettingsTab>();

            tabsContents = contentsList.ToArray();
        }

        private readonly List<GUIContent> contentsList = new List<GUIContent>();

        protected void AddTab<T>() where T : EditorTab, new()
        {
            AddTab<T>(tabs.Count);
        }

        protected void AddTab<T>(int index) where T : EditorTab, new()
        {
            try {
                var newTab = new T();
                tabs.Insert(index, newTab);
                contentsList.Insert(index, newTab.EditorButtonContent);
            } catch (Exception e) {
                Debug.LogError($"Error on tab initialization: {typeof(T).Name}\n{e}");
            }
        }

        protected void OverrideTab<TBase, TOverride>()
            where TOverride : TBase, new()
            where TBase : EditorTab, new()
        {
            for (int i = 0; i < tabs.Count; i++) {
                if (!(tabs[i] is TBase)) {
                    continue;
                }

                tabs[i] = new TOverride();
                return;
            }
        }

        protected void OpenTab<T>() where T : EditorTab
        {
            for (var index = 0; index < tabs.Count; index++) {
                if (!(tabs[index] is T))
                    continue;

                currentTab = index;
                tabs[currentTab].OnTabOpened();
                return;
            }
        }

        protected virtual void OnEnable()
        {
            if (instance == null) {
                instance = this;
            }

            helpMessage = new GUIContent();
            InitTabs();
        }

        protected virtual void OnGUI()
        {
            DrawWindow();
        }
    }
}
 
 