﻿using UnityEditor;
using UnityEditor.SceneManagement;

namespace MMK.Core.Editor
{
    public class EditorsMenu
    {
        [MenuItem("Open scene/Starting scene")]
        public static void OpenStartingScene ()
        {
            OpenScene(Constants.StartingMenuName, Constants.MMKCoreRoot);
        }

        [MenuItem("Open scene/Main menu", false, 0)]
        public static void OpenMenu ()
        {
            OpenScene(Constants.MainMenuScene, Constants.MMKCoreRoot);
        }

        [MenuItem("Open scene/Map", false, 0)]
        public static void OpenMap ()
        {
            OpenScene(Constants.MapScene, Constants.MMKCoreRoot);
        }

        //[MenuItem("Open scene/Game", false, 0)]
        //public static void OpenGame ()
        //{
        //    OpenScene(Constants.MainGameScene);
        //}

        public static void OpenScene (string name, string path)
        {
            if (MMKWindow.Active()) {
                MMKWindow.CloseWindow();
                EditorSceneManager.OpenScene(path + "Scenes/" + name + ".unity");
            } else if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo()) {
                EditorSceneManager.OpenScene(path + "Scenes/" + name + ".unity");
            }
        }
    }
}
