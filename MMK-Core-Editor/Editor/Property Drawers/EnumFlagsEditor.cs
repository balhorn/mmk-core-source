﻿using MMK.Core;
using UnityEditor;
using UnityEngine;

namespace MMK.Core.Editor
{
    [CustomPropertyDrawer(typeof (EnumFlagsAttribute))]
    public class EnumFlagsDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            property.intValue = EditorGUI.MaskField(position, label, property.intValue, property.enumNames);
        }
    }
}