﻿using UnityEditor;

namespace MMK.Core.Editor
{
    [CustomEditor(typeof(GameSettings))]
    public class GameSettingsEditor : UnityEditor.Editor
    {
        private GameSettings instance;
        private SerializedProperty fakeFriendsProperty;

        private void OnEnable ()
        {
            instance = (GameSettings) target;

            fakeFriendsProperty = serializedObject.FindProperty("fakeFriends");
            fakeFriendsProperty.arraySize = instance.maximumFakeFriends;
        }

        public override void OnInspectorGUI ()
        {
            serializedObject.Update();
            EditorGUI.BeginChangeCheck();

            #region Game mode constraints

            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Game mode contsraints");
            instance.maximumBoostersAmount = EditorUtilites.IntField("Maximum booster amount",
                instance.maximumBoostersAmount);
            instance.maximumScoreMilestones = EditorUtilites.IntField("Maximum score milestones",
                instance.maximumScoreMilestones);
            instance.maximumLifes = EditorUtilites.IntField("Maximum lifes",
                instance.maximumLifes);


            EditorUtilites.Space(15f);

            instance.regainLifesHours = EditorUtilites.IntField("Regain life hours",
                instance.regainLifesHours);
            instance.regainLifesMinutes = EditorUtilites.IntField("Regain life minutes",
                instance.regainLifesMinutes);
            instance.regainLifesSeconds = EditorUtilites.IntField("Regain life seconds",
                instance.regainLifesSeconds);

            EditorUtilites.EndOffsetBlock();

            #endregion

            #region MapSettings

            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Map settings");
            instance.levelsPerLocation = EditorUtilites.IntField("Levels per location", instance.levelsPerLocation);
            instance.fogOffset = EditorUtilites.IntField("Fog offset", instance.fogOffset);
            EditorUtilites.EndOffsetBlock();
            #endregion

            #region Score settings

            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Score settings");
            instance.tokenScore = EditorUtilites.IntField("Token score", instance.tokenScore);
            EditorUtilites.EndOffsetBlock();

            #endregion

            #region Audio settings

            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Audio settings");
            instance.backgroundMusic = EditorUtilites.ObjectField("Background music", instance.backgroundMusic);
            instance.levelStartSound = EditorUtilites.ObjectField("Level start sound", instance.levelStartSound);
            instance.winSound = EditorUtilites.ObjectField("Win sound", instance.winSound);
            instance.matchSound = EditorUtilites.ObjectField("Match sound", instance.matchSound);
            instance.furtherMatchSound = EditorUtilites.ObjectField("Further match sound", instance.furtherMatchSound);
            instance.shuffleSound = EditorUtilites.ObjectField("Shuffle sound", instance.shuffleSound);
            instance.tokenLandingSound = EditorUtilites.ObjectField("Token landing sound", instance.tokenLandingSound);
            instance.buttonTapSound = EditorUtilites.ObjectField("Button tap sound", instance.buttonTapSound);
            EditorUtilites.EndOffsetBlock();

            #endregion

            #region  Other settings
            DrawFakeFriendsSettings();
            #endregion

            serializedObject.ApplyModifiedProperties();
            if (EditorGUI.EndChangeCheck()) {
                EditorUtility.SetDirty(instance);
            }
        }

        private void DrawFakeFriendsSettings ()
        {
            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Fake friends settings");
            EditorGUI.BeginChangeCheck();
            instance.maximumFakeFriends = EditorUtilites.IntField("Maximum fake friends", instance.maximumFakeFriends);
            if (EditorGUI.EndChangeCheck()) {
                fakeFriendsProperty.arraySize = instance.maximumFakeFriends;
            }
            EditorGUILayout.PropertyField(fakeFriendsProperty, true);
            EditorUtilites.EndOffsetBlock();
        }

    }
}
