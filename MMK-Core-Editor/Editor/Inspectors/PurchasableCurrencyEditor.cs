﻿using MMK.Core.InApp;
using UnityEditor;

namespace MMK.Core.Editor
{
    [CustomEditor(typeof(PurchasableCurrency))]
    public class PurchasableCurrencyEditor : PurchasableItemEditor
    {
        protected override void DrawRightColumnProprties()
        {
            var currency = GetInstance<PurchasableCurrency>();

            EditorGUILayout.BeginVertical();
            EditorGUILayout.PrefixLabel("Amout");
            currency.amount = EditorGUILayout.IntField(currency.amount);
            EditorGUILayout.EndVertical();
        }
    }
}
