﻿using System;
using System.Collections.Generic;
using System.Linq;
using MMK.Core.InApp;
using UnityEditor;
using UnityEngine;

namespace MMK.Core.Editor
{
    [CustomEditor(typeof (MonetizationSettings))]
    public class ShopSettingsEditor : UnityEditor.Editor
    {
        private MonetizationSettings instance;
        private readonly List<Type> productTypes = Utilites.GetDerivedTypes(typeof (PurchasableItem)).ToList();

        private GUIContent editButtonContent;
        private GUIContent deleteButtonContent;

        private const float ProductInstanceHeight = 50f;
        private const float ButtonSize = 25f;

        private const string ProductsFolder = Constants.MMKRoot + "Resources/Products/";

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            DrawProducts();
            DrawRateSettings();

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawRateSettings()
        {
            instance.rateUsUrl = EditorUtilites.TextField("Rate us url", instance.rateUsUrl);
        }

        #region Products

        private void DrawProducts()
        {
            EditorUtilites.BeginOffsetBlock("Shop settings");

            foreach (var product in instance.products.Cast<PurchasableItem>()) {
                DrawProductItem(product);
            }

            if (GUILayout.Button("Add new product")) {
                AddNewProduct();
            }

            EditorUtilites.EndOffsetBlock();
        }

        private void AddNewProduct()
        {
            var menu = new GenericMenu();
            foreach (var type in productTypes) {
                var itemContent = new GUIContent(Utilites.SplitCamelCase(type.Name));
                menu.AddItem(itemContent, true, AddProduct, type);
            }
            menu.ShowAsContext();
        }

        private void AddProduct(object userData)
        {
            var type = userData as Type;
            var newProduct = CreateInstance(type) as PurchasableItem;
            instance.products.Add(newProduct);

            EditorUtility.SetDirty(instance);
            AssetDatabase.CreateAsset(newProduct, ProductsFolder + "New product.asset");
        }

        private void DrawProductItem(PurchasableItem product)
        {
            EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.Height(ProductInstanceHeight));
            GUILayout.FlexibleSpace();

            EditorGUILayout.BeginHorizontal();
            var productLabel = $"{product.name} ({product.GetType().Name})";
            EditorGUILayout.LabelField(productLabel);

            GUILayout.FlexibleSpace();

            if (GUILayout.Button(editButtonContent, GUILayout.Height(ButtonSize), GUILayout.Width(ButtonSize))) {
                EditProduct(product);
            }

            if (GUILayout.Button(deleteButtonContent, GUILayout.Height(ButtonSize), GUILayout.Width(ButtonSize))) {
                DeleteProduct(product);
            }

            EditorGUILayout.EndHorizontal();

            GUILayout.FlexibleSpace();
            EditorGUILayout.EndVertical();

            GUILayout.Space(5f);
        }

        private void DeleteProduct(PurchasableItem product)
        {
            instance.products.Remove(product);

            var productPath = AssetDatabase.GetAssetPath(product);
            AssetDatabase.DeleteAsset(productPath);
        }

        private void EditProduct(PurchasableItem product)
        {
            Selection.activeObject = product;
        }

        #endregion

        private void OnEnable ()
        {
            instance = (MonetizationSettings) target;

            editButtonContent = new GUIContent {
                text = "E"
            };

            deleteButtonContent = new GUIContent {
                text = "D"
            };
        }
    }
}
