﻿using System;
using MMK.Core.Editor;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace MMK.Core.Editor
{
    [CustomEditor(typeof (Level))]
    public class LevelEditor : UnityEditor.Editor
    {
        private LevelController controller;
        private Level level;
        private Transform levelParent;

        private SerializedProperty numberProperty;

        private const string NumberPropertyName = "number";

        public static event Delegates.WithIntParam OnLevelRemoved;

        public Delegates.VoidDelegate levelUpdated;

        private void OnEnable ()
        {
            level = (Level) target;
            controller = Camera.main.GetComponent<LevelController>();
            levelParent = GameObject.FindGameObjectWithTag("Levels")?.transform;
            numberProperty = serializedObject.FindProperty(NumberPropertyName);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.PropertyField(numberProperty);
            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Edit")) {
                MoveToGameScene();
            }
        }

        #region Level building
        private void MoveToGameScene()
        {
            var number = level.number;
            EditorSceneManager.OpenScene(Constants.MMKRoot + "Scenes/mainGame.unity");
            MMKWindow.OpenWindow();
        }

        #endregion

        #region Map editing

        public void InsertBetween (Level previous, Level next)
        {
            if (!controller.Levels.Contains(level)) {
                InsertNewLevel(previous, next);
            } else {
                UpdateLevel(previous, next);
            }

            //OnLevelInserted?.Invoke(level.number);
        }

        private void InsertNewLevel (Level previous, Level next)
        {
            int index;
            if (previous == null && next == null) {
                level.Number = 1;
                index = 0;
            } else if (previous != null && next == null) {
                level.Number = previous.number + 1;
                index = controller.Levels.Count;
            } else {
                level.Number = next.number;
                index = next.number - 1;
                for (int i = index; i < controller.Levels.Count; ++i) {
                    controller.Levels[i].number++;
                    EditorUtility.SetDirty(controller.Levels[i]);
                }
            }

            controller.Levels.Insert(index, level);
            level.transform.parent = levelParent;
        }

        private void UpdateLevel (Level previous, Level next)
        {
            if (previous == null && next != null && next.number > 1) {
                previous = controller.GetPreviousLevel(level.number == next.number - 1 ? level : next);
            }

            var index = previous != null ? previous.number : 0;
            index -= Convert.ToInt32(previous != null && previous.number > level.number);

            controller.Levels.Remove(level);
            controller.Levels.Insert(index, level);

            for (int i = 0; i < controller.Levels.Count; i++) {
                controller.Levels[i].Number = i + 1;
                EditorUtility.SetDirty(controller.Levels[i]);
            }
        }

        public void RemoveLevel()
        {
            int number = level.number;
            //levelUpdated?.Invoke();
            OnLevelRemoved?.Invoke(number);
            if (number == 0) {
                return;
            }

            for (int i = number; i < controller.Levels.Count; ++i) {
                controller.Levels[i].Number -= 1;
                EditorUtility.SetDirty(controller.Levels[i]);
            }

            level.Number = 0;
            controller.Levels.Remove(level);


            level.gameObject.name = "Odd level";
        }
        #endregion
    }
}
