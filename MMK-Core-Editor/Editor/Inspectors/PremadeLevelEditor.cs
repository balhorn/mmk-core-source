﻿using MMK.Core.Editor;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace MMK.Core.Editor
{
    [CustomEditor(typeof (PremadeLevel))]
    public class PremadeLevelEditor : UnityEditor.Editor
    {
        private PremadeLevel premadeLevel;

        private void OnEnable ()
        {
            premadeLevel = (PremadeLevel) target;
        }

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("Edit")) {
                var number = premadeLevel.number;
                EditorSceneManager.OpenScene(Constants.MMKRoot + "Scenes/mainGame.unity");
                MMKWindow.OpenWindow();
            }

            //if (GUILayout.Button("Test level")) {
            //    premadeLevel.RunInTestMode();
            //}

            //TODO Draw readonly inspector similar to level editor window
            DrawDefaultInspector();
        }
    }
}
