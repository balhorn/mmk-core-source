﻿using System;
using UnityEngine;
using System.Collections;
using MMK.Core;
using MMK.Core.UI;
using UnityEditor;
using UnityEngine.UI;

//[CustomEditor(typeof (LoadingScreen))]
public class LoadingScreenEditor : Editor
{
    private LoadingScreen screen;

    private void OnEnable ()
    {
        screen = (LoadingScreen) target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUI.BeginChangeCheck();

        screen.initialSceneName = EditorGUILayout.TextField("Initial scene", screen.initialSceneName);
        screen.fadeDuration = EditorGUILayout.FloatField("Fade duration", screen.fadeDuration);

        screen.indicator = (LoadingScreen.IndicatorType) EditorGUILayout.EnumPopup("Indicator type", screen.indicator);

        switch (screen.indicator) {
            case LoadingScreen.IndicatorType.ProgressBar:
                screen.progressBar =
                    (ProgressBarFiller) EditorGUILayout.ObjectField("Progress bar", screen.progressBar, typeof (ProgressBarFiller),
                        true);
                break;
            case LoadingScreen.IndicatorType.Spinner:
                screen.spinner =
                    (GameObject) EditorGUILayout.ObjectField("Spinner", screen.spinner, typeof (GameObject), true);
                screen.rotatingSpeed = EditorGUILayout.Slider("Rotating speed", screen.rotatingSpeed, 0f, 10f);
                break;
        }

        screen.progressText =
            (Text)EditorGUILayout.ObjectField("Progress text", screen.progressText, typeof(Text), true);

        if (EditorGUI.EndChangeCheck()) {
            EditorUtility.SetDirty(screen);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
