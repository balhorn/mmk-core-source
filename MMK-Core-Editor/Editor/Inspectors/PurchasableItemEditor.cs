﻿using MMK.Core.InApp;
using UnityEditor;
using UnityEngine;

namespace MMK.Core.Editor
{
    public class PurchasableItemEditor : UnityEditor.Editor
    {
        private PurchasableItem instance;

        private readonly GUILayoutOption[] spriteOptions = {
            GUILayout.Height(100f),
            GUILayout.Width(100f)
        };

        protected virtual void OnEnable ()
        {
            instance = (PurchasableItem) target;
        }

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginVertical();
            instance.icon = EditorGUILayout.ObjectField(instance.icon, typeof (Sprite), false, spriteOptions) as Sprite;
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            EditorGUILayout.PrefixLabel("Product id");
            instance.id = EditorGUILayout.TextField(instance.id);
            instance.mostPopular = EditorGUILayout.ToggleLeft("Most popular", instance.mostPopular);
            DrawRightColumnProprties();
            EditorGUILayout.EndVertical();

            EditorGUILayout.EndHorizontal();

            DrawCentralProperties();

            if (EditorGUI.EndChangeCheck()) {
                EditorUtility.SetDirty(instance);
            }
        }

        protected T GetInstance<T> () where T : PurchasableItem
        {
            return instance as T;
        }

        protected virtual void DrawRightColumnProprties ()
        {
        }

        protected virtual void DrawCentralProperties()
        {
        }
    }
}
