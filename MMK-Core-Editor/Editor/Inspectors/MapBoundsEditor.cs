﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace MMK.Core.Editor
{
    [CustomEditor(typeof (MapController))]
    public class MapBoundsEditor : UnityEditor.Editor
    {
        private MapController controller;

        private Rect strictRectangle;
        private Rect brackingRectangle;

        private Rect strictRectangleCached;
        private Rect brackingRectangleCached;

        private Camera camera;

        public Camera Camera => camera ?? (camera = Camera.main);

        private void OnEnable ()
        {
            controller = (MapController)target;
        }

        private void OnSceneGUI ()
        {
            brackingRectangle = controller.yieldingBounds;
            strictRectangle = controller.strictBounds;

            BeginChangeCheck();

            if (Mathf.Approximately(brackingRectangle.size.magnitude, 0f)) {
                SetDefaultCameraBounds(ref brackingRectangle);
            }
            if (Mathf.Approximately(strictRectangle.size.magnitude, 0f)) {
                SetDefaultCameraBounds(ref strictRectangle);
            }

            DrawHandles(ref brackingRectangle);
            DrawHandles(ref strictRectangle);

            controller.yieldingBounds = brackingRectangle;
            controller.strictBounds = strictRectangle;

            if (EndChangeCheck()) {
                Undo.RecordObject(controller, "Map controller");
            }
        }

        private void SetDefaultCameraBounds(ref Rect rect)
        {
            var cameraHeight = Camera.orthographicSize * 2f;
            var cameraWidth = (Screen.width * 1f / Screen.height) * cameraHeight;

            rect.center = Camera.transform.position;
            rect.size = new Vector2(cameraWidth, cameraHeight);
        }

        public void AdjustBounds (List<SpriteRenderer> locations)
        {
            if (locations == null || locations.Count == 0) {
                return;
            }

            var minValue = locations.Min(location => location.bounds.center.y);
            var maxValue = locations.Max(location => location.bounds.center.y);

            var bottomBorder = locations.Find(location => Math.Abs(location.bounds.center.y - minValue) < 0.01f);

            if (locations.Count == 1) {
                AdjustBounds(bottomBorder, bottomBorder);
                return;
            }

            var topBorder = locations.Find(location => Math.Abs(location.bounds.center.y - maxValue) < 0.01f);

            AdjustBounds(bottomBorder, topBorder);
        }

        public void AdjustBounds (SpriteRenderer bottom, SpriteRenderer top)
        {
            if (top == null || bottom == null) {
                throw new System.NullReferenceException("One of the map borders is not defined");
            }

            var pixelsPerUnit = bottom.sprite.pixelsPerUnit;

            var minY  = bottom.bounds.min.y;
            var maxY  = top.bounds.max.y;
            var width = Camera.aspect * 2f * Camera.orthographicSize;

            var centerY = (minY + maxY) / 2;
            var centerX = top.transform.position.x;

            strictRectangle.yMin = minY;
            strictRectangle.yMax = maxY;
            strictRectangle.width = width;
            strictRectangle.center = new Vector2(centerX, centerY);

            minY = strictRectangle.yMin + controller.pixelsBottomOffset / pixelsPerUnit;
            maxY = strictRectangle.yMax - controller.pixelsTopOffset / pixelsPerUnit;
            centerY = (minY + maxY) / 2;

            brackingRectangle.yMin = minY;
            brackingRectangle.yMax = maxY;
            brackingRectangle.width = width;
            brackingRectangle.center = new Vector2(centerX, centerY);

            controller.strictBounds   = strictRectangle;
            controller.yieldingBounds = brackingRectangle;

            Undo.RecordObject(controller, "Map controller");
        }

        private void DrawHandles (ref Rect rect)
        {
            rect.center = Handles.FreeMoveHandle(
                rect.center,
                Quaternion.identity,
                HandleUtility.GetHandleSize(rect.center) * 0.1f,
                new Vector3(1f, 1f, 1f),
                Handles.CircleCap
            );

            rect.xMin =
                Handles.ScaleValueHandle(
                    rect.xMin,
                    rect.center - new Vector2(rect.width / 2, 0f),
                    Quaternion.identity,
                    HandleUtility.GetHandleSize(rect.center) * 0.5f,
                    Handles.CubeCap,
                    0.3f);

            rect.xMax =
                Handles.ScaleValueHandle(
                    rect.xMax,
                    rect.center + new Vector2(rect.width / 2, 0f),
                    Quaternion.identity,
                    HandleUtility.GetHandleSize(rect.center) * 0.5f,
                    Handles.CubeCap,
                    0.3f);

            rect.yMax =
                Handles.ScaleValueHandle(
                    rect.yMax,
                    rect.center + new Vector2(0f, rect.height / 2),
                    Quaternion.identity,
                    HandleUtility.GetHandleSize(rect.center) * 0.5f,
                    Handles.CubeCap,
                    0.3f);

            rect.yMin =
                Handles.ScaleValueHandle(
                    rect.yMin,
                    rect.center - new Vector2(0f, rect.height / 2),
                    Quaternion.identity,
                    HandleUtility.GetHandleSize(rect.center) * 0.5f,
                    Handles.CubeCap,
                    0.3f);
        }

        private void BeginChangeCheck ()
        {
            strictRectangleCached = strictRectangle;
            brackingRectangleCached = brackingRectangle;
        }

        private bool EndChangeCheck ()
        {
            return !Mathf.Approximately(Vector3.Distance(strictRectangle.center, strictRectangleCached.center), 0f)
                   || !Mathf.Approximately(strictRectangle.xMin, strictRectangleCached.xMin)
                   || !Mathf.Approximately(strictRectangle.xMax, strictRectangleCached.xMax)
                   || !Mathf.Approximately(strictRectangle.yMin, strictRectangleCached.yMin)
                   || !Mathf.Approximately(strictRectangle.yMax, strictRectangleCached.yMax)
                   || !Mathf.Approximately(brackingRectangle.xMin, brackingRectangleCached.xMin)
                   || !Mathf.Approximately(brackingRectangle.xMax, brackingRectangleCached.xMax)
                   || !Mathf.Approximately(brackingRectangle.yMin, brackingRectangleCached.yMin)
                   || !Mathf.Approximately(brackingRectangle.yMax, brackingRectangleCached.yMax);
        }
    }
}

