﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(GUISkin))]
public class GUISkinEditor : Editor
{
    private GUISkin skin;
    private int currentFieldIndex;
    private string[] fields;
    private int standardStylesCount;
    private static GUIStyle styleInBuffer;

    private string CurrentStyle
    {
        get { return fields[currentFieldIndex]; }
    }

    private bool IsCurrentCustom
    {
        get { return currentFieldIndex >= standardStylesCount; }
    }

    private void OnEnable ()
    {
        skin = (GUISkin)target;

        UpdateFields();
    }

    private void UpdateFields ()
    {
        fields = new string[] { "box", "label", "textField", "textArea", "button", "toggle", "window", "horizontalSlider", "horizontalSliderThumb", "verticalSlider",
        "verticalSliderThumb", "horizontalScrollbar", "horizontalScrollbarThumb", "horizontalScrollbarLeftButton", "horizontalScrollbarRightButton", "verticalScrollbar", "verticalScrollbarThumb", "verticalScrollbarUpButton", "verticalScrollbarDownButton", "scrollView",
        null };

        standardStylesCount = fields.Length;

        // добавим кастомные
        System.Array.Resize(ref fields, fields.Length + skin.customStyles.Length);
        int i = standardStylesCount;
        foreach (var style in skin.customStyles) {
            fields[i++] = style.name;
        }

    }

    public override void OnInspectorGUI ()
    {
        base.OnInspectorGUI();

        //if(GUI.changed)
        UpdateFields();

        EditorGUILayout.Space();
        GUILayout.Label("Style Editor", EditorStyles.boldLabel);

        currentFieldIndex = EditorGUILayout.Popup("Current Style: ", currentFieldIndex, fields);
        if (currentFieldIndex == standardStylesCount - 1)
            currentFieldIndex = standardStylesCount - 2;

        if (GUILayout.Button("Copy current to buffer")) {
            styleInBuffer = new GUIStyle(skin.FindStyle(CurrentStyle));
        }
        GUI.enabled = IsCurrentCustom;
        if (GUILayout.Button("Cut current to buffer")) {
            CutStyle();
        }
        GUI.enabled = styleInBuffer != null &&  IsCurrentCustom;
        if (GUILayout.Button("Paste buffer to current")) {
            PasteStyle();
        }
        GUI.enabled = styleInBuffer != null;
        if (GUILayout.Button("Paste buffer to new")) {
            PasteNew();
        }
        GUI.enabled = true;
        if (GUILayout.Button("Add default style")) {
            styleInBuffer = new GUIStyle {name = "default"};
            PasteNew();
        }


        EditorGUILayout.LabelField("BUFFER: ", (styleInBuffer != null ? styleInBuffer.name : "<empty>"));
    }

    private void CutStyle ()
    {
        styleInBuffer = skin.FindStyle(CurrentStyle);
        if (IsCurrentCustom) {
            RemoveCurrent();
        }
    }

    private void RemoveCurrent ()
    {
        if (!IsCurrentCustom)
            return;

        List<GUIStyle> list = new List<GUIStyle>();

        foreach (var s in skin.customStyles) {
            if (s.name != CurrentStyle)
                list.Add(s);
        }

        skin.customStyles = list.ToArray();

        EditorUtility.SetDirty(skin);
        UpdateFields();
    }

    private void PasteNew ()
    {
        GUIStyle[] styles = skin.customStyles;
        System.Array.Resize(ref styles, skin.customStyles.Length + 1);
        styles[styles.Length - 1] = new GUIStyle(styleInBuffer);
        styles[styles.Length - 1].name = "new " + styles[styles.Length - 1].name;
        skin.customStyles = styles;
        EditorUtility.SetDirty(skin);
        UpdateFields();
    }

    private void PasteStyle ()
    {
        int i = currentFieldIndex - standardStylesCount;
        skin.customStyles[i] = new GUIStyle(styleInBuffer);
        EditorUtility.SetDirty(skin);
        UpdateFields();
    }

}