﻿using UnityEditor;
using UnityEngine;

namespace MMK.Core.Editor
{
    [InitializeOnLoad]
    public static class MMKStyles
    {
        private static readonly Font OpenSansSemibold;
        private static readonly Font OpenSansBold;

        private static readonly GUISkin MMKSkin;

        public static GUIStyle boldLabel => GetStyleByName(BoldLabelStyleName)/* = new GUIStyle(EditorStyles.label) {
            font = OpenSansBold,
            fontSize = 14,
        }*/;

        public static GUIStyle articleLabel => GetStyleByName(ArticleLableStyleName)/* = new GUIStyle(EditorStyles.label) {
            font = OpenSansBold,
            fontSize = 15,
        }*/;

        public static GUIStyle label => GetStyleByName(LabelStyleName)/* = new GUIStyle(EditorStyles.label) {
            font = OpenSansSemibold,
            fontSize = 13,
        }*/;

        public static GUIStyle listLabel => GetStyleByName(ListLabelStyleName)/* = new GUIStyle(EditorStyles.label) {
            font = OpenSansSemibold,
            fontSize = 12,
        }*/;

        public static GUIStyle selectionGrid => GetStyleByName(SelectionGridStyleName)/* = new GUIStyle(GUI.skin.button) {
            margin = new RectOffset(0, 0, 0, 0),
            imagePosition = ImagePosition.ImageAbove,
            clipping = TextClipping.Overflow,
            font = OpenSansBold,
            fontSize = 13,
        }*/;

        public static GUIStyle levelEditorToolbar => GetStyleByName(LevelEditorToolbarStyleName)/* = new GUIStyle(GUI.skin.button) {
            fontSize = 14,
            font = OpenSansSemibold,
        }*/;

        public static GUIStyle helpBox => GetStyleByName(HelpBoxStyleName)/* = new GUIStyle(GUI.skin.box) {
            richText = true,
            alignment = TextAnchor.MiddleLeft,
            imagePosition = ImagePosition.ImageAbove,
            font = OpenSansSemibold,
            fontSize = 12,
        }*/;

        public static GUIStyle footerButton => GetStyleByName(FooterButtonStyleName)/* = new GUIStyle(GUI.skin.button) {
            font = OpenSansBold,
            fontSize = 13,
            clipping = TextClipping.Overflow,
        }*/;

        public static GUIStyle centeredTextField => GetStyleByName(CenteredTextFieldStyleName)/* = new GUIStyle(EditorStyles.textField) {
            alignment = TextAnchor.MiddleCenter,
            fontSize = 13,
            font = OpenSansSemibold,
        }*/;

        public static GUIStyle centeredIntField => GetStyleByName(CenteredIntFieldStyleName)/* = new GUIStyle(EditorStyles.numberField) {
            alignment = TextAnchor.MiddleCenter,
            fontSize = 13,
            font = OpenSansSemibold,
        }*/;

        public static GUIStyle blockOffset => GetStyleByName(BlockOffsetStyleName)/* = new GUIStyle (EditorStyles.inspectorDefaultMargins) {
            margin = new RectOffset(0, 40, 40, 40),
        }*/;

        public static GUIStyle buildModeOffset => GetStyleByName(BuildModeOffsetStyleName)/* = new GUIStyle(EditorStyles.inspectorDefaultMargins) {
            margin = new RectOffset(0, 40, 15, 15),
        }*/;

        public static GUIStyle popup => GetStyleByName(PopupStyleName)/* = new GUIStyle(EditorStyles.popup) {
            fixedHeight = EditorUtilites.CalculateSize(26f),
            fontSize = 12,
            font = OpenSansSemibold,
        }*/;

        public static GUIStyle leftTab => GetStyleByName(LeftTabStyleName)/* = new GUIStyle {
            normal = { background = EditorUtilites.GetTexture("#fcfcfc") }
        }*/;

        public static GUIStyle header => GetStyleByName(HeaderStyleName)/* = new GUIStyle {
            normal = { background = EditorUtilites.GetTexture("#f5f5f5") }
        }*/;

        public static GUIStyle footer => GetStyleByName(FooterStyleName)/* = new GUIStyle {
            normal = { background = EditorUtilites.GetTexture("#ebebeb") }
        }*/;

        public static GUIStyle body => GetStyleByName(BodyStyleName)/* = new GUIStyle {
            normal = { background = EditorUtilites.GetTexture("#fafafa") }
        }*/;

        public static GUIStyle GetStyleByName(string styleName)
        {
            return MMKSkin.FindStyle(styleName) ?? EditorStyles.label;
        }

        private const string SkinName = "MMK_GUI.guiskin";

        private const string BoldLabelStyleName = "boldLabel";
        private const string ArticleLableStyleName = "articleLabel";
        private const string LabelStyleName = "label";
        private const string ListLabelStyleName = "listLabel";
        private const string SelectionGridStyleName = "selectionGrid";
        private const string LevelEditorToolbarStyleName = "levelEditorToolbar";
        private const string HelpBoxStyleName = "helpBox";
        private const string FooterButtonStyleName = "footerButton";
        private const string CenteredTextFieldStyleName = "centeredTextField";
        private const string CenteredIntFieldStyleName = "centeredIntField";
        private const string BlockOffsetStyleName = "blockOffset";
        private const string BuildModeOffsetStyleName = "buildModeOffset";
        private const string PopupStyleName = "popup";
        private const string LeftTabStyleName = "leftTab";
        private const string HeaderStyleName = "header";
        private const string FooterStyleName = "footer";
        private const string BodyStyleName = "body";

        static MMKStyles()
        {
            const string path = Constants.MMKCoreRoot + SkinName;
            MMKSkin = AssetDatabase.LoadAssetAtPath<GUISkin>(path);
        }
    }
}