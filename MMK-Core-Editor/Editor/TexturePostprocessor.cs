﻿using UnityEditor;

namespace MMK.Core.Editor
{
    public class TexturePostprocessor : AssetPostprocessor
    {
        private void OnPreprocessTexture ()
        {
            var textureImporter = (TextureImporter) assetImporter;
            textureImporter.isReadable = true;
        }
    }
}
