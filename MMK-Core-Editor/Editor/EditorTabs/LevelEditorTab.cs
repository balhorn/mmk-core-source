﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MMK.Core.Editor
{
    public class LevelEditorTab : EditorTab
    {
        public bool LevelChanged { get; set; }
        public GUIContent HelpMessage { get; set; }

        private PremadeLevel currentLevel;
        private List<PremadeLevel> levels;

        protected GUIContent[] levelNames;
        protected string currentLevelNumber;
        protected int previousLevelNumber;

        protected const string PlayerPrefsKey = EditorUtilites.PlayerPrefsKey;

        private int LastEditedLevel => PlayerPrefs.HasKey(PlayerPrefsKey) ? PlayerPrefs.GetInt(PlayerPrefsKey) : 1;

        public LevelEditorTab ()
        {
            ActiveButtonContent = new GUIContent("LE");
            InactiveButtonContent = ActiveButtonContent;

            InitLE();
        }

        #region Overrides

        protected override void DrawHeader()
        {
            DrawLevelSwitcher();
        }

        protected override void DrawFooter()
        {
            DrawButtons();
        }

        #endregion

        #region Accessors

        public List<T> Levels<T> () where T : PremadeLevel
        {
            return levels as List<T>;
        }

        public void SetLevels<T>(List<T> newLevels) where T : PremadeLevel
        {
            levels = newLevels.Cast<PremadeLevel>().ToList();
        }

        public T GetLevel<T> (int number) where T : PremadeLevel
        {
            return Levels<T>()[number - 1];
        }

        public T CurrentLevel<T> () where T : PremadeLevel
        {
            var result = CurrentLevelProp as T;
            return result;
        }

        public void CurrentLevel<T> (T newLevel) where T : PremadeLevel
        {
            CurrentLevelProp = newLevel;
        }

        protected virtual PremadeLevel CreateNewLevel ()
        {
            return ScriptableObject.CreateInstance<PremadeLevel>();
        }

        #endregion

        #region Drawing

        private void DrawLevelSwitcher ()
        {
            GUILayout.Space(EditorUtilites.CalculateSize(32f));

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Level Quick Choosing", MMKStyles.boldLabel);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("first", EditorUtilites.Height(26), EditorUtilites.Width(42f))) {
                LoadPremadeLevel(1);
            }

            GUILayout.Space(EditorUtilites.CalculateSize(30f));

            if (GUILayout.Button("←", EditorUtilites.Height(26), EditorUtilites.Width(42f))) {
                LoadPremadeLevel(CurrentLevel<PremadeLevel>().number - 1);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            GUILayout.Space(EditorUtilites.CalculateSize(15f));

            EditorGUI.BeginChangeCheck();
            currentLevelNumber = EditorGUILayout.TextField(currentLevelNumber, MMKStyles.centeredTextField, EditorUtilites.Height(32),
                EditorUtilites.Width(50f));

            if (EditorGUI.EndChangeCheck()) {
                try {
                    var result = int.Parse(currentLevelNumber);
                    LoadPremadeLevel(result);
                } catch (FormatException) {
                    Debug.LogError("You've typed some kind of bullshit. Please, enter valid number");
                }
            }

            GUILayout.Space(EditorUtilites.CalculateSize(15f));

            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("→", EditorUtilites.Height(26), EditorUtilites.Width(42f))) {
                LoadPremadeLevel(CurrentLevel<PremadeLevel>().number + 1);
            }

            GUILayout.Space(EditorUtilites.CalculateSize(30f));

            if (GUILayout.Button("last", EditorUtilites.Height(26), EditorUtilites.Width(42f))) {
                LoadPremadeLevel(Levels<PremadeLevel>().Count);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.Space(EditorUtilites.CalculateSize(32f));
        }

        private void DrawButtons ()
        {
            EditorGUILayout.BeginVertical(MMKStyles.footer, EditorUtilites.Height(78f));
            {
                GUILayout.FlexibleSpace();
                EditorGUILayout.BeginHorizontal();

                GUILayout.Space(EditorUtilites.CalculateSize(16f));

                var defaultColor = GUI.color;
                GUI.color = EditorUtilites.HexToColor("#ffbdbd");
                if (GUILayout.Button("Delete level", MMKStyles.footerButton, EditorUtilites.Width(128f), EditorUtilites.Height(48f)) &&
                    EditorUtility.DisplayDialog("Confirm delete", "Do you really want to delete this level?", "Yes", "No")) {
                    DeleteLevel();
                }
                GUI.color = defaultColor;

                GUILayout.FlexibleSpace();

                if (GUILayout.Button("Test level", MMKStyles.footerButton, EditorUtilites.Width(128f), EditorUtilites.Height(48f))) {
                    SaveLevel();
                    TestLevel();
                }

                GUILayout.Space(EditorUtilites.CalculateSize(16f));

                //GUI.enabled = levelChanged;
                GUI.color = EditorUtilites.HexToColor("#e7fabf");
                if (GUILayout.Button("Save level", MMKStyles.footerButton, EditorUtilites.Width(128f), EditorUtilites.Height(48f))) {
                    SaveLevel();
                }
                GUI.color = defaultColor;
                //GUI.enabled = true;

                GUILayout.Space(EditorUtilites.CalculateSize(16f));

                EditorGUILayout.EndHorizontal();
                GUILayout.FlexibleSpace();
            }
            EditorGUILayout.EndVertical();
        }

        #endregion

        #region Initialization

        protected virtual string LevelsFolder => Constants.LevelsFolder;

        private PremadeLevel CurrentLevelProp
        {
            get { return currentLevel; }
            set
            {
                currentLevel = value;
                Debug.Log($"Level set to {value}");
            }
        }

        // ReSharper disable once InconsistentNaming
        private void InitLE ()
        {
            LoadLevels();
            LoadPremadeLevel(LastEditedLevel);
        }

        private void LoadLevels ()
        {
            levels = new List<PremadeLevel>();
            var index = Application.dataPath.LastIndexOf("/", StringComparison.Ordinal) + 1;
            var path = Application.dataPath.Remove(index) + LevelsFolder;

            foreach (var level in Directory.GetFiles(path, "*.asset")
                .Select(fileName => fileName.Substring(fileName.IndexOf("Assets", StringComparison.Ordinal)))
                .Select(fileName => AssetDatabase.LoadAssetAtPath<PremadeLevel>(fileName))) {
                levels.Add(level);
            }

            levels.Sort((lvl1, lvl2) => lvl1.number.CompareTo(lvl2.number));
        }

        protected virtual void LoadPremadeLevel (int level)
        {
            levels.RemoveAll(lvl => lvl == null);

            level = Mathf.Clamp(level, 1, levels.Count + 1);
            PlayerPrefs.SetInt(EditorUtilites.PlayerPrefsKey, level);
            PlayerPrefs.Save();

            if (CurrentLevelProp == null || CurrentLevelProp.number != level) {
                try {
                    CurrentLevelProp = GetLevel<PremadeLevel>(level);
                } catch (ArgumentOutOfRangeException) {
                    CurrentLevelProp = CreateNewLevel();
                    CurrentLevelProp.number = level;

                    levels.Add(CurrentLevelProp);
                }
            }

            currentLevelNumber = CurrentLevelProp.number.ToString();
        }

        #endregion

        #region Utility

        protected virtual void SaveLevel ()
        {
            EditorUtility.SetDirty(CurrentLevelProp);

            if (!EditorUtility.IsPersistent(CurrentLevelProp)) {
                var levelNumber = CurrentLevelProp.number;
                AssetDatabase.CreateAsset(CurrentLevelProp,
                    $"{LevelsFolder}/{string.Format(Constants.LevelNameFormat, levelNumber)}.asset");
                RefreshLevelNames();
            }

            ChangeOrder();

            AssetDatabase.SaveAssets();

            LevelChanged = false;
        }

        private void TestLevel ()
        {
            if (SceneManager.GetActiveScene().name != Constants.MainGameScene) {
                EditorSceneManager.OpenScene($"{Constants.MMKRoot}Scenes/{Constants.MainGameScene}.unity");
            }

            CurrentLevelProp.RunInTestMode(AssetDatabase.LoadAssetAtPath<GameObject>($"{Constants.MMKRoot}Prefabs/Map/Level.prefab"));

            EditorApplication.isPlaying = true;
        }

        private void DeleteLevel ()
        {
            var number = CurrentLevelProp.number;
            for (int index = number; index < levels.Count; index++) {
                levels[index].number--;
                EditorUtility.SetDirty(levels[index]);
            }

            levels.RemoveAt(number - 1);
            AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(CurrentLevelProp));

            RefreshLevelNames();
            LoadPremadeLevel(number);
        }

        public void ChangeOrder ()
        {
            var currentNumber = CurrentLevelProp.number - 1;
            previousLevelNumber = Mathf.Clamp(previousLevelNumber, 0, levels.Count - 1);
            if (previousLevelNumber == currentNumber || previousLevelNumber == currentNumber + 1) {
                return;
            }

            var offset      = (int) Mathf.Sign(currentNumber - previousLevelNumber);
            var startNumber = Mathf.Min(previousLevelNumber, currentNumber);
            var endNumber   = Mathf.Max(previousLevelNumber, currentNumber);

            for (int number = startNumber; number <= endNumber; ++number) {
                var level = levels[number];
                level.number += offset;
                level.name = $"Level_{level.number}";
                EditorUtility.SetDirty(level);
            }

            levels.Remove(CurrentLevelProp);
            levels.Insert(previousLevelNumber, CurrentLevelProp);

            CurrentLevelProp.number = previousLevelNumber + 1;
            CurrentLevelProp.name = $"Level_{previousLevelNumber + 1}";
            EditorUtility.SetDirty(CurrentLevelProp);

            currentLevelNumber = CurrentLevelProp.number.ToString();

            RefreshLevelNames();
        }

        public void RefreshLevelNames ()
        {
            levelNames = new[] { "Level_0" }.Concat(levels
                        .Where(lvl => lvl != null)
                        .Select(lvl => lvl.name))
                        .Select(name => new GUIContent(name))
                        .ToArray();
        }

        #endregion
    }
}