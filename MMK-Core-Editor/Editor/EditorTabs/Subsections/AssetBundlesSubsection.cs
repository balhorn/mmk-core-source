﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace MMK.Core.Editor
{
    public abstract class AssetBundlesSubsection<T> : EditorSubsection where T : PremadeLevel
    {
        protected GameSettings settings;

        protected LevelEditorTab LE => ParentTab as LevelEditorTab;

        private const string TempBundlesFolder = "Assets/Temp";
        private const string BundlesUriPrefsKey = "bundlesUri";

        protected AssetBundlesSubsection()
        {
            TabContent.text = "Asset bundles";
            settings = Resources.Load<GameSettings>("GameSettings");
            //ParentTab = MMKWindow.FindTab<LevelEditorTab>();
        }

        public override void Draw()
        {
            if (LE != null) {
                DrawSectionBody();
            } else {
                DrawWarning();
            }
        }

        private void DrawSectionBody()
        {
            EditorUtilites.BeginOffsetBlock();
            EditorUtilites.BoldTitle("Asset bundle management");
            EditorUtilites.Space(17f);
            EditorGUI.BeginChangeCheck();
            AssetBundleManager.assetBundlesUri = EditorUtilites.TextField("Uri", AssetBundleManager.assetBundlesUri);
            if (EditorGUI.EndChangeCheck()) {
                EditorPrefs.SetString(BundlesUriPrefsKey, AssetBundleManager.assetBundlesUri);
            }

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            try {
                if (EditorUtilites.ButtonFitContent("Download", MMKStyles.footerButton, EditorUtilites.Width(128f),
                    EditorUtilites.Height(48f))) {
                    LoadBundles();
                }

                if (EditorUtilites.ButtonFitContent("Upload", MMKStyles.footerButton, EditorUtilites.Width(128f),
                    EditorUtilites.Height(48f))) {
                    UpdateBundles();
                }
            } catch (UriFormatException) {
                EditorUtility.DisplayDialog("Invalid uri format", "Current uri string formatted poorly", "Ok");
            } catch (NotSupportedException) {
                EditorUtility.DisplayDialog("Not supported", "Current uri format is not supported. Please, provide file:// uri",
                    "Ok");
            }

            EditorGUILayout.EndHorizontal();
            GUILayout.FlexibleSpace();
            EditorUtilites.EndOffsetBlock();
        }

        private void DrawWarning()
        {
            EditorGUILayout.HelpBox(
                "This section is only available withing Level Editor tab!",
                MessageType.Warning,
                true);
        }

        protected void UpdateBundles ()
        {
            try {
                var parts = TempBundlesFolder.Split('/');
                AssetDatabase.CreateFolder(parts[0], parts[1]);

                foreach (var level in LE.Levels<T>()) {
                    //Convert level to json
                    var fileName = $"{TempBundlesFolder}/{level.name}.json";
                    var json = JsonUtility.ToJson(level);
                    File.WriteAllText($"{Application.dataPath}/{parts[1]}/{level.name}.json", json);
                    AssetDatabase.Refresh();

                    //Assign json file to bundle
                    var importer = AssetImporter.GetAtPath(fileName);
                    var bundleNumber = level.number / settings.levelsPerLocation + 1;
                    importer.assetBundleName = string.Format(Constants.LevelAssetBundleNameFormat, bundleNumber);
                    importer.SaveAndReimport();
                }

                var buildTargets = new[] {BuildTarget.WebGL, BuildTarget.Android, BuildTarget.iOS};
                foreach (var currentBuildTarget in buildTargets) {
                    var platformName = GetPlatformName(currentBuildTarget);
                    AssetDatabase.CreateFolder(TempBundlesFolder, platformName);

                    AssetDatabase.RemoveUnusedAssetBundleNames();
                    BuildPipeline.BuildAssetBundles(
                        Path.Combine(TempBundlesFolder, platformName),
                        BuildAssetBundleOptions.ChunkBasedCompression,
                        currentBuildTarget);

                    Debug.Log($"Uploading with build target {(int) currentBuildTarget}");
                    Debug.Log("Platform name: " + platformName);
                    AssetBundleManager.UpdateAssetBundles(
                        GetBundlesPath(platformName),
                        settings.levelsPerLocation,
                        platformName);
                }
            } catch (Exception e) {
                Debug.Log(e);
                EditorUtility.DisplayDialog("Failed to update bundles", e.Message, "Ok");
            } finally {
                FileUtil.DeleteFileOrDirectory(TempBundlesFolder);
            }
        }

        private string GetBundlesPath(string platformName)
        {
            return Path.Combine(Path.Combine(TempBundlesFolder, platformName), Constants.AssetBundleFolder);
        }

        private string GetPlatformName (BuildTarget buildTarget)
        {
            switch (buildTarget) {
                case BuildTarget.WebGL:
                    return "fb";
                case BuildTarget.Android:
                    return "android";
                case BuildTarget.iOS:
                    return "ios";
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void LoadBundles ()
        {
            try {
                var platform = GetPlatformName(EditorUserBuildSettings.activeBuildTarget);
                var levels = AssetBundleManager.LoadLevelsFromAssetBundles<T>(settings.levelsPerLocation, platform);

                //Delete current levels
                foreach (var level in LE.Levels<T>()) {
                    var path = AssetDatabase.GetAssetPath(level);
                    AssetDatabase.DeleteAsset(path);
                }

                //Create new from bundles
                for (int index = 0; index < levels.Count; index++) {
                    var level = levels[index];
                    //var levelCopy = Instantiate(level); //need to make a copy to avoid error while creating new asset
                    var path = new StringBuilder(Constants.LevelsFolder)
                        .Append("/")
                        .AppendFormat(Constants.LevelNameFormat, new object[] {level.number})
                        .Append(".asset")
                        .ToString();

                    AssetDatabase.CreateAsset(level, path);

                    levels[index] = level;
                }

                LE.SetLevels(levels);
                //LoadPremadeLevel(PlayerPrefs.HasKey(PlayerPrefsKey) ? PlayerPrefs.GetInt(PlayerPrefsKey) : 1);

                AssetDatabase.Refresh();
                AssetDatabase.SaveAssets();
            } catch (ArgumentException) {
                EditorUtility.DisplayDialog("Invalid Uri", "Unable to load bundles from the provided Uri", "Ok");
            }
        }
    }
}