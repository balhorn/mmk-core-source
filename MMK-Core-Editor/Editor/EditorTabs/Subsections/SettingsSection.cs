﻿using UnityEngine;

namespace MMK.Core.Editor
{
    public class SettingsSection<T> : EditorSubsection where T : ScriptableObject
    {
        protected ScriptableObject settings;
        private UnityEditor.Editor settingsEditor;

        public override void Draw()
        {
            settingsEditor.OnInspectorGUI();
        }

        public SettingsSection(string settingsAssetName)
        {
            TabContent = new GUIContent(settingsAssetName);
            settings = Resources.Load<T>(settingsAssetName);
            settingsEditor = UnityEditor.Editor.CreateEditor(settings);
        }
    }
}