﻿using UnityEngine;

namespace MMK.Core.Editor
{
    public abstract class EditorSubsection
    {
        public GUIContent TabContent { get; protected set; }
        public EditorTab ParentTab { get; set; }

        public abstract void Draw();

        protected EditorSubsection()
        {
            TabContent = new GUIContent("New subsection");
        }
    }
}