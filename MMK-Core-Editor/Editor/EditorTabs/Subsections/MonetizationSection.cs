﻿using UnityEngine;

namespace MMK.Core.Editor
{
    public class MonetizationSection : EditorSubsection
    {
        private ShopSettingsEditor editor;

        public override void Draw()
        {
            editor.OnInspectorGUI();
        }

        public MonetizationSection()
        {
            TabContent = new GUIContent("Monetization");
            var settings = Resources.Load<GameSettings>("GameSettings");
            editor = UnityEditor.Editor.CreateEditor(settings.Monetization) as ShopSettingsEditor;
        }
    }
}