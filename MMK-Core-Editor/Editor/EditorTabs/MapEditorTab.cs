﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace MMK.Core.Editor
{
    public class MapEditorTab : EditorTab
    {
        private MapController mapController;
        private LevelController levelController;

        private Level        currentLevel;
        private List<Level>  levels;

        private MapBoundsEditor mapBoundsEditor;
        private LevelEditor        levelEditor;

        private List<SpriteRenderer> locationRenderers;
        private List<GameObject> locations;

        private int navigatedLocation;
        private int toolbarIndex;
        private int currentLevelIndex;
        private int previousLevelIndex;
        private int nextLevelIndex;
        private int undoGroupIndex;

        private bool mapChanged;

        private string currentLevelNumber;

        private Sprite endOfRoadSprite;
        private Sprite fogSprite;

        private GameObject locationsParent;

        private GameSettings settings;
        private GameSettings Settings => settings ?? (settings = Resources.Load<GameSettings>("GameSettings"));

        private string[] levelNames;
        private string[] locationNames;
        private readonly string[] toolbarOptions = {"Editor", "Setup"};

        private const string CurrentLevelKey = "current_level";
        private readonly string PathToPrefab = $"{Constants.MMKRoot}Prefabs/Map/Level.prefab";
        private const string LocationsTag = "Locations";
        private const string UndoKey = "Map editor changes";

        private Level CurrentLevel
        {
            get { return currentLevel; }
            set
            {
                currentLevel = value;
                currentLevelNumber = currentLevel?.number.ToString() ?? "";
                levelEditor = UnityEditor.Editor.CreateEditor(currentLevel) as LevelEditor;
                FocusSceneView(currentLevelIndex);
            }
        }

        public override void OnTabOpened()
        {
            base.OnTabOpened();

            try {
                if (SceneManager.GetActiveScene().name != Constants.MapScene) {
                    EditorSceneManager.OpenScene($"{Constants.MMKRoot}Scenes/{Constants.MapScene}.unity");
                }

                InitMapEditor();
                Subscribe();
            } catch (Exception) {
            }
        }

        public override void OnTabClosed()
        {
            base.OnTabClosed();

            Unsubscribe();
        }

        public MapEditorTab()
        {
            InitContents();
        }

        protected override void DrawHeader()
        {
            DrawNavigation();
        }

        protected override void DrawFooter()
        {
            DrawFooterButtons();
        }

        protected override void DrawBody()
        {
            DrawEditor();
            DrawSetup();
        }

        private void DrawNavigation()
        {
            GUILayout.Space(EditorUtilites.CalculateSize(30f));

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Level Quick Choosing", EditorStyles.boldLabel);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(EditorUtilites.Height(42));
            GUILayout.FlexibleSpace();

            EditorGUILayout.BeginVertical();
            GUILayout.FlexibleSpace();
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("first", EditorUtilites.Height(26), EditorUtilites.Width(42f))) {
                CurrentLevel = levels[levels.Count - 1];
            }

            if (GUILayout.Button("←", EditorUtilites.Height(26), EditorUtilites.Width(42f))) {
                CurrentLevel = levels[Mathf.Clamp(++currentLevelIndex, 0, levels.Count - 1)];
            }
            EditorGUILayout.EndHorizontal();
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndVertical();

            EditorGUI.BeginChangeCheck();
            currentLevelNumber = EditorGUILayout.TextField(currentLevelNumber, MMKStyles.centeredTextField, EditorUtilites.Height(32),
                EditorUtilites.Width(50f));

            if (EditorGUI.EndChangeCheck()) {
                try {
                    var result = Mathf.Clamp(int.Parse(currentLevelNumber), 1, levels.Count);
                    CurrentLevel = levels[levels.Count - result];
                } catch (FormatException) {
                    Debug.LogError("You've typed some kind of bullshit. Please, enter valid number");
                }
            }

            EditorGUILayout.BeginVertical();
            GUILayout.FlexibleSpace();
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("→", EditorUtilites.Height(28), EditorUtilites.Width(42f))) {
                CurrentLevel = levels[Mathf.Clamp(--currentLevelIndex, 0, levels.Count - 1)];
            }

            if (GUILayout.Button("last", EditorUtilites.Height(28), EditorUtilites.Width(42f))) {
                CurrentLevel = levels[0];
            }
            EditorGUILayout.EndHorizontal();
            GUILayout.FlexibleSpace();
            EditorGUILayout.EndVertical();

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.Space(EditorUtilites.CalculateSize(30f));
        }

        private void DrawEditor()
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Location", MMKStyles.boldLabel, EditorUtilites.Width(75f));

            GUILayout.Space(18f);

            if (GUILayout.Button("←", EditorUtilites.Height(28f), EditorUtilites.Width(42f))) {
                FocusSceneOnLocation(++navigatedLocation);
            }

            GUILayout.Space(25f);

            EditorGUI.BeginChangeCheck();
            navigatedLocation = EditorGUILayout.Popup(navigatedLocation, locationNames, MMKStyles.popup, EditorUtilites.Width(200f));
            if (EditorGUI.EndChangeCheck()) {
                FocusSceneOnLocation(navigatedLocation);
            }

            GUILayout.Space(25f);

            if (GUILayout.Button("→", EditorUtilites.Height(28f), EditorUtilites.Width(42f))) {
                FocusSceneOnLocation(--navigatedLocation);
            }
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(EditorUtilites.CalculateSize(12f));

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Delete location", EditorUtilites.Width(150f), EditorUtilites.Height(38f))) {
                DeleteLocation();
            }

            GUILayout.FlexibleSpace();

            if (GUILayout.Button("Create new location", EditorUtilites.Width(182f), EditorUtilites.Height(38f))) {
                InvokeLocationWindow();
            }


            EditorGUILayout.EndHorizontal();

            GUILayout.Space(EditorUtilites.CalculateSize(42f));

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Level", MMKStyles.boldLabel, EditorUtilites.Width(75f));

            GUILayout.Space(18f);

            if (GUILayout.Button("←", EditorUtilites.Height(28f), EditorUtilites.Width(42f))) {
                CurrentLevel = levels[Mathf.Clamp(++currentLevelIndex, 0, levels.Count - 1)];
            }

            GUILayout.Space(25f);

            EditorGUI.BeginChangeCheck();
            currentLevelIndex = EditorGUILayout.Popup(currentLevelIndex, levelNames, MMKStyles.popup,
                EditorUtilites.Width(200f));
            if (EditorGUI.EndChangeCheck()) {
                CurrentLevel = levels[currentLevelIndex];
            }

            GUILayout.Space(25f);

            if (GUILayout.Button("→", EditorUtilites.Height(28f), EditorUtilites.Width(42f))) {
                CurrentLevel = levels[Mathf.Clamp(--currentLevelIndex, 0, levels.Count - 1)];
            }

            GUILayout.Space(18f);

            EditorGUILayout.EndHorizontal();

            GUILayout.Space(EditorUtilites.CalculateSize(12f));

            EditorGUILayout.BeginHorizontal();
            {
                if (GUILayout.Button("Delete level", EditorUtilites.Width(128f), EditorUtilites.Height(38f))) {
                    DeleteLevel();
                }

                GUILayout.FlexibleSpace();

                //if (GUILayout.Button(editButtonContent, EditorUtilites.Width(108f), EditorUtilites.Height(38f))) {
                //    PlayerPrefs.SetInt(EditorUtilites.PlayerPrefsKey, levels.Count - currentLevelIndex);
                //    PlayerPrefs.Save();

                //    MMKWindow.OpenLevelEditor();
                //}

                if (GUILayout.Button("Create new level", EditorUtilites.Width(164f), EditorUtilites.Height(38f))) {
                    LoadNewLevel();
                }

            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }

        private void DrawSetup()
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(levelsPerLocationContent);
            Settings.levelsPerLocation = EditorGUILayout.IntField(Settings.levelsPerLocation, MMKStyles.centeredTextField, EditorUtilites.Height(28f),
                EditorUtilites.Width(42f));
            EditorGUILayout.EndHorizontal();

            mapController.movingSpeed = EditorGUILayout.Slider("Swipe speed", mapController.movingSpeed, 0f, 10f);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            GUILayout.Label("Fog", MMKStyles.boldLabel);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(fogCoveredAreaContent, MMKStyles.label);
            Settings.fogOffset = EditorGUILayout.IntField(Settings.fogOffset, MMKStyles.centeredIntField,
                EditorUtilites.Height(28f), EditorUtilites.Width(42f));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(fogSpriteContent, MMKStyles.label);
            fogSprite = EditorGUILayout.ObjectField(fogSprite, typeof(Sprite), false) as Sprite;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(endOfRoadContent, MMKStyles.label);
            endOfRoadSprite =
                EditorGUILayout.ObjectField(endOfRoadSprite, typeof(Sprite), false) as Sprite;
            EditorGUILayout.EndHorizontal();


            EditorGUILayout.Space();

            GUILayout.Label("Map bounds", MMKStyles.boldLabel);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Top", MMKStyles.label);
            mapController.pixelsTopOffset = EditorGUILayout.FloatField(mapController.pixelsTopOffset,
                MMKStyles.centeredIntField, EditorUtilites.Height(28f), EditorUtilites.Width(42f));
            GUILayout.Label("px", MMKStyles.label);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Bottom", MMKStyles.label);
            mapController.pixelsBottomOffset = EditorGUILayout.FloatField(mapController.pixelsBottomOffset,
                MMKStyles.centeredIntField, EditorUtilites.Height(28f), EditorUtilites.Width(42f));
            GUILayout.Label("px", MMKStyles.label);
            EditorGUILayout.EndHorizontal();
        }

        private void DrawFooterButtons()
        {
            GUILayout.FlexibleSpace();
            var defaultColor = GUI.color;
            GUI.color = EditorUtilites.HexToColor("#ffbdbd");
            if (GUILayout.Button("Reset", MMKStyles.footerButton, EditorUtilites.Width(128f), EditorUtilites.Height(48f))) {
                Undo.RevertAllDownToGroup(undoGroupIndex);
            }

            GUI.color = EditorUtilites.HexToColor("#e7fabf");
            GUI.enabled = mapChanged;
            if (GUILayout.Button("Save changes", MMKStyles.footerButton, EditorUtilites.Width(128f), EditorUtilites.Height(48f))) {
                Save();
            }
            GUI.color = defaultColor;
            GUI.enabled = true;
        }

        private void RearrangeHierarchy ()
        {
            foreach (var lvl in levels) {
                lvl.gameObject.name = $"Level {lvl.number}";
                lvl.transform.SetSiblingIndex(lvl.number - 1);
            }
        }

        private void FocusSceneView (int index)
        {
            if (levels.Count == 0) {
                SceneView.lastActiveSceneView?.LookAt(Vector3.zero);
                return;
            }

            index = Mathf.Clamp(index, 0, levels.Count - 1);

            Selection.activeGameObject = levels[index].gameObject;
            if (SceneView.lastActiveSceneView != null) {
                SceneView.lastActiveSceneView.AlignViewToObject(levels[index].transform);
            }
        }

        private void Save ()
        {
            mapBoundsEditor.AdjustBounds(locationRenderers);

            EditorUtility.SetDirty(Settings);
            EditorUtility.SetDirty(mapController);

            EditorSceneManager.SaveOpenScenes();

            Undo.RecordObjects(new UnityEngine.Object[] { Settings, mapController }, UndoKey);
            undoGroupIndex = Undo.GetCurrentGroup();

            mapChanged = false;
        }

        private void FocusSceneOnLocation (int index)
        {
            index = Mathf.Clamp(index, 0, locations.Count - 1);

            float targetPositionX, targetPositionY;
            if (locationRenderers.Count == 0) {
                targetPositionY = targetPositionX = 0;
            } else {
                var transform = locations[index].transform;
                var renderer = transform.GetChild(transform.childCount - 1).GetComponent<SpriteRenderer>();
                var camera = SceneView.lastActiveSceneView.camera;

                targetPositionY = renderer.bounds.center.y - renderer.bounds.extents.y + camera.orthographicSize;
                targetPositionX = renderer.bounds.center.x;
            }

            SceneView.lastActiveSceneView?.LookAt(new Vector2(targetPositionX, targetPositionY));
        }

        private void InvokeLocationWindow ()
        {
            var window = EditorWindow.GetWindow<AddLocationWindow>();
            window.MapEditor = this;
        }

        public void AddAnotherLocation (Sprite sprite)
        {
            if (sprite == null) {
                return;
            }

            var instance = new GameObject(sprite.name);
            var renderer = instance.AddComponent<SpriteRenderer>();
            renderer.sprite = sprite;

            float xPos, yPos;
            if (locationRenderers.Count != 0) {
                var lastLocationRenderer = locationRenderers[0];
                xPos = lastLocationRenderer.bounds.center.x;
                yPos = lastLocationRenderer.bounds.max.y + renderer.bounds.extents.y;
            } else {
                xPos = yPos = 0f;
            }

            locationRenderers.Insert(0, renderer);
            navigatedLocation = 0;
            RefreshLocationNames();

            var position = new Vector2(xPos, yPos);
            instance.transform.position = position;
            instance.transform.parent = locationsParent.transform;
            instance.transform.SetAsLastSibling();

            mapBoundsEditor.AdjustBounds(locationRenderers);

            SceneView.lastActiveSceneView?.LookAt(position);
            Undo.RegisterCreatedObjectUndo(instance, UndoKey);
        }

        public void AddAnotherLocation (GameObject prefab)
        {
            prefab.name = $"Location {locations.Count + 1}";
            locations.Insert(0, prefab);

            var transform = prefab.transform;
            transform.SetParent(locationsParent.transform);
            transform.SetAsFirstSibling();

            float centerX, centerY;
            if (locationRenderers.Count != 0) {
                var lastRenderer = locationRenderers[0];

                centerX = lastRenderer.bounds.center.x;
                centerY = lastRenderer.bounds.max.y;
            } else {
                centerX = centerY = 0f;
            }

            var position = new Vector2(centerX, centerY);

            SnapLocation(transform, position);
            SceneView.lastActiveSceneView?.LookAt(position);

            navigatedLocation = 0;
            AddRenderers(prefab);
            SortRenderers(prefab);
            RefreshLocationNames();
            mapBoundsEditor.AdjustBounds(locationRenderers);

            Undo.RegisterCreatedObjectUndo(prefab, UndoKey);
        }

        private void SnapLocation(Transform locationTransform, Vector2 pivot)
        {
            locationTransform.position = pivot;

            var renderers = locationTransform.GetComponentsInChildren<SpriteRenderer>()
                .OrderBy(rend => rend.bounds.center.y);
            float totalHeight = 0f;
            foreach (var spriteRenderer in renderers) {
                var halfHeight = spriteRenderer.bounds.extents.y;
                spriteRenderer.gameObject.transform.localPosition = new Vector2(0f, totalHeight + halfHeight);
                totalHeight += 2 * halfHeight;
            }
        }

        private void DisplayLocationInfo(GameObject prefab)
        {
            var topPoint = locationRenderers[0].bounds.max.y;

            Debug.Log("Current top point: " + topPoint);
            Debug.Log("New object position: " + prefab.transform.position.y);

            var renderers = prefab.GetComponentsInChildren<SpriteRenderer>()
                .OrderBy(rend => rend.bounds.center.y);
            foreach (var spriteRenderer in renderers) {
                Debug.LogFormat("Sprite: {0}. Min point: {1}, max point {2}",
                    spriteRenderer.gameObject.name,
                    spriteRenderer.bounds.min.y,
                    spriteRenderer.bounds.max.y);
            }

            Debug.Log("Total height: " + renderers.Sum(rend => rend.bounds.size.y));
        }

        private void LoadNewLevel ()
        {
            var position = levels.Count != 0 ? levels[0].transform.position : Vector3.zero;
            position.y += 1f;

            var asset = AssetDatabase.LoadAssetAtPath<GameObject>(PathToPrefab);
            var levelPrefab =
                UnityEngine.Object.Instantiate(asset, position,
                    Quaternion.identity) as GameObject;
            levelPrefab = PrefabUtility.ConnectGameObjectToPrefab(levelPrefab, asset);
            levelPrefab.transform.position = position;

            var level = levelPrefab.GetComponent<Level>();
            level.number = -1;  //ensure that we don't get already existing level
            var lastLevel = levels.Count != 0 ? levels[0] : null;

            levels.Insert(0, level);
            currentLevelIndex = 0;
            CurrentLevel = level;
            levelEditor.InsertBetween(lastLevel, null);

            RearrangeHierarchy();
            RefreshLevelNames();
        }

        private void DeleteLevel ()
        {
            levels.Remove(CurrentLevel);
            levelEditor?.RemoveLevel();
            Undo.DestroyObjectImmediate(CurrentLevel?.gameObject);
            CurrentLevel = levels.Count != 0 ? levels[Mathf.Clamp(currentLevelIndex, 0, levels.Count - 1)] : null;

            RefreshLevelNames();
            RearrangeHierarchy();
        }

        private void DeleteLocation ()
        {
            if (!EditorUtility.DisplayDialog("Delete location",
                "Are you sure you want to delete location with all its levels?", "Yes, delete", "Don't delete")) {
                return;
            }

            var location = locations[navigatedLocation];
            var bounds = GetBounds(location);
            var levelColliders = Physics2D.BoxCastAll(bounds.center, bounds.size, 0f, Vector2.zero);
            foreach (var level in levelColliders
                .Where(hit => hit.collider != null)
                .Select(hit => hit.collider.gameObject.GetComponent<Level>())
                .Where(level => level != null)) {
                levels.Remove(level);
                levelEditor = UnityEditor.Editor.CreateEditor(level) as LevelEditor;
                levelEditor.RemoveLevel();
                Undo.DestroyObjectImmediate(level.gameObject);
            }

            locations.RemoveAt(navigatedLocation);
            UnityEngine.Object.DestroyImmediate(location);

            levels.RemoveAll(lvl => lvl == null);
            locationRenderers.RemoveAll(rend => rend == null);

            navigatedLocation = Mathf.Clamp(navigatedLocation, 0, locationRenderers.Count - 1);
            FocusSceneOnLocation(navigatedLocation);

            RefreshLevelNames();
            RefreshLocationNames();

            mapBoundsEditor.AdjustBounds(locationRenderers);
        }

        private void InsertLevel ()
        {
            if (nextLevelIndex != 0 && previousLevelIndex >= nextLevelIndex) {
                return;
            }

            var previousLevel = previousLevelIndex != 0 ? levels[previousLevelIndex - 1] : null;
            var nexLevel = nextLevelIndex != 0 ? levels[nextLevelIndex - 1] : null;

            levelEditor.InsertBetween(previousLevel, nexLevel);
            RearrangeHierarchy();
        }

        private void RefreshLocationNames ()
        {
            locationNames = locations.Select(location => location.name).ToArray();

            try {
                Draw();
            } catch (NullReferenceException e) {
                Debug.LogWarning(e);
            }
        }

        private void RefreshLevelNames ()
        {
            levelNames = levels.Select(level => level.gameObject.name).ToArray();

            try {
                Draw();
            } catch (NullReferenceException e) {
                Debug.LogWarning(e);
            }
        }

        private void SortRenderers (GameObject target)
        {
            var sortedRenderers = target.GetComponentsInChildren<SpriteRenderer>()
                .OrderByDescending(renderer => renderer.bounds.center.y)
                .ToList();

            for (int index = 0; index < sortedRenderers.Count; index++) {
                var renderer = sortedRenderers[index];
                renderer.transform.SetSiblingIndex(index);
            }
        }

        private void AddRenderers (GameObject prefab)
        {
            foreach (var renderer in prefab.GetComponentsInChildren<SpriteRenderer>()
                .OrderBy(rend => rend.bounds.center.y)) {
                locationRenderers.Insert(0, renderer);
            }
        }

        private float GetBoundsHeight (GameObject prefab)
        {
            var renderers = prefab.GetComponentsInChildren<SpriteRenderer>();
            return renderers.Select(rend => rend.bounds).Sum(bounds => bounds.size.y);
        }

        private Bounds GetBounds (GameObject prefab)
        {
            var result = new Bounds();
            var renderers = prefab.GetComponentsInChildren<SpriteRenderer>();

            foreach (var bounds in renderers.Select(rend => rend.bounds)) {
                result.Encapsulate(bounds);
            }

            return result;
        }

        private void SelectionChanged ()
        {
            var selectedLevels = Selection.GetFiltered(typeof (Level), SelectionMode.TopLevel);

            if (selectedLevels.Length == 0) {
                return;
            }

            var level = selectedLevels[0] as Level;
            if (level == null) {
                return;
            }

            CurrentLevel = level;
            currentLevelIndex = levels.IndexOf(level);

            try {
                Draw();
            } catch (NullReferenceException e) {
                Debug.LogWarning(e);
            }
        }

        private void HierarchyWindowChanged ()
        {
            levels = UnityEngine.Object.FindObjectsOfType<Level>()
                .Where(lvl => lvl.number != 0)
                .OrderByDescending(lvl => lvl.number)
                .ToList();

            RefreshLevelNames();

            try {
                Draw();
            } catch (NullReferenceException e) {
                Debug.LogWarning(e);
            }
        }

        private void InitMapEditor()
        {
            mapController = Object.FindObjectOfType<MapController>();
            levelController = Object.FindObjectOfType<LevelController>();

            mapBoundsEditor = UnityEditor.Editor.CreateEditor(mapController) as MapBoundsEditor;

            locationsParent = GameObject.FindGameObjectWithTag(LocationsTag);
            if (locationsParent == null) {
                Debug.LogError("Locations container wasn't found." +
                               $" Please, provide a game object with the tag {LocationsTag} and place" +
                               " all you location sprites under it");
            }
            locations = new List<GameObject>();
            var transform = locationsParent.transform;
            foreach (var child in transform.GetChildren()) {
                locations.Add(child.gameObject);
                SortRenderers(child.gameObject);
            }

            locationRenderers = locationsParent?.GetComponentsInChildren<SpriteRenderer>()
                .OrderByDescending(renderer => renderer.bounds.center.y)
                .ToList();

            levels = Object.FindObjectsOfType<Level>()
                .Where(lvl => lvl.number != 0)
                .OrderByDescending(lvl => lvl.number)
                .ToList();

            RearrangeHierarchy();

            levelNames = levels.Select(level => level.gameObject.name).ToArray();
            locationNames = new string[transform.childCount];

            if (locationRenderers != null && locationRenderers.Count > 0) {
                for (int index = 0; index < transform.childCount; ++index) {
                    locationNames[index] =
                        $"Loc {transform.childCount - index}: {transform.GetChild(index).gameObject.name}";
                }
            }

            if (levels.Count == 0) {
                CurrentLevel = null;
            } else if (PlayerPrefs.HasKey(CurrentLevelKey)) {
                var index = Mathf.Min(levels.Count - 1, PlayerPrefs.GetInt(CurrentLevelKey));
                CurrentLevel = levels[index];
            } else {
                CurrentLevel = levels[0];
            }
        }

        private void Subscribe ()
        {
            Selection.selectionChanged += SelectionChanged;
            EditorApplication.hierarchyWindowChanged += HierarchyWindowChanged;
        }

        private void Unsubscribe ()
        {
            Selection.selectionChanged -= SelectionChanged;
            EditorApplication.hierarchyWindowChanged -= HierarchyWindowChanged;
        }

        #region GUIContents

        private GUIContent autoDetectionContent;
        private GUIContent endOfRoadContent;
        private GUIContent fogSpriteContent;
        private GUIContent fogCoveredAreaContent;
        private GUIContent levelsPerLocationContent;
        private GUIContent editButtonContent;
        private GUIContent mapBoundsContent;

        private void InitContents ()
        {
            ActiveButtonContent = new GUIContent("M");
            InactiveButtonContent = ActiveButtonContent;

            autoDetectionContent = new GUIContent {
                text = "Levels position autodetection",
                tooltip = "Enable manual levels rearrangement based on detection radius"
            };

            endOfRoadContent = new GUIContent {
                text = "End of road sprite",
                tooltip = "Sprite to be displayed at the end of available levels"
            };

            fogSpriteContent = new GUIContent {
                text = "Fog sprite",
                tooltip = "Sprite to represent fog"
            };

            fogCoveredAreaContent = new GUIContent {
                text = "Fog offset",
                tooltip = "How many uncompleted levels will be shown before fog appears"
            };

            levelsPerLocationContent = new GUIContent {
                text = "Levels per location",
                tooltip = "How many levels one location will contain"
            };

            editButtonContent = new GUIContent {
                text = "Edit",
            };

            mapBoundsContent = new GUIContent {
                text = "Edit map bounds",
            };
        }

        #endregion
    }
}