﻿using System.Linq;
using UnityEngine;

namespace MMK.Core.Editor
{
    public class SettingsTab : EditorTab
    {
        public SettingsTab()
        {
            subsections.AddRange(new EditorSubsection[] {
                new SettingsSection<GameSettings>("GameSettings"),
                new MonetizationSection()
            });

            subsectionsContents = subsections.Select(sec => sec.TabContent).ToArray();
            contentsList.Clear();
            contentsList.AddRange(subsectionsContents);

            ActiveButtonContent = new GUIContent("S");
            InactiveButtonContent = ActiveButtonContent;
        }
    }
}