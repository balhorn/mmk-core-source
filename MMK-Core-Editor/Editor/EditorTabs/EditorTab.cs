﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MMK.Core.Editor
{
    public abstract class EditorTab
    {
        public GUIContent EditorButtonContent => isActive ? ActiveButtonContent : InactiveButtonContent;

        protected readonly List<EditorSubsection> subsections = new List<EditorSubsection>();
        protected readonly List<GUIContent> contentsList = new List<GUIContent>();
        protected GUIContent[] subsectionsContents;

        protected GUIContent ActiveButtonContent { get; set; }
        protected GUIContent InactiveButtonContent { get; set; }

        protected bool isActive;

        private Vector2 currentScrollPosition;
        private int currentSubsection;

        public virtual void OnTabOpened()
        {
            isActive = true;
        }

        public virtual void OnTabClosed()
        {
            isActive = false;
        }

        public virtual void Draw()
        {
            EditorGUILayout.BeginVertical(MMKStyles.header);
            DrawHeader();
            EditorGUILayout.EndVertical();

            currentScrollPosition = EditorGUILayout.BeginScrollView(currentScrollPosition);

            EditorGUILayout.BeginVertical(MMKStyles.body);
            if (subsections.Count > 0) {
                currentSubsection = GUILayout.Toolbar(currentSubsection, subsectionsContents,
                    MMKStyles.levelEditorToolbar,
                    EditorUtilites.Height(50f));

                subsections[currentSubsection].Draw();
            } else {
                DrawBody();
            }
            EditorGUILayout.EndVertical();

            EditorGUILayout.EndScrollView();

            EditorGUILayout.BeginVertical(MMKStyles.footer);
            DrawFooter();
            EditorGUILayout.EndVertical();
        }

        protected virtual void DrawHeader(){}

        /// <summary>
        /// This method is invoked if there are no subsections. Otherwise it's ignored
        /// </summary>
        protected virtual void DrawBody(){}

        protected virtual void DrawFooter(){}

        protected void AddSubsection<T>() where T : EditorSubsection, new()
        {
            AddSubsection<T>(subsections.Count);
        }

        protected void AddSubsection<T>(int index) where T : EditorSubsection, new()
        {
            var newTab = new T {
                ParentTab = this
            };

            subsections.Insert(index, newTab);
            contentsList.Insert(index, newTab.TabContent);
        }

        protected void AddSubsection (EditorSubsection subsection)
        {
            AddSubsection(subsection, subsections.Count);
        }

        protected void AddSubsection (EditorSubsection subsection, int index)
        {
            subsections.Insert(index, subsection);
            contentsList.Insert(index, subsection.TabContent);
        }
    }
}