﻿using System.IO;
using System.Linq;
using UnityEditor;

namespace MMK.Core.Editor
{
    public class MMKSetup : UnityEditor.Editor
    {
        public static readonly string DirectoryName = Constants.MMKRoot + "Sprites";

        [MenuItem("Window/MMK/Setup", false, 10)]
        public static void Init ()
        {
            ReimportSprites();
        }

        public static void ReimportSprites ()
        {
            var fileNames = Directory.GetFiles(DirectoryName, "*", SearchOption.AllDirectories);
            foreach (var name in fileNames.Where(name => !name.Contains(".meta") && name.Contains(".png"))) {
                var spriteName = name.Replace("\\", "/");
                AssetDatabase.ImportAsset(spriteName);
            }
        }
    }
}

